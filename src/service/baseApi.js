import axios from "axios"

export const instance = axios.create({
     baseURL: process.env.NEXT_PUBLIC_BASEAPI,
})

export const post = async (url, payload={}) => {
    try{

        const response = await instance.post(url, payload)
        if(response.status === 200 || response.status === 201){
            return response.data
        }

    }catch(error){
        // console.log(`error ${url}`, error)
        return error?.response?.data
        // throw error
    }
}

export const get = async (url) => {
    try{
        const response = await instance.get(url)
        if(response.status === 200){
            return response.data
        }
    } catch(error){
        // console.log(`error ${url}`, error)
        return error?.response?.data
        // throw error
    }
}

export const put = async (url, payload={}) => {
    try{
        const response = await instance.put(url, payload)
        // if(response.status === 200){
            return response.data
        // }
    } catch(error){
        // console.log(`error ${url}`, error)
        return error?.response?.data
        // throw error
    }
}
export const deleteMethod = async (url, payload={}) => {
    try{
        const response = await instance.delete(url, payload)
        if(response.status === 200){
            return response.data
        }
    } catch(error){
        // console.log(`error ${url}`, error)
        return error?.response?.data
        // throw error
    }
}
export const getNoToken = async (url) => {
    try{
        const response = await axios.get(`${process.env.NEXT_PUBLIC_BASEAPI}${url}`) 
        if(response.status === 200){
            return response.data
        }
    } catch(error){
        // console.log(`error ${url}`, error)
        return error?.response?.data
        // throw error
    }
}