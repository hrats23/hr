import { AuthFormContainer } from '@/StyleComponet/Auth/Index'
import AuthLayout from '@/layout/AuthLayout'
import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import ForgotFrom from '@/components/authClient/ForgotForm'
import MetaHeader from '@/components/reusable/MetaHeader'

function Forgot() {

  return (
    <>
    <MetaHeader title='Forgot Password'/>
    <AuthLayout>
      <AuthFormContainer>
        <ForgotFrom />
      </AuthFormContainer>
    </AuthLayout>
    </>
  )
}

export default Forgot