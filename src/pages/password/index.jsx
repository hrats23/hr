import { AuthFormContainer } from '@/StyleComponet/Auth/Index'
import AuthLayout from '@/layout/AuthLayout'
import React from 'react'
import Head from 'next/head'
import PasswordForm from '@/components/authClient/PasswordForm'
import MetaHeader from '@/components/reusable/MetaHeader'

function Password() {
    
    return (
        <>
        <MetaHeader title='Password' />
        <AuthLayout>
            <AuthFormContainer>
                <PasswordForm />
            </AuthFormContainer>
        </AuthLayout>
        </>
    )
}

export default Password