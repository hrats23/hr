import { Aside } from '@/StyleComponet/hr'
import MetaHeader from '@/components/reusable/MetaHeader'
import DashBoardLayout from '@/layout/DashBoard'
import React from 'react'

const DashBoard = () => {
  return (
    <>
    <MetaHeader title='Dashboard' />
    <DashBoardLayout>
      Coming soon
    </DashBoardLayout>
    </>
  )
}

export default DashBoard