import { AuthFormContainer } from "@/StyleComponet/Auth/Index"
import LoginFrom from "@/components/authClient/LoginFrom"
import MetaHeader from "@/components/reusable/MetaHeader"
import AuthLayout from "@/layout/AuthLayout"

function Login() {

    return (
      <>
      <MetaHeader title='Login'/>
        <AuthLayout>
          <AuthFormContainer>
            <LoginFrom />
          </AuthFormContainer>
        </AuthLayout>
      </>
    )
  }

export default Login
