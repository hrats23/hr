import React from 'react'
import { Addbtn, Adddiv, AllJobTitleContainer, Aside, AsideContainer, Available, CandidatesDiv, Carddiv, Checkdiv, DaysDiv, DetailsDiv, Dropdiv, Dropform, DummyImages, Filterdiv, Formdiv, Halfdiv, Headdiv, Headerdiv, Headingdiv, HomeScreen, Homediv, InterviewedDiv, JobTextColor, JobTextDiv, Jobdiv, Letter, LineDiv, MenuDiv, MiddleHeaderText, Middlediv, NextDiv, NumSpan, OpensDiv, Outbtn, Outdiv, PublishDateText, PublishDiv, Radio, Selecterdiv, ShortlistedDiv, SourcedDiv, ViewButton, ZeroDiv, ZeroOneDiv } from '@/StyleComponet/hr'
import DashBoard from '@/layout/DashBoard'
import { Button } from '@/StyleComponet/Auth/Index'
import PlusIcon from '@/assets/svg/Buttonplus'
import { SubHeader, HeaderBtn } from '@/StyleComponet/career'
import { BackBtnContainer, BackText, FromHeading, InputContainer, JobContainer, JobFrom, JobFromContainer, JobHeader, JobInputContainer, JobInputAlign, JobLabelRequired, OptionLabel, JobInputLabel } from '@/StyleComponet/hr';
import BackBtn from '@/assets/svg/BackBtn'
import { Dropdown } from 'antd'
import MenuBar from '@/assets/svg/MenuBar'
import ViewJobModal from '@/components/modals/ViewJobModal'
import { useRouter } from 'next/router'
import MetaHeader from '@/components/reusable/MetaHeader'

function Index() {
  const router = useRouter()
  const [jobModal, setJobModal] = React.useState(false)

  // console.log(router.query)
  return (
    <>
    <MetaHeader title='Candidates' />
      <DashBoard>
        <HomeScreen style={{
          flexDirection: 'column'
        }}>
          {/* <Homediv> */}
            <SubHeader>
              <BackBtnContainer style={{
                gap: '0px',
                marginLeft: '40px',
                // textAlign: 'center'
              }}>
                <BackBtn style={{
                  width: '24px',
                  height: '24px',
                  marginRight: '9px',
                  cursor: 'pointer'
                }} onClick={() => router.push('/')} />
                <BackText style={{fontWeight: '600', marginRight: '15px'}}>
                  {router.query.job_name}
                </BackText>
                <ul style={{
                  marginLeft: '10px',
                  color: "#747474",
                  fontSize: "14px",
                  fontStyle: "normal",
                  fontWeight: "400",
                  lineHeight: "24px",
                }}>
                  <li style={{ listStyle: "disc" }}>{router.query.jobtype}</li>
                </ul>
              </BackBtnContainer>

              <BackBtnContainer
                style={{
                  marginRight: '39.77px',
                  gap: '0px'
                }}
              >
                <HeaderBtn
                  style={{
                    width: '131px',
                    marginRight: '24px'
                  }}
                  onClick={() => setJobModal(true)}
                >
                  Job details
                </HeaderBtn>
                <HeaderBtn
                  style={{
                    width: '167px',
                    marginRight: '17px'
                  }}
                >
                  Add candidate
                </HeaderBtn>
                <MenuBar style={{ cursor: 'pointer' }} />
              </BackBtnContainer>

            </SubHeader>
          <Aside style={{ boxShadow: 'none', position: 'relative', zIndex: 0 }}>
            Coming Soon..
          </Aside>

        </HomeScreen>
      </DashBoard >
      <ViewJobModal open={jobModal} close={() => setJobModal(false)} id={router.query.id}/>
    </>
  )
}

export default Index