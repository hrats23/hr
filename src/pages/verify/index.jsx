import { AuthFormContainer } from '@/StyleComponet/Auth/Index'
import AuthLayout from '@/layout/AuthLayout'
import React, { useState } from 'react'
import Head from 'next/head';
import VerifyForm from '@/components/authClient/VerifyForm';
import MetaHeader from '@/components/reusable/MetaHeader';


function Verify() {
    

  return (
    <>
    <MetaHeader title='Verify'/>
    <AuthLayout>
        <AuthFormContainer>
            <VerifyForm />
        </AuthFormContainer>
    </AuthLayout>
    </>
  )
}

export default Verify