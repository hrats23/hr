import React, { useEffect, useState } from 'react'
import DashBoard from '@/layout/DashBoard'
import privateRouter from '@/HOC/privateRouter'
import dynamic from 'next/dynamic'
// navigation router
import { useRouter } from 'next/router'
// images 
import BackBtn from '@/assets/svg/BackBtn'
// centerlized components
const MyEditor = dynamic(() => import('@/components/MyEditor'), { ssr: false })
import JobDescriptionTemp from '@/components/modals/JobDescriptionTemp'
import JobInput from '../../components/reusable/JobInput'
// styled components
import { BackBtnContainer, BackText, FromHeading, InputContainer, JobContainer, JobFrom, JobFromContainer, JobHeader, JobInputContainer, JobInputAlign, JobLabelRequired, OptionLabel, JobInputLabel, StrickyJobHeader } from '@/StyleComponet/hr';
import { Button } from '@/StyleComponet/Auth/Index';
// library
import { useForm } from 'react-hook-form';
import { get, put } from '@/service/baseApi';
import { message } from 'antd';
import dayjs from 'dayjs'
import MetaHeader from '@/components/reusable/MetaHeader'
import { moneyDigit, removeComma } from '@/assets/dummyData'


function Index() {
  const [tempModel, setTempModel] = useState(false);
  const router = useRouter();
  const [departments, setDepartments] = useState([]);
  const [workingModes, setWorkingModes] = useState([]);
  const [seniorityLevels, setSeniorityLevels] = useState([]);
  const [employmentTypes, setEmploymentTypes] = useState([]);
  const [techStacks, setTechStacks] = useState([]);
  const [educationName, setEducationName] = useState([])
  const [noticePeriod, setNoticePeriod] = useState([])
  const [openingJobs, setOpeningJobs] = useState([])
  const [defaultDescription, setDefaultDescription] = useState({})
  const [description, setDescription] = useState('');
  const [patchValue, setpatchValue] = useState({})

    // console.log(defaultDescription)

  const { control, handleSubmit, formState: { errors, isValid }, setValue } = useForm({
    mode: "onChange",
    defaultValues: () => defaultValue()
  });

  async function defaultValue() {
    const getData = await get("/add_new_job")
    const dropdown = getData?.data;
    setDepartments(dropdown?.departments);
    setWorkingModes(dropdown?.workingModes);
    setSeniorityLevels(dropdown?.seniorityLevels);
    setEmploymentTypes(dropdown?.employmentTypes);
    setTechStacks(dropdown?.techStacks);
    setEducationName(dropdown?.educations);
    setNoticePeriod(dropdown?.notice_period);
    setOpeningJobs(dropdown?.no_of_openings);


    const res = await get(`/view_job/${router.query.id}`);
    let data = res?.job;
    let ts1 = dropdown?.departments?.filter(val => val.id === data?.department_id)[0]
    let ts2 = dropdown?.workingModes?.filter(val => val.id === data?.working_mode_id)[0]
    let ts3 = dropdown?.seniorityLevels?.filter(val => val.id === data?.seniority_level_id)[0]
    let ts4 = dropdown?.employmentTypes?.filter(val => val.id === data?.employment_type_id)[0]
    let ts5 = dropdown?.techStacks?.filter(val => val.id === data?.tech_stack_id)[0]
    let ts6 = dropdown?.educations?.filter(val => val.id === data?.education_id)[0]
    let ts7 = dropdown?.notice_period?.filter(val => val.id === data?.notice_period)[0]
    let ts8 = dropdown?.no_of_openings?.filter(val => val.id === data?.no_of_openings)[0]

    setpatchValue(data)
    setDescription(data?.job_description)
   
    const closeDate = dayjs(data?.closed_by)
    // console.log(data?.closed_by, closeDate) 

    // console.log(moneyDigit(parseInt(data?.min_ctc)), removeComma('1,50,000'))

    const defaultData = {
      department: { value: ts1?.id, label: ts1?.department_name },
      working_model: { value: ts2?.id, label: ts2?.working_mode_name },
      seniority_level: { value: ts3?.id, label: ts3?.seniority_level_name },
      employe_type: { value: ts4?.id, label: ts4?.employment_name },
      techstack: { value: ts5?.id, label: ts5?.tech_stack_name },
      education: { value: ts6?.id, label: ts6?.education_type },
      notice_period: { value: ts7?.id, label: ts7?.day },
      opening_jobs: { value: ts8?.id, label: ts8?.opening },
      Job_title: data?.job_title,
      location: data?.location_name,
      min_salary: moneyDigit(parseInt(data?.min_ctc)),
      max_salary: moneyDigit(parseInt(data?.max_ctc)),
      min_experience: parseInt(data?.min_experience),
      max_experience: parseInt(data?.max_experience),
      close_date: closeDate,
      job_description: data?.job_description,
    }
    // console.log('defaultData', defaultData)
    return defaultData
  }

  const onSubmit = async (data) => {
    console.log('payload description',data?.job_description === defaultDescription.description_context, data?.job_description, defaultDescription.description_context)
    let payload = {
      job_title: data?.Job_title,
      department_id: data?.department.value,
      location_name: data?.location,
      working_mode_id: data?.working_model.value,
      job_description_id: data?.job_description === defaultDescription.description_context ? defaultDescription?.id : "",
      description_context: data?.job_description === defaultDescription.description_context ? '' : data?.job_description,
      seniority_level_id: data?.seniority_level.value,
      employment_type_id: data?.employe_type.value,
      tech_stack_id: data?.techstack.value,
      education_id: data?.education?.value ? data?.education?.value : undefined,
      min_ctc: data?.min_salary ? removeComma(data?.min_salary) : 0,
      max_ctc: data?.max_salary ? removeComma(data?.max_salary) : 0,
      min_experience: data.min_experience,
      max_experience: data.max_experience,
      // closed_by: data?.close_date?.$y + '-' + (`${data?.close_date?.$M + 1}`).padStart(2, '0') + '-' + data?.close_date?.$D,
      closed_by: dayjs(data.close_date).format('YYYY-MM-DD'),
      notice_period: data?.notice_period?.value,
      no_of_openings: data?.opening_jobs?.value,
    }
    console.log(payload);
    const res = await put(`/update_job/${router.query.id}`, payload);
    if (res?.status) {
      message.success(res?.message || 'Job added successfully');
      router.push('/')
    } else {
      message.error(res?.message || 'Something went wrong');
    }

  };


  return (
    <>
    <MetaHeader title='Job' />
      <DashBoard>
        <JobContainer>
          <JobFrom onSubmit={handleSubmit(onSubmit)}>
            <StrickyJobHeader>
              <BackBtnContainer>
                <BackBtn style={{
                  width: '24px',
                  height: '24px',
                  cursor: 'pointer'
                }} onClick={() => router.back()} />
                <BackText>
                  Edit job
                </BackText>
              </BackBtnContainer>
              <Button style={{ height: '48px', fontWeight: 700 }} disabled={!isValid} $topside={'0px'} width={'217px'}>Save</Button>
            </StrickyJobHeader>

            <JobFromContainer>
              <FromHeading> What&#39;s the job you&#39;re hiring for? </FromHeading>
              <InputContainer style={{ marginTop: '38px' }}>
                <JobInput
                  htmlFor="Job_title"
                  label="Job title"
                  // disable={true}
                  type="text"
                  name="Job_title"
                  placeholder="Enter the job title"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  // patternValue={/^[a-zA-Z ]*$/}
                  // patternMsg="Only alphabets are allowed"
                  requiredMsg="Job title is required"
                />
                <JobInput
                  htmlFor="department"
                  label="Department"
                  type="dropDown"
                  name="department"
                  placeholder="Select department"
                  requiredIcon={true}
                  dataOptions={departments}
                  control={control}
                  errors={errors}
                  requiredMsg="Department is required"
                  keyName="department_name"
                />
              </InputContainer>
              <InputContainer>
                <JobInput
                  htmlFor="location"
                  label="Location"
                  type="text"
                  name="location"
                  placeholder="Enter Location"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  patternValue={/^[a-zA-Z ]*$/}
                  patternMsg="Only alphabets are allowed"
                  requiredMsg="Location is required"
                />

                <JobInput
                  htmlFor="working_model"
                  label="Working model"
                  type="dropDown"
                  name="working_model"
                  placeholder="Select model"
                  requiredIcon={true}
                  dataOptions={workingModes}
                  control={control}
                  errors={errors}
                  requiredMsg="Work mode is required"
                  keyName="working_mode_name"
                />
              </InputContainer>
            </JobFromContainer>

            <JobFromContainer style={{ marginTop: '11px', marginBottom: '52px' }}>
              <JobHeader style={{
                marginBottom: '18px'
              }}>
                <FromHeading> Fill in some details for the job? </FromHeading>
                <Button style={{
                  height: '48px',
                  fontWeight: 700,
                  lineHeight: '24px'
                }} $topside={'0px'} width={'212px'}
                  type='button' onClick={() => setTempModel(pre => !pre)}>Select template</Button>
              </JobHeader>

              <JobInputLabel style={{ marginBottom: '15px', display: 'block' }}>Job description<JobLabelRequired>*</JobLabelRequired></JobInputLabel>
              <MyEditor
                name='job_description'
                placeholder="Write something awesome..."
                control={control}
                errors={errors}
                defaultDescription={defaultDescription}
                requiredMsg="Job description is required"
                description={description}
                setDescription={setDescription}
              />

              <InputContainer>
                <JobInput
                  htmlFor="seniority_level"
                  label="Seniority level"
                  type="dropDown"
                  name="seniority_level"
                  placeholder="Select level"
                  requiredIcon={true}
                  dataOptions={seniorityLevels}
                  control={control}
                  errors={errors}
                  requiredMsg="Senior level is required"
                  keyName="seniority_level_name"
                />
                <JobInput
                  htmlFor="employe_type"
                  label="Employment type"
                  type="dropDown"
                  name="employe_type"
                  placeholder="Select type"
                  requiredIcon={true}
                  dataOptions={employmentTypes}
                  control={control}
                  errors={errors}
                  requiredMsg="Employment is required"
                  keyName="employment_name"
                />
              </InputContainer>

              <InputContainer>
                <JobInput
                  htmlFor="techstack"
                  label="Techstack"
                  type="dropDown"
                  name="techstack"
                  placeholder="Select techstack"
                  requiredIcon={true}
                  dataOptions={techStacks}
                  control={control}
                  errors={errors}
                  requiredMsg="Techstack is required"
                  keyName="tech_stack_name"
                />

                <JobInput
                  htmlFor="education"
                  label="Education"
                  type="dropDown"
                  name="education"
                  placeholder="Select education"
                  optionLabel={true}
                  dataOptions={educationName}
                  control={control}
                  errors={errors}
                  keyName="education_type"
                />
              </InputContainer>

              <InputContainer>
                <JobInputContainer>
                  <JobInputLabel> Annual range <OptionLabel>( Optional )</OptionLabel></JobInputLabel>
                  <JobInputAlign >
                    <JobInput
                      // type="text"
                      formate='salary'
                      name="min_salary"
                      placeholder="Eg. 1,00,000 Lakhs"
                      control={control}
                      errors={errors}
                      setValue={setValue}
                      maxLengthMsg="Max 7 digits"
                      maxLengthValue={9}
                    />
                    -
                    <JobInput
                      // type="text"
                      formate='salary'
                      name="max_salary"
                      placeholder="Eg. 2,00,000 Lakhs"
                      control={control}
                      errors={errors}
                      setValue={setValue}
                      maxLengthMsg="Max 7 digits"
                      maxLengthValue={9}
                    />
                  </JobInputAlign>
                </JobInputContainer>
                <JobInputContainer>
                  <JobInputLabel>Work experience (in years) <JobLabelRequired>*</JobLabelRequired></JobInputLabel>
                  <JobInputAlign >
                    <JobInput
                      type="number"
                      name="min_experience"
                      placeholder="Min."
                      control={control}
                      errors={errors}
                      requiredMsg="Minimum experience is required"
                      maxLengthMsg="Max 2 digits"
                      maxLengthValue={2}
                    />
                    -
                    <JobInput
                      type="number"
                      name="max_experience"
                      placeholder="Max."
                      control={control}
                      errors={errors}
                      requiredMsg="Maximum experience is required"
                      maxLengthMsg="Max 2 digits"
                      maxLengthValue={2}
                    />
                  </JobInputAlign>
                </JobInputContainer>
              </InputContainer>

              <InputContainer>
                <JobInput
                  htmlFor="close_date"
                  label="Close by"
                  type="date"
                  name="close_date"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  requiredMsg="close date is required"
                  // defaultValue={patchValue?.closed_by}
                />
                <JobInput
                  htmlFor="notice_period"
                  label="Notice period"
                  type="dropDown"
                  name="notice_period"
                  placeholder="Select notice period"
                  requiredIcon={true}
                  dataOptions={noticePeriod}
                  control={control}
                  errors={errors}
                  requiredMsg="Notice period is required"
                  keyName="day"
                />
              </InputContainer>

              <InputContainer>
                <JobInput
                  htmlFor="opening_jobs"
                  label="Number of openings"
                  type="dropDown"
                  name="opening_jobs"
                  placeholder="Select openings"
                  optionLabel={true}
                  dataOptions={openingJobs}
                  control={control}
                  errors={errors}
                  keyName="opening"
                />
              </InputContainer>

            </JobFromContainer>
          </JobFrom>
        </JobContainer>
      </DashBoard>

      <JobDescriptionTemp open={tempModel} close={() => setTempModel(pre => !pre)} setDefaultDescription={setDefaultDescription} />
    </>
  )
}

// export default privateRouter(Index)
export default Index