import React, { useState } from 'react'
import privateRouter from '@/HOC/privateRouter'
import AdminDashBoardLayout from '@/layout/AdminDashBoardLayout'
import { AddUserBtn, AdminHeaderText, AdminMainContainer, AdminMainHeader, AdminTD, AdminTH, AdminTR, AdminTable, AdminTableContainer, AdminTableDiv } from '@/StyleComponet/admin'
import PlusIcon from '@/assets/svg/PlusIcon'
import AddAdminUser from '@/components/modals/AddAdminUser'
import { Button } from '@/StyleComponet/Auth/Index'
import dynamic from 'next/dynamic'
import MetaHeader from '@/components/reusable/MetaHeader'
const UserTable = dynamic(() => import('@/components/UserTable'), { ssr: false })

function Admin() {
  const [addUserModal, setAddUserModal] = useState(false)
  const [totalUser, setTotalUser] = useState(0)

  return (
    <>
    <MetaHeader title='Admin' />
      <AdminDashBoardLayout>
        <AdminMainContainer>
          <AdminMainHeader $colorbackground={'true'} $topborder={'false'}>
            <AdminHeaderText>
              Manage users ({totalUser})
            </AdminHeaderText>
              <Button
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '10px',
                }}
                $topside={'0px'}
                width={'192px'}
                onClick={() => setAddUserModal(true)}
              >
                <PlusIcon />
                Add user
              </Button>

          </AdminMainHeader>
          <AdminTable>
            <UserTable setTotalUser={setTotalUser}/>
          </AdminTable>
        </AdminMainContainer>
      </AdminDashBoardLayout>
      {/* <AddAdminUser open={addUserModal} close={() => setAddUserModal(false)} /> */}
      {addUserModal && <AddAdminUser open={addUserModal} close={() => setAddUserModal(false)} />}

    </>
  )
}

export default privateRouter(Admin)