import { JobListWraper, BackBtnText, SpaceCenter, JobRightContainer, JobleftContainer, JobleftHeading, JobleftParagrap, Positions, JobleftCard, JobleftCardText, JobleftCardMail, JobRightHeading, FormTopic, SubmitBtn, FileUploadSection } from '@/StyleComponet/career'
import { BackBtnContainer, InputContainer, JobFrom, } from '@/StyleComponet/hr';
import { useRouter } from 'next/router'
import BackBtn from '@/assets/svg/BackBtn'
import UnderLine from '@/assets/svg/UnderLine'
import ArrowDown from '@/assets/svg/ArrowDown'
import React from 'react'
import JobListMsg from '@/assets/svg/JobListMsg'
import JobInput from '@/components/reusable/JobInput';
// library
import { useForm } from 'react-hook-form';
import MetaHeader from '@/components/reusable/MetaHeader';

const departments = [
  'Full Time',
  'Intern',
  'Trainee',
]
function Index() {
  const router = useRouter()
  const { control, handleSubmit, formState: { errors, isValid }, reset, setValue } = useForm({
    mode: "onChange",
  });

  const onSubmit = (data) => {
    console.log(JSON.stringify(data, null, 1));
  };

  return (
    <>
    <MetaHeader title='Careers' />
      <JobListWraper>
        <BackBtnContainer
          style={{
            gap: '12px',
            marginBottom: '56px'
          }}
        >
          <BackBtn
            onClick={() => router.back()}
            style={{
              width: '44px',
              height: '44px'
            }}
          />
          <BackBtnText>Back to job listing</BackBtnText>
        </BackBtnContainer>

        <SpaceCenter>
          <JobleftContainer>
            <JobleftHeading>Thank you for choosing Siam computing</JobleftHeading>
            <UnderLine />
            <JobleftParagrap>
              In order to ensure a smooth and efficient process, we ask that you please take the time to carefully fill out this job application form.
            </JobleftParagrap>
            <JobleftCard>
              <JobListMsg />
              <JobleftCardText>In case of any queries reach out to </JobleftCardText>
              <JobleftCardMail>jobs@siamcomputing.com</JobleftCardMail>
            </JobleftCard>
          </JobleftContainer>

          <JobRightContainer>
            <Positions>
              <JobRightHeading>Please fill in the form below and we will get in touch with you</JobRightHeading>
              <ArrowDown
                style={{
                  position: 'absolute',
                  right: '-30px',
                  top: '17px'
                }}
              />
            </Positions>
            <JobFrom
              style={{
                borderRadius: "10px",
                background: "#FFF",
                boxShadow: "0px 4px 40px 0px rgba(252, 51, 51, 0.10)",
                padding: '30px 36px',
              }}
              onSubmit={handleSubmit(onSubmit)}
            >

              <JobInput
                htmlFor="about_us"
                label="How do you know about us"
                type="dropDown"
                name="about_us"
                placeholder="Choose an option"
                requiredIcon={true}
                dataOptions={departments}
                control={control}
                errors={errors}
                requiredMsg="Department is required"
              />

              <FormTopic style={{ marginTop: '42px' }}>Upload your resume</FormTopic>

              <FileUploadSection>
                <span>Drop file here or click to upload</span>
                <input
                  type="file"
                  name="myFile"
                // style={{ display: 'none' }}
                />
              </FileUploadSection>

              <FormTopic style={{ marginTop: '40px' }}>Contact information</FormTopic>

              <InputContainer style={{ marginTop: '19px' }}>
                <JobInput
                  htmlFor="first_name"
                  label="First name"
                  type="text"
                  name="first_name"
                  placeholder="Enter first name"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  requiredMsg="Location is required"
                />

                <JobInput
                  htmlFor="last_name"
                  label="Last name"
                  type="text"
                  name="last_name"
                  placeholder="Enter last name"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  requiredMsg="Location is required"
                />
              </InputContainer>

              <InputContainer style={{ marginTop: '22px' }}>
                <JobInput
                  htmlFor="mobile_number"
                  label="Mobile number"
                  type="number"
                  name="mobile_number"
                  placeholder="Enter mobile number"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  requiredMsg="Location is required"
                />

                <JobInput
                  htmlFor="email"
                  label="Email address"
                  type="email"
                  name="email"
                  placeholder="Enter email address"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  requiredMsg="Location is required"
                />
              </InputContainer>

              <InputContainer style={{ marginTop: '22px' }}>
                <JobInput
                  htmlFor="description"
                  label="Description"
                  type="textarea"
                  name="description"
                  placeholder="Let us know more about yourself..."
                  textAreaResize='none'
                  rows='8'
                  control={control}
                  errors={errors}
                />
              </InputContainer>

              <FormTopic style={{ marginTop: '42px' }}>Job information</FormTopic>

              <InputContainer style={{ marginTop: '28px' }}>
                <JobInput
                  htmlFor="position"
                  label="Position applying for"
                  type="text"
                  name="position"
                  placeholder="Product design engineer"
                  control={control}
                  errors={errors}
                />
                <JobInput
                  htmlFor="total_experience"
                  label="Total experience"
                  type="dropDown"
                  name="total_experience"
                  placeholder="Choose an option"
                  requiredIcon={true}
                  dataOptions={departments}
                  control={control}
                  errors={errors}
                  requiredMsg="Department is required"
                />
              </InputContainer>

              <InputContainer style={{ marginTop: '22px' }}>
                <JobInput
                  htmlFor="relevent_experience"
                  label="Relevant experience"
                  type="dropDown"
                  name="relevent_experience"
                  placeholder="Choose an option"
                  requiredIcon={true}
                  dataOptions={departments}
                  control={control}
                  errors={errors}
                  requiredMsg="Department is required"
                />
                <JobInput
                  htmlFor="current_ctc"
                  label="Current CTC"
                  type="number"
                  name="current_ctc"
                  placeholder="Enter current CTC"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  requiredMsg="Department is required"
                />
              </InputContainer>

              <InputContainer style={{ marginTop: '22px' }}>
                <JobInput
                  htmlFor="expected_ctc"
                  label="Expected CTC"
                  type="number"
                  name="expected_ctc"
                  placeholder="Enter expected CTC"
                  requiredIcon={true}
                  control={control}
                  errors={errors}
                  requiredMsg="Department is required"
                />
                <JobInput
                  htmlFor="notice_period"
                  label="Notice period"
                  type="dropDown"
                  name="notice_period"
                  placeholder="Choose an option"
                  requiredIcon={true}
                  dataOptions={departments}
                  control={control}
                  errors={errors}
                  requiredMsg="Department is required"
                />
              </InputContainer>

              <SubmitBtn>Submit</SubmitBtn>

            </JobFrom>

          </JobRightContainer>

        </SpaceCenter>
      </JobListWraper>
    </>
  )
}

export default Index