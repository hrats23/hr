import React, { useEffect, useState } from 'react'
import { CareerApplyBtn, CareerArrowContainer, CareerBackButton, CareerFooter, CareerFooterBtn, CareerFooterConteriner, CareerFooterText, CareerJobContainer, CareerJobDetailsMain, CareerJobLoation, CareerJobTitle, CareerKeyCardList, CareerKeyDetailsContainer, CareerKeyTitle } from '@/StyleComponet/career'
import { useRouter } from 'next/router'
import CareerBackIcon from '@/assets/svg/CareerBackIcon'
import CareerKeyCardBox from '@/components/reusable/CareerKeyCardBox'
import EmployemenType from '@/assets/svg/EmployementTypeIcon'
import WorkModal from '@/assets/svg/WorkModal'
import WorkExprience from '@/assets/svg/WorkExperience'
import NoticePeriod from '@/assets/svg/NoticePeriod'
import AnualRange from '@/assets/svg/AnualRange'
import NumberOpening from '@/assets/svg/NumberOpening'
import Education from '@/assets/svg/Education'
import CareerDownArror from '@/assets/svg/CareerDownArror'
import { JobPreviewDescription } from '@/StyleComponet/candidate'
import MetaHeader from '@/components/reusable/MetaHeader'
import { getNoToken } from '@/service/baseApi'
import { moneyDigit } from '@/assets/dummyData'

const keyDetailsData = [
    {
        title: 'Employement Type',
        icon: EmployemenType,
        subTitle: 'Full Time'
    }, {
        title: 'Work Modal',
        icon: WorkModal,
        subTitle: 'Remote'
    }, {
        title: 'Work Experience',
        icon: WorkExprience,
        subTitle: '1 year'
    }, {
        title: 'Notice Period',
        icon: NoticePeriod,
        subTitle: '1 month'
    }, {
        title: 'Annual Range',
        icon: AnualRange,
        subTitle: '1 year'
    }, {
        title: 'Number of Openings',
        icon: NumberOpening,
        subTitle: '1'
    }, {
        title: 'Education',
        icon: Education,
        subTitle: 'B.E'
    }
]

function Index({ searchParams }) {
    const router = useRouter()
    const [jobData, setJobData] = useState({})
    const [keyDetails, setKeyDetails] = useState(keyDetailsData)
    const [isLoading, setIsLoading] = useState(false)

    // console.log(router.query.jobId)
    useEffect(() => {
        const fetch = async () => {
            setIsLoading(true)
            const res = await getNoToken(`career-single-job/${router.query.jobId}`)
            
            // console.log(res)
            setJobData(res.job)
            
            setKeyDetails(pre => {
                let dummy = [...pre]
                // console.log(dummy)
                dummy[0].subTitle = res?.job?.employment_type_name || 'Not Specified'
                dummy[1].subTitle = res?.job?.working_mode_name || 'Not Specified'
                dummy[2].subTitle = res?.job?.seniority_level_name || 'Not Specified'
                dummy[3].subTitle = res?.job?.notice_period || 'Not Specified'
                dummy[4].subTitle = `${moneyDigit(res?.job?.min_ctc)} - ${moneyDigit(res?.job?.max_ctc)}` || 'Not Specified'
                dummy[5].subTitle = res?.job?.no_of_openings || 0
                dummy[6].subTitle = res?.job?.education_type || 'Not Specified'
                return dummy
            })
            setIsLoading(false)
        }
        fetch()
    }, [router.query.jobId])
    return (
        <>
            <MetaHeader title='Careers' />

            <CareerJobDetailsMain>
                <CareerBackButton onClick={() => router.push('/career')}>
                    <CareerBackIcon />
                    Back to Job Listings
                </CareerBackButton>


                {!isLoading && (<>
                    <CareerJobTitle>
                        {jobData?.job_title || 'Job Title'}
                    </CareerJobTitle>

                    <CareerJobLoation>
                        Location: {jobData?.location_name || 'Location'} / {jobData?.working_mode_name || 'City'}
                    </CareerJobLoation>
                    <CareerKeyDetailsContainer>
                        <CareerKeyTitle style={{ lineHeight: 'normal' }}>
                            Key details
                        </CareerKeyTitle>
                        <CareerKeyCardList>
                            {keyDetails.map((item, index) => (
                                <CareerKeyCardBox color='pink' item={item} key={index} />
                            ))}
                        </CareerKeyCardList>
                    </CareerKeyDetailsContainer>

                    <div class="ql-editor">

                        <JobPreviewDescription dangerouslySetInnerHTML={{ __html: jobData?.job_description }}></JobPreviewDescription>
                    </div>
                    <CareerJobContainer>
                        <CareerApplyBtn onClick={() => router.push(`/career/${router.query.id}/apply?job_id=${jobData?.id}`)}>Apply to this Job</CareerApplyBtn>
                    </CareerJobContainer>
                </>)}


            </CareerJobDetailsMain>
            {!isLoading &&
                <CareerFooter>
                    <CareerFooterConteriner>
                        <CareerFooterText>
                            Get in touch with the HR for queries.
                        </CareerFooterText>
                        <CareerFooterBtn>
                            Email HR Now
                        </CareerFooterBtn>
                        <CareerArrowContainer>
                            <CareerDownArror />
                        </CareerArrowContainer>
                    </CareerFooterConteriner>
                </CareerFooter>
            }
        </>
    )
}

export default Index