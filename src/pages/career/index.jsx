import {  CareerHeader, UnSelectedItem, CareerMain, SelectedHeader, SelectedItem, CardContainer, CardHeading, CardBoxes, CardFooter, CardFooterFirstChild, CardFooterSecondChild } from '@/StyleComponet/career'
import React, { useEffect, useState } from 'react'
import CirclePath from '@/assets/svg/CirclePath'
import MetaHeader from '@/components/reusable/MetaHeader';
import { getNoToken } from '@/service/baseApi';
import { useRouter } from 'next/router';
import { useQuery } from 'react-query';

function Index() {
    const router = useRouter()
    const [activeIndex, setActiveIndex] = useState('Intern');
    const [headIndex, setHeadIndex] = useState('Intern');
    const {isLoading, data} = useQuery(['career-all-jobs'], () => getNoToken(`career-all-jobs`), {
        refetchOnWindowFocus: false
    })
    
    const [var1, var2, var3] = data?.employment_type?.filter(item => item.employment_name !== 'Trainee') || []
    

    const handleItemClick = (value) => {
        value.employment_name === 'Full Time' ? setActiveIndex([value?.employment_name, 'Trainee']) : setActiveIndex(value?.employment_name);
        setHeadIndex(value?.employment_name);
    };

    // console.log(data?.employment_type?.filter(item => item.employment_name !== 'Trainee').sort((a, b) => a.employment_name.localeCompare(b.employment_name)))


    return (
        <>
            <MetaHeader title='Careers' />
            <CareerMain>
                <CareerHeader>
                    {
                        [var3, var1, var2].map((item, index) => {
                            return (
                                <SelectedHeader
                                    key={index}
                                    className={headIndex == item?.employment_name ? 'active' : ''}
                                    onClick={() => handleItemClick(item)}
                                >
                                    {
                                        headIndex == item?.employment_name ?
                                            <>
                                                <CirclePath />
                                                <SelectedItem>{item?.employment_name}</SelectedItem>
                                            </>
                                            : <UnSelectedItem>{item?.employment_name}</UnSelectedItem>
                                    }
                                </SelectedHeader>
                            )
                        })
                    }
                </CareerHeader>

                <CardContainer>
                    {
                        data?.jobs?.filter(item => Array.isArray(activeIndex) ? activeIndex.includes(item?.employment_name)   : item?.employment_name === activeIndex).map((item, index) =>
                            <CardBoxes key={index} tabIndex={0} onClick={() => router.push(`/career/job?jobId=${item?.id}`)}>
                                <CardHeading>{item?.job_title}</CardHeading>
                                <CardFooter>
                                    <CardFooterFirstChild>{item?.location_name}</CardFooterFirstChild>
                                    <CardFooterSecondChild>Posted: {item?.posted_on}
                                    </CardFooterSecondChild>
                                </CardFooter>
                            </CardBoxes>
                        )
                    }
                </CardContainer>

            </CareerMain>
        </>
    )
}

export default Index