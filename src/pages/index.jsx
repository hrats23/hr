import React, { Suspense, useEffect } from 'react'
import DashBoard from '@/layout/DashBoard'
import { Addbtn, Adddiv, AllJobTitleContainer, Aside, AsideBtnContainer, AsideContainer, AsideFilterHeader, AsideListContainer, Available, CandidatesDiv, Carddiv, Checkdiv, DaysDiv, DetailsDiv, Dropdiv, Dropform, DummyImages, Filterdiv, Formdiv, Halfdiv, Headdiv, Headerdiv, Headingdiv, HomeScreen, Homediv, InterviewedDiv, JobInputLabel, JobTextColor, JobTextDiv, Jobdiv, Letter, LineDiv, LogoutText, MenuDiv, MiddleHeaderText, Middlediv, NextDiv, NumSpan, OpensDiv, Outbtn, Outdiv, PublishDateText, PublishDiv, Radio, RatioTitle, Selecterdiv, ShortlistedDiv, SourcedDiv, ViewButton, ZeroDiv, ZeroOneDiv } from '@/StyleComponet/hr'
import privateRouter from '@/HOC/privateRouter'
import PlusIcon from '@/assets/svg/Buttonplus'
import OutArrow from '@/assets/svg/Logout'
import Image from 'next/image';
import middle from '@/assets/image/System.png'
import { useState } from "react";
import { Button, CancelBtn, ResetBtn } from '@/StyleComponet/Auth/Index'
import RationList from '@/components/reusable/RationList'
import { departmentsList } from '@/assets/dummyData'
import AsideDropdown from '@/components/reusable/AsideDropdown'

import { Dropdown, Skeleton, message } from 'antd';
import { get } from '@/service/baseApi'
import { useQuery } from 'react-query'
import WarningBox from '@/components/modals/WarningBox'
import JobCards from '@/components/reusable/JobCards'
import { useForm } from 'react-hook-form'
import AsideDate from '@/components/reusable/AsideDate'
import { useRouter } from 'next/router'
import dayjs from 'dayjs'
import MetaHeader from '@/components/reusable/MetaHeader'
import { Colors } from '@/StyleComponet/Index'

/**
 * Handles the click event of the "Add new job" button.
 */
function Home() {
  const router = useRouter()
  const [warningMessage, setWarningMessage] = useState({});
  const [boxOpen, setBoxOpen] = useState(false)
  const [fileterValue, setFileterValue] = useState({
    Jobs: "all_jobs",
    date: "all"
  })
  const [department, setDepartment] = useState('')
  const [resetFilter, setResetFilter] = useState(false)

  const customDate = (array) => dayjs(getValues('customeDate')?.[array]?.$d || null).format('YYYY-MM-DD')

  const { control, watch, formState: { errors }, getValues, setValue } = useForm({ mode: "onChange" })

  const { data, isLoading } = useQuery(['allJobs', fileterValue.Jobs, fileterValue.date, department, getValues('customeDate')],
    () => get(`/view_all_jobs?filters=${fileterValue.Jobs}&date_filter=${fileterValue.date}&department_id=${department ? department : ''}
    ${fileterValue.date === 'custom' ?
        `&from_date=${customDate(0) === 'Invalid Date' ? '' : customDate(0)}&to_date=${customDate(1) === 'Invalid Date' ? '' : customDate(1)}`
        : ''}`), {
    refetchOnWindowFocus: false
  })

  const { data: data2, isLoading: isLoading2 } = useQuery(['hrSideFilter'], () => get('/filter_options'), {
    refetchOnWindowFocus: false
  })

  useEffect(() => {
    setDepartment(watch('deparment')?.value)
    // console.log(watch('deparment'))
  }, [watch()])

  // console.log(resetFilter)


  const handleLogout = () => {
    setWarningMessage({
      title: 'Are you sure you want to logout?',
      // subTitle: 'Do you really want to logout?',
      main: 'logout'
    })
    setBoxOpen(true)
  }

  const handleAddJob = () => {
    // TODO: Implement the logic for adding a new job
    router.push('/job')

  };

  // console.log(getValues())

  const handleExport = async() => {

    // console.log(`${process.env.NEXT_PUBLIC_BASEAPI}export-csv?filters=${getValues('Jobs') || 'all_jobs'}&department_id=${getValues('deparment')?.value || ''}&date_filter=${getValues('date') || 'all'}${getValues('date') === 
    // "custom" ? `&from_date=${customDate(0) === 'Invalid Date' ? '' : customDate(0)}&to_date=${customDate(1) === 'Invalid Date' ? '' : customDate(1)}` : ''}&token=${localStorage.getItem('token')}`)

    window.open(`${process.env.NEXT_PUBLIC_BASEAPI}export-csv?filters=${getValues('Jobs') || 'all_jobs'}&department_id=${getValues('deparment')?.value || ''}&date_filter=${getValues('date') || 'all'}${getValues('date') === 
    "custom" ? `&from_date=${customDate(0) === 'Invalid Date' ? '' : customDate(0)}&to_date=${customDate(1) === 'Invalid Date' ? '' : customDate(1)}` : ''}&token=${localStorage.getItem('token')}`)
  }

  const handleReset = () => {
    setResetFilter(true)

    setFileterValue({ Jobs: "all_jobs", date: "all" })
    setDepartment('')
    setValue('deparment', undefined)
    setValue('customeDate', undefined)
    
  }



// console.log(getValues())

  return (
    <>
      <MetaHeader title='Home'/>
      <DashBoard>
        <HomeScreen>
          <Aside>
            <AsideContainer>
              <AsideBtnContainer>
                <Button
                  style={{
                    marginTop: '33px',
                    marginBottom: '35px',
                    display: 'flex',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    gap: '10px',
                    fontWeight: '700',
                    lineHeight: '24px'
                  }}
                  $topside={'0px'}
                  width={'240px'}
                  onClick={handleAddJob}
                >
                  <PlusIcon />
                  Add new job
                </Button>
              </AsideBtnContainer>
              <AsideListContainer>
                <AsideFilterHeader>
                  <JobInputLabel style={{ fontWeight: '500', fontSize: '16px', lineHeight: '24px', color: Colors.blackText }}>Filter by</JobInputLabel>
                <ResetBtn onClick={() => handleReset()} >Reset All</ResetBtn>
                </AsideFilterHeader>
                <RationList resetFilter={resetFilter} setResetFilter={setResetFilter} option={data2?.job_status} control={control} isLoading={isLoading2} name='Jobs' label='Job Status' defaultValue='all_jobs' setFileterValue={setFileterValue} />
                <AsideDropdown resetFilter={resetFilter} list={data2?.department} control={control} isLoading={isLoading2} name='deparment' label='Department' placeHolder='Select department' />
                <RationList resetFilter={resetFilter} setResetFilter={setResetFilter} option={data2?.filter_by_date} control={control} isLoading={isLoading2} name='date' label='Days' defaultValue='all' setFileterValue={setFileterValue} />
                {fileterValue?.date === 'custom' && (
                  <AsideDate
                    label="Date"
                    name="customeDate"
                    placeHolder="Select date"
                    control={control}
                    requiredMsg='Date is required' />
                )}
              </AsideListContainer>
            </AsideContainer>
            <AsideContainer style={{ margin: '20px' }}>
              <Outbtn onClick={() => handleLogout()}>
                <OutArrow />
                <LogoutText>

                Log out
                </LogoutText>
              </Outbtn>
            </AsideContainer>
          </Aside>
          <Homediv>
            <Middlediv $colorbackground={'true'} $topborder={'false'}>
              <MiddleHeaderText>{data2?.job_status?.filter(item => item?.label === fileterValue.Jobs)?.[0]?.value || 'All Jobs'} ({data?.jobs?.length || 0}) </MiddleHeaderText>
              {/* <CSVLink data={body} headers={headers}> */}

              
              <Button
                disabled={data?.jobs?.length === 0}
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'center',
                  gap: '10px',
                  lineHeight: '24px'
                }}
                $topside={'0px'}
                width={'131px'}
                height={'38px'}
                onClick={() => handleExport()}
              // onClick={() => setAddUserModal(true)}
              >
                Export
              </Button>
              {/* </CSVLink> */}

            </Middlediv>

            <Carddiv>
              {(data?.jobs?.length === 0) && (
                <DummyImages>
                  <Image src={middle} alt='' width='374' height='228' />
                  <Available>Looks like there are no { watch('Jobs') === 'onhold' ? 'On hold  jobs' : watch('Jobs') === 'closed' ? 'Closed jobs' : 'openings' } available</Available>
                </DummyImages>
              )}
              {Array.isArray(data?.jobs) && data?.jobs?.map((value, index) => (
                <JobCards value={value} key={index} />
              ))}
              {isLoading && Array(12).fill(null).map((_, index) => (
                <Skeleton key={index} active style={{
                  width: '100%',
                  maxWidth: '498px',
                  height: '100%',
                  maxHeight: '235px',
                  marginTop: '16px',
                  borderRadius: '4px',
                  padding: '18px',
                }} />
              ))}
            </Carddiv>
          </Homediv>
        </HomeScreen>
      </DashBoard>
      <WarningBox open={boxOpen} close={() => setBoxOpen(false)} warningMessage={warningMessage} />
    </>
  );
}

export default privateRouter(Home)


///['JOBS, DEPARTMENTS', "\\"]