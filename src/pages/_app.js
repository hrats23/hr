import { GlobalStyle } from '@/StyleComponet/Index'
import UserProvider from '@/context/User'
import dynamic from 'next/dynamic';
import { QueryClient, QueryClientProvider } from 'react-query';
import 'react-quill/dist/quill.snow.css';

 function App({ Component, pageProps }) {
  const queryClient = new QueryClient();
  
  return (
    <QueryClientProvider client={queryClient}>
      <UserProvider>
        <GlobalStyle />
        <Component {...pageProps} />
      </UserProvider>
    </QueryClientProvider>
  )
}

export default dynamic(() => Promise.resolve(App), { ssr: false })
