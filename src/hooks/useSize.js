'use client'

import { useEffect, useState } from "react";
const useSize = () => {
    const [windowSize, setWindowSize] = useState({
        height: undefined,
        width: undefined,
    });

    useEffect(() => {
        const windowSizeHandler = () => {
            setWindowSize({
                height: window.innerHeight,
                width: window.innerWidth,
            });
        };
        window.addEventListener("resize", windowSizeHandler);

        return () => {
            window.removeEventListener("resize", windowSizeHandler);
        };
    }, []);

    return windowSize;
};

export default useSize;