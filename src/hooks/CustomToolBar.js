import React from "react";
import { Quill } from "react-quill";

// Custom Undo button icon component for Quill editor. You can import it directly
// from 'quill/assets/icons/undo.svg' but I found that a number of loaders do not
// handle them correctly
const CustomUndo = () => (
    <svg viewBox="0 0 18 18">
        <polygon className="ql-fill ql-stroke" points="6 10 4 12 2 10 6 10" />
        <path
            className="ql-stroke"
            d="M8.09,13.91A4.6,4.6,0,0,0,9,14,5,5,0,1,0,4,9"
        />
    </svg>
);

// Redo button icon component for Quill editor
const CustomRedo = () => (
    <svg viewBox="0 0 18 18">
        <polygon className="ql-fill ql-stroke" points="12 10 14 12 16 10 12 10" />
        <path
            className="ql-stroke"
            d="M9.91,13.91A4.6,4.6,0,0,1,9,14a5,5,0,1,1,5-5"
        />
    </svg>
);

// Undo and redo functions for Custom Toolbar
export function undoChange() {
    this.quill.history.undo();
}
export function redoChange() {
    this.quill.history.redo();
}


const fontSizeArr = ['14px', '16px', '18px', '20px'];
const fontFamily = ['Inter', 'impact', 'courier', 'comic'];

// Add sizes to whitelist and register them
const Size = Quill.import("attributors/style/size");
Size.whitelist = fontSizeArr;
Quill.register(Size, true);

// Add fonts to whitelist and register them
const Font = Quill.import("formats/font");
Font.whitelist = fontFamily;
Quill.register(Font, true);

// Modules object for setting up the Quill editor
export const modules = {
    toolbar: {
      container: "#t1",
      handlers: {
        undo: undoChange,
        redo: redoChange,
      }
    },
    history: {
      delay: 500,
      maxStack: 100,
      userOnly: true
    }
  };

  export const modules1 = {
    toolbar: {
      container: "#t2",
      handlers: {
        undo: undoChange,
        redo: redoChange,
      }
    },
    history: {
      delay: 500,
      maxStack: 100,
      userOnly: true
    }
  };

// Formats objects for setting up the Quill editor
export const formats = [
    "header",
    "font",
    "size",
    "bold",
    "italic",
    "underline",
    "link",
    "align",
    "strike",
    "script",
    "blockquote",
    "background",
    "list",
    "bullet",
    "indent",
];

// Quill Toolbar component
const QuillToolbar = ({id}) => (
    <div id={`${id}`}>
        <div className="left-side">
            <span className="ql-formats">
                <button className="ql-bold" data-tooltip='Bold'/>
                <button className="ql-italic" data-tooltip='Italic'/>
                <button className="ql-underline" data-tooltip='Underline'/>
                <button className="ql-link" data-tooltip='Link'/>
            </span>
            <span className="ql-formats">
                <button className="ql-align" value="center" data-tooltip='Center'>
                    C
                </button>
                <button className="ql-align" value="right" data-tooltip='Right'>
                    R
                </button>
                <button className="ql-align" value="justify" data-tooltip='Justify'>
                    J
                </button>
            </span>
            <span className="ql-formats">
                <button className="ql-list" value="bullet" data-tooltip='Bullet' />
                <button className="ql-list" value="ordered"  data-tooltip='Ordered'/>
            </span>
            <span className="ql-formats ql-all-fonts">

                <select className="ql-font" defaultValue="Inter" data-tooltip='Font Style'>
                    {fontFamily.map((font, index) => (
                        <option key={index} value={font}>{font}</option>
                    ))}
                </select>
                <select className="ql-size" defaultValue="14px" data-tooltip='Font Size'>
                    {fontSizeArr.map((size, index) => (
                        <option className="ql-size-item" key={index} value={size}>{size}</option>
                    ))}
                </select>
            </span>
            <span className="ql-formats">
                <button className="ql-undo" data-tooltip='Undo'>
                    <CustomUndo />
                </button>
                <button className="ql-redo" data-tooltip='Redo'>
                    <CustomRedo />
                </button>
            </span>

        </div>

    </div>
);

export default QuillToolbar;
