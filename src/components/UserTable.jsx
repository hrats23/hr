'use client'

import { Table } from 'antd'
import React, { useEffect, useState } from 'react'
import { Colors } from '@/StyleComponet/Index';
// import AdminAction from './Table/AdminAction';
// import TableDropDown from './Table/TableDropDown';
import { get } from '@/service/baseApi';
import { useUser } from '@/context/User';
import { useQuery } from 'react-query';
import dynamic from 'next/dynamic';

const AdminAction = dynamic(() => import('./Table/AdminAction'), { ssr: false });
const TableDropDown = dynamic(() => import('./Table/TableDropDown'), { ssr: false });

const columns = [
  {
    title: 'S.no',
    dataIndex: 'key',
    key: 'key',
    render: (text, record, index) => <p key={index}>{record.serial_number}</p>,
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: (text) => <p>{text}</p>,
    width: 150
  },
  {
    title: 'User Role',
    dataIndex: 'role',
    key: 'role',
    width: 140,
    render: (_, user) => <TableDropDown rolesList={user.rolesList} DefaultValue={user.role} user={user} />,
  },
  {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
  },
  {
    title: 'Employee ID',
    dataIndex: 'employeeId',
    key: 'employeeId',
  },
  {
    title: 'Mobile Number',
    dataIndex: 'mobile',
    key: 'mobile',
  },
  {
    title: 'Action',
    key: 'id',
    dataIndex: 'id',
    render: (_, user) => <AdminAction status={Boolean(user.status)} user={user} />
  },
];

function UserTable({ setTotalUser }) {
  const [TableData, setTableData] = useState([])
  const [totalPage, setTotalPage] = useState(0)
  const [curentPage, setCurentPage] = useState(1)
  const { user } = useUser()

  const { data, isLoading, isFetching } = useQuery(['getAllUsers', curentPage], 
  () => get(`/read-users?requestPage=${curentPage}&user_id=${user.user_id}`), {
    cacheTime: 0,
  })

  // console.log('TableData', TableData)


  useEffect(() => {
    const fetch = async () => {
      // console.log(data)
      setTotalPage(data?.pagination?.total)
      setTotalUser(data?.pagination?.total)
      setTableData(pre => {
        return data?.data?.map((item, i) => ({
          key: item?.employee_id,
          id: item?.user_id,
          name: `${item?.first_name} ${item?.last_name}`,
          role: item?.role_id,
          email: item?.email,
          mobile: item?.mobile,
          employeeId: item?.employee_id,
          status: item?.status,
          rolesList: data?.roles,
          curentPage: data?.pagination?.current,
          serial_number: item?.serial_number
        }))
      })
      // console.log('res', res.roles)
    }
    fetch()
  }, [data])

  return (

    <Table
      columns={columns}
      dataSource={TableData || []}
      loading={isLoading || isFetching}
      pagination={{
        total: totalPage,
        current: curentPage,
        // disabled: true,
        onChange: (page) => {
          setCurentPage(page)
          // refetch()
        },
        style: {
          paddingRight: '40px',
        }
      }}
      scroll={{
        x: 1000,
      }} />
  )
}

export default UserTable