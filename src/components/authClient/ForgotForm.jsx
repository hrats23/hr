'use client'

import { Button, ForgotBtnText, ForgotDiv } from '@/StyleComponet/Auth/Index'
import React, { useEffect, useState } from 'react'
import Success from '@/assets/svg/Success'
import { useRouter } from 'next/router'
import AuthWelcome from '../reusable/AuthWelcome'
import TextInput from '../reusable/AuthInput'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import Link from 'next/link'
import { Colors } from '@/StyleComponet/Index'
import { post } from '@/service/baseApi'
import { message } from 'antd'

const schema = yup.object({
  email: yup.string().required('Please enter a valid company email id').matches(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/g, 'Please enter a valid email id').transform(val => val.toLowerCase()),
})

function ForgotFrom() {

  const [ifSuccess, setIfSuccess] = useState(false)
  const [isLoading, setIsloading] = useState(false)

  const router = useRouter()

  const { control, handleSubmit, formState: { errors, isValid }, getValues } = useForm({ resolver: yupResolver(schema), mode: 'onChange', defaultValues: { email: '', } })

  const onSubmit = async(data) => {

    if(getValues('email') === 'hrats@siamcomputing.com') return null
    setIsloading(true)
    const res = await post('/forgot-password', data)
    // console.log('res', res)
    if (res.status) {
      // console.log('okay', data)

      setIfSuccess(true)
      const intervel = setTimeout(() => {
        router.push({
          pathname: '/verify',
          query: {
            email: data.email
          }
        })
        // setIfSuccess(false)
        clearInterval(intervel)
      }, 2000)

    } else {
      message.error(res.errors.email || 'Something went wrong')
    }
    setIsloading(false)
    // e.preventDefault()


  }
  return (
    <>
      {ifSuccess ?
        <>
          <Success />
          <AuthWelcome title='OTP has been sent in your email' subTitle='Check your email and follow instruction' />
        </> :
        <>
          <ForgotDiv >
            <AuthWelcome title='Forgot password?' subTitle='Please enter the email address and follow the instructions in your email.' />
            <form onSubmit={handleSubmit(onSubmit)}>
              <Controller
                control={control}
                name='email'
                render={({ field }) =>
                  <TextInput label='Email ID' name='email' placeholder='Enter Email' field={{ ...field }} error={errors.email} />
                }
              />
              <Button type='submit' disabled={getValues('email') === 'hrats@siamcomputing.com'||isLoading || !isValid }>Request</Button>
              <ForgotBtnText $side='center'><Link style={{ color: Colors.loginBtnColor }} href='/login'>Back to login</Link></ForgotBtnText>
            </form>
          </ForgotDiv>
        </>}
    </>
  )
}

export default ForgotFrom