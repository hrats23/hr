'use client'

import React, { useState } from 'react'
import AuthWelcome from '../reusable/AuthWelcome'
import { Button, AuthSubTitle, AuthTextInput, Otpdiv, AuthTextLabel, AuthVerifyForm, ErrorDiv, ErrorText, ForgotBtnText } from '@/StyleComponet/Auth/Index'
import OtpInput from 'react-otp-input';
import { useRouter } from 'next/router';
import { Colors } from '@/StyleComponet/Index';
import useSize from '@/hooks/useSize';
import { message } from 'antd';
import { post } from '@/service/baseApi';

function VerifyForm() {
    const [otp, setOtp] = useState('')
    const [isLoading, setIsloading] = useState(false)
    const [isDisabled, setIsDisabled] = useState(false)
    const [error, setError] = useState({
        status: false,
        message: 'Enter the OTP received on your email'
    })

    const router = useRouter()
    const { email } = router.query
    const { height, width } = useSize()

    // console.log(decodeURIComponent(email))

    const onSubmit = async () => {
        // e.preventDefault()
        const payload = {
            email: decodeURIComponent(email),
            otp
        }
        setIsloading(true)
        const res = await post('/verify-otp', payload)

        // console.log('res', res)
        if (res.status) {
            router.push({
                pathname: '/password',
                query: {
                    otp: encodeURIComponent(otp)
                }
            })
        } else {
            // message.error()
            setError({
                status: true,
                message: res?.errors?.email?.[0] || res?.message
                    || 'OTP verification failed'
            })
        }
        setIsloading(false)
    }

    const handleResendOtp = async () => {
        const data = {
            email : decodeURIComponent(email)
        }
        setIsloading(true)
        const res = await post('/forgot-password', data)
        // console.log('res', res)
        if (res.status) {
            // console.log('okay', data)
            message.success(res.message || 'OTP sent successfully')
            setIsDisabled(true)
            const intervel = setTimeout(() => {
                setIsDisabled(false)
                console.log('okay')
                clearInterval(intervel)
            }, 60000)

        } else {
            message.error(res.message || 'Something went wrong')
        }
        setIsloading(false)
    }

    const handleChanges = (value) => {
        // e.prevantDefault()
        // console.log(e)
        setError(pre => ({
            ...pre,
            status: false
        }))
        setOtp(value)
    }
    return (
        <>
            <AuthWelcome title='Verify your email address' />
            <AuthSubTitle fontSize='14px' style={{ marginTop: '14px' }} $maxwidth='436px'>We just emailed a six-digit code to {decodeURIComponent(email)}</AuthSubTitle>
            <AuthVerifyForm>
                <AuthTextInput>
                    <Otpdiv>

                        <AuthTextLabel>Enter OTP</AuthTextLabel>

                        <OtpInput
                            value={otp.toString()}
                            onChange={handleChanges}
                            numInputs={6}
                            inputType='number'
                            containerStyle={{
                                marginTop: '8px'
                            }}
                            // renderSeparator={<span>-</span>}
                            // inputStyle={{ width: width < 1280 ? '40px' : '56px', height: width < 1280 ? '40px' : '54px', fontSize: '16px', border: '1px solid', borderColor: error.status ? Colors.errorColor : Colors.blackText, borderRadius: '8px', marginRight: '9px', }}
                            // inputStyle={{ width: width < 1280 ? '40px' : '56px', height: width < 1280 ? '40px' : '54px', fontSize: '16px', border: '1px solid', borderColor: error.status ? Colors.errorColor : Colors.blackText, borderRadius: '8px', marginRight: '9px', }}
                            renderInput={(props) => <input {...props} />}
                        />

                        {error.status && <ErrorDiv>
                            <ErrorText>{error.message}</ErrorText>
                        </ErrorDiv>}
                    </Otpdiv>
                </AuthTextInput>
                <Button type={'button'} disabled={isLoading || !(otp.length >= 6)} onClick={() => onSubmit()}>Verify</Button>
                <ForgotBtnText $side='center' style={{ fontSize: '14px', opacity: isDisabled ? '0.5' : '1' }} onClick={() => isDisabled ? null : handleResendOtp()}>
                    <span style={{cursor:isDisabled ? 'not-allowed': 'pointer'}}>Resend OTP</span>
                </ForgotBtnText>
            </AuthVerifyForm>
        </>
    )
}

export default VerifyForm