'use client'

import { useRouter } from 'next/router'
import React, { useState } from 'react'
import AuthWelcome from '../reusable/AuthWelcome'
import AuthInput from '../reusable/AuthInput'
import Success from '@/assets/svg/Success'
import { Button, AuthVerifyForm } from '@/StyleComponet/Auth/Index'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import Link from 'next/link'
import { post } from '@/service/baseApi'
import { message } from 'antd'

const schema = yup.object({
    password: yup.string().matches(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$*&!.])[A-Za-z\d@$*&!.]{8,}$/g, 'Require 8+ characters with uppercase, lowercase, number, and special character.'),
    newPassword: yup.string().oneOf([yup.ref('password')], 'Both passwords must match'),
})

function PasswordForm() {
    const [ifSuccess, setIfSuccess] = useState(false)
    const [isLoading, setIsloading] = useState(false)

    const router = useRouter()
    const {otp} = router.query

    const { control, handleSubmit, formState: { errors, isValid } } = useForm({ resolver: yupResolver(schema), mode: 'onChange', defaultValues: { password: '', newPassword: '' } })

    const onSubmit = async(data) => {
        // console.log('okay', data)

        delete data.newPassword
        const payload = {
            ...data,
            otp
        }
        // console.log(payload)
        
        setIsloading(true)
        const res = await post('/reset-password', payload)

        if(res.status){
            setIfSuccess(true)
            const interver = setInterval(() => {
                router.push('/login')
                // setIfSuccess(false)
                clearInterval(interver)
            }, 3000)
        } else {
            message.error(res.message || 'Something went wrong')
        }
        setIsloading(false)
    }

    return (
        <>
            {ifSuccess ?
                <>
                    <AuthWelcome title='Welcome !' downTitle='Login in to continue' />
                    <Success style={{ width: '115px', height: '115px', margin: '30px' }} />
                    <AuthWelcome title='Password changed' subTitle='Your password has been successfully changed' />
                    <Button onClick={() => router.push('/login')}>Login</Button>
                </> :
                <>
                    <AuthWelcome title='Set new password' />
                    <AuthVerifyForm onSubmit={handleSubmit(onSubmit)}>
                        <Controller
                            control={control}
                            name='password'
                            render={({ field }) =>
                                <AuthInput label='New Password' field={{ ...field }} placeholder='Enter password' password={true} error={errors.password} marginTop={true} />}
                        />

                        <Controller
                            control={control}
                            name='newPassword'
                            render={({ field }) =>
                                <AuthInput label='Confirm Password' field={{ ...field }} placeholder='Enter password' password={true} error={errors.newPassword} marginTop={true} />
                            }
                        />

                        <Button type='submit' disabled={isLoading || !isValid}>Change Password</Button>
                    </AuthVerifyForm>
                </>
            }
        </>
    )
}

export default PasswordForm