'use client'

import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import AuthWelcome from '../reusable/AuthWelcome'
import AuthInput from '../reusable/AuthInput'
import { AuthForm, Button, ForgotBtnText, LoginFromContainer } from '@/StyleComponet/Auth/Index'
import { useUser } from '@/context/User'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import Link from 'next/link'
import { Colors } from '@/StyleComponet/Index'
import { get, instance, post } from '@/service/baseApi'
import { message } from 'antd'
import axios from 'axios'

const schema = yup.object({
  email: yup.string().required('Email is required').matches(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/g, 'Please enter a valid email id').transform(val => val.toLowerCase()),

  password: yup.string().required('Password is required').matches(/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[@$*&!.])[A-Za-z\d@$*&!.]{8,}$/g, 'Require 8+ characters with uppercase, lowercase, number, and special character.'),
})

function LoginFrom() {
  const route = useRouter()
  const { user, setUser } = useUser()
  const [isLoading, setIsloading] = useState(false)

  const { control, handleSubmit, formState: { errors, isValid }, getValues, watch } = useForm({ resolver: yupResolver(schema), mode: 'onChange', defaultValues: { email: '', password: '' } })

  const onSubmit = async (data) => {
    // console.log('okay', data)

    setIsloading(true)
    const res = await post('/login', data)
    if (res.status) {
      // console.log(res)
      localStorage.setItem('token', res.token)
      localStorage.setItem('user_id', res.user_id)
      setUser({
        isLogin: res?.status,
        ...res
      })

      instance.defaults.headers.common['Authorization'] = `Bearer ${res.token}`
      if (res?.role_name === 'Super Admin') {
        route.push('/admin')
      } else if (res?.role_name === 'hr_recruiter') {
        route.push('/')
      }

    } else {
      message.error(res?.message || 'Something went wrong')
    }
    setIsloading(false)
  }

  // console.log(watch('email'))
  return (
    <>
      {/* <LoginFromContainer style={{ maxWidth: '384px' }}> */}
      <div style={{ paddingBottom: '40px' }}>
        <AuthWelcome title='Welcome!' downTitle='Login in to continue' />
      </div>
      <AuthForm onSubmit={handleSubmit(onSubmit)}>
        <Controller
          control={control}
          name='email'
          render={({ field }) =>
            <AuthInput label='Email ID' field={{ ...field }} placeholder='name@example.com' error={errors.email} />}
        />

        <Controller
          control={control}
          name='password'
          render={({ field }) =>
            <AuthInput label='Password' field={{ ...field }} placeholder='Enter Password' password={true} marginTop={true} error={errors.password} />} />
        <ForgotBtnText

          style={{
            opacity: watch('email') !== 'hrats@siamcomputing.com' ? 1 : .5,
            marginTop: '30px'
            // display: watch('email') !== 'hrats@siamcomputing.com' ? 'block' : 'none',
          }} $side='end'>
          <Link style={{ color: Colors.loginBtnColor, cursor: watch('email') !== 'hrats@siamcomputing.com' ? 'pointer' : 'not-allowed', }}
            href={watch('email') !== 'hrats@siamcomputing.com' ? '/forgot' : '/login'}>Forgot Password?</Link></ForgotBtnText>
        <Button type='submit' disabled={isLoading || !isValid} >Login</Button>
      </AuthForm>
      {/* </LoginFromContainer> */ }
    </>
  )
}

export default LoginFrom