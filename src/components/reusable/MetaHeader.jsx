import Head from 'next/head'
import React from 'react'

function MetaHeader(props) {
    const { title, content } = props
  return (
    <Head>
      <title>{title}</title>
      {/* <link rel="profile" href="https://gmpg.org/xfn/11" /> */}
      <meta name={title} content="Home" />
    </Head>
  )
}

export default MetaHeader