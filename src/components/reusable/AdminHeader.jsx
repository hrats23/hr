import { Colors } from '@/StyleComponet/Index'
import React from 'react'
import { styled } from 'styled-components'
import BackArrow from '@/assets/svg/BackArrow'
import Avatar from '@/assets/svg/Avatar'
import Home from '@/assets/svg/Home'
import PhotoProfile from '@/assets/svg/PhotoProfile'


const Header = styled.header`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${Colors.adminGray};`


const HeaderContainer = styled.div`
width: 100%;
  max-width: 1500px;
  /* background-color: blue; */
  height: 119px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  `

const NavDiv = styled.div`
  display: flex;
  justify-content:space-between;
  align-items: center;
`


const ArrowDiv = styled.div`
  background-color: #D9D9D9;
  /* border-radius: 34px 0px 41px 0px; */
  display: flex;
  flex-direction: row;
  justify-content: center;
  border-radius: 32px;
  height: 48px;
  width: 48px;
  align-items: center;
  padding: 12px;
  margin-left: 39px;
`


const DashDiv = styled.div`
  background-color: #D9D9D9;
  margin-left: 57px;
  height: 44px;
  width: 143px;
  border-radius: 2px;
  align-items: center;
  padding: 10px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  gap:10px;
  margin-left: 57px;
  /* font-family: Manrope; */
  font-size: 18px;
  font-weight: 700;
`

const ProfileDiv = styled.div`
  background-color:#FFFFFF;
  display: flex;
  justify-content: center;
  align-items: center;
  /* border-radius: 2.58px; */
  border-radius: 11px;
  height: 49px;
  width: 53px;
  margin-top: 39px;
  margin-right: 70px;
`


function AdminHeader() {
  return (
    <Header>
      <HeaderContainer>
        <NavDiv>
          <ArrowDiv>
            <BackArrow/>
          </ArrowDiv>

          <DashDiv>
              <Home/>  DashBoard
          </DashDiv>
          
         </NavDiv>

          <ProfileDiv>
            <PhotoProfile/>
          </ProfileDiv>





        {/* <ul>
          <li><a href="#"><Home/>DashBoard</a></li>
          <li><a href="#"> <Avatar/> </a></li>
        </ul> */}
      </HeaderContainer>
    </Header>
  )
}

export default AdminHeader