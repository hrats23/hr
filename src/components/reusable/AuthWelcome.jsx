import React from "react";

import {
  AuthSubTitle,
  AuthTitle,
  AuthWelcomeDiv,
} from "@/StyleComponet/Auth/Index";

function AuthWelcome({ title, downTitle, subTitle }) {
  return (
    <AuthWelcomeDiv>
      {title && <AuthTitle>{title}</AuthTitle>}
      {downTitle && <AuthTitle>{downTitle}</AuthTitle>}
      {subTitle && <AuthSubTitle>{subTitle}</AuthSubTitle>}
    </AuthWelcomeDiv>
  );
}

export default AuthWelcome;
