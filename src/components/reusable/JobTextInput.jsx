import React from 'react'
import { JobInputContainer, JobInputLabel, JobInputRange, JobLabelRequired, JobRangeNumberInput, JobTextInputs, OptionLabel } from '@/StyleComponet/hr';
import Select from "react-select";
import { Colors } from '@/StyleComponet/Index';
import { DatePicker } from 'antd';
import DateIcon from '@/assets/svg/DateIcon'
import dayjs from 'dayjs';
import { ErrorDiv, ErrorText } from '@/StyleComponet/Auth/Index';

const { RangePicker } = DatePicker

function JobTextInput(props) {
  const { lable, option, type, range, field, formateType, required, placeHolder, dropDown, list = [], error, placeHolderRangeFrom, placeHolderRangeTo } = props;

  return (
    <JobInputContainer>
      <JobInputLabel>{lable}{required && <JobLabelRequired>*</JobLabelRequired>} {option && <OptionLabel >( Optional )</OptionLabel>}</JobInputLabel>
      {type === 'range' && <JobInputRange>
        <JobRangeNumberInput placeholder={placeHolderRangeFrom} />
        -
        <JobRangeNumberInput placeholder={placeHolderRangeTo} />
      </JobInputRange>}

      {type === 'dropDown' &&
        <Select
          components={{
            IndicatorSeparator: () => null
          }}
          {...field}
          styles={{
            control: (baseStyles, state) => ({
              ...baseStyles,
              height: '56px',
              marginTop: '14px',
              borderRadius: '6px',
              border: `1px solid ${Colors.blackGray}`,
              boxShadow: 'none',
              '&:hover': {
                border: `1px solid ${Colors.blackGray}`
              },
              cursor: 'pointer',
            }),
            valueContainer: baseStyles => ({
              ...baseStyles,
              padding: '12px',
            }),
            option: (baseStyles, state) => ({
              ...baseStyles,
              height: '40px',
              cursor: 'pointer',
              background: state.isSelected ? Colors.loginBtnColor : state.isFocused ? Colors.lightPurple : Colors.white,
              // '&:hover': {
              //   background: state.selectOption ? Colors.lightPurple : Colors.white
              // },

            })
          }}
          placeholder={placeHolder}
          options={lable === 'User role' ? list.map((val, i) => ({ value: val.id, label: val.role_name }))
            : list.map((val, i) => ({ value: val, label: val }))}
        />
      }

      {type === 'date' && <DatePicker
        style={{
          height: '56px',
          padding: '20px 12px',
          marginTop: '14px',
          borderRadius: '4px',
          border: ` 1px solid ${Colors.blackGray}`,
          fontSize: '20px'
        }}
        {...field}
        disabledDate={(current) => current < dayjs().endOf('day')}
        format="MMM DD, YYYY"
      // suffixIcon={<DateIcon />}
      // suffixIcon={null}

      />
      }

      {type === undefined && <JobTextInputs {...field} type={formateType ? formateType : 'text'} style={{ textTransform: lable === 'Email ID' ? 'lowercase' : 'none'}}  placeholder={placeHolder} />}

      {error && <ErrorDiv>
        <ErrorText>{error.message}</ErrorText>
      </ErrorDiv>}
    </JobInputContainer>
  )
}

export default JobTextInput