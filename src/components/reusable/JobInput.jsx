import React from 'react';
// styled components
import { JobInputContainer, JobInputLabel, JobLabelRequired, JobTextInputs, OptionLabel } from '@/StyleComponet/hr';
import { ErrorDiv, ErrorText } from '@/StyleComponet/Auth/Index';
import { Colors } from '@/StyleComponet/Index';
// library
import Select from "react-select";
import { Controller } from 'react-hook-form';
import { ConfigProvider, DatePicker } from 'antd';
import dayjs from 'dayjs';
import { JobTextArea } from '@/StyleComponet/career';
import { number } from 'yup';
import { moneyDigit, removeComma } from '@/assets/dummyData';

export default function JobInput(props) {

    const { htmlFor, label, type, name, placeholder, errors, requiredMsg, minLengthMsg,
        minLengthValue, maxLengthValue, maxLengthMsg, patternValue, patternMsg, control, dataOptions, requiredIcon, optionLabel, keyName, defaultValue, textAreaResize, rows, disable, formate, setValue} = props

    return (
        <>
            <JobInputContainer>
                <JobInputLabel htmlFor={htmlFor}>
                    {label}{requiredIcon && <JobLabelRequired>*</JobLabelRequired>} {optionLabel && <OptionLabel >( Optional )</OptionLabel>}
                </JobInputLabel>

                <>
                    {
                        (type === 'text' || type === 'email' || type === 'number' || type === 'password') &&
                        <>
                            <JobTextInputs
                                type={type}
                                disabled={disable}
                                name={name}
                                placeholder={placeholder}
                                {...control.register(name, {
                                    required: requiredMsg,
                                    minLength: {
                                        value: minLengthValue,
                                        message: minLengthMsg
                                    },
                                    maxLength: {
                                        value: maxLengthValue,
                                        message: maxLengthMsg
                                    },
                                    pattern: {
                                        value: patternValue,
                                        message: patternMsg,
                                    },
                                })}
                            />
                        </>
                    }
                    {
                        (formate === 'salary') && <>
                        <JobTextInputs
                                type={type}
                                disabled={disable}
                                name={name}
                                placeholder={placeholder}
                                {...control.register(name, {
                                    required: requiredMsg,
                                    minLength: {
                                        value: minLengthValue,
                                        message: minLengthMsg
                                    },
                                    maxLength: {
                                        value: maxLengthValue,
                                        message: maxLengthMsg
                                    },
                                    pattern: {
                                        value: patternValue,
                                        message: patternMsg,
                                    },
                                    onChange: (e) => {
                                        if(formate === 'salary'){
                                            const remove = removeComma(e.target.value || '0')
                                            setValue(name, moneyDigit(remove))
                                            
                                        }
                                        
                                    }
                                })}
                            />
                        </>
                    }

                    {
                        type === 'dropDown' &&
                        <>
                            <Controller
                                name={name}
                                control={control}
                                rules={{ required: requiredMsg }}
                                render={({ field }) => (
                                    <Select
                                        isSearchable={false}
                                        {...field}
                                        components={{
                                            IndicatorSeparator: () => null
                                        }}
                                        isDisabled={disable}
                                        styles={{
                                            control: (baseStyles, state) => ({
                                                ...baseStyles,
                                                height: '56px',
                                                marginTop: '14px',
                                                borderRadius: '6px',
                                                border: `1px solid ${Colors.blackGray}`,
                                                boxShadow: 'none',
                                                '&:hover': {
                                                    border: `1px solid ${Colors.blackGray}`
                                                },
                                                cursor: 'pointer',
                                            }),
                                            dropdownIndicator: (base, state) => ({
                                                ...base,
                                                transform: state.selectProps.menuIsOpen ? 'rotate(180deg)' : null,
                                                transition: 'transform 0.2s',
                                            }),
                                            valueContainer: baseStyles => ({
                                                ...baseStyles,
                                                padding: '12px',
                                            }),
                                            placeholder: baseStyles => ({
                                                ...baseStyles,
                                                // lineHeight: '16.94px',
                                                color: Colors.radioLabel,
                                                textTransform: 'capitalize',
                                                lineHeight: '19.36px',
                                                color: '#7E7E7E'
                                            }),
                                            option: (baseStyles, state) => ({
                                                ...baseStyles,
                                                height: '40px',
                                                cursor: 'pointer',
                                                background: state.isSelected ? Colors.loginBtnColor : state.isFocused ? Colors.lightPurple : Colors.white,
                                            })
                                        }}
                                        placeholder={placeholder}
                                        options={dataOptions?.map((val, i) => ({ value: keyName ? val?.id : val, label: keyName ? val[keyName] : val }))}
                                    />
                                )}
                            />
                        </>
                    }

                    {
                        type === 'date' &&
                        <>
                            <Controller
                                name={name}
                                control={control}
                                rules={{ required: requiredMsg }}
                                render={({ field }) => (
                                    <ConfigProvider theme={{ token: { colorPrimary: Colors.loginBtnColor } }}>

                                    
                                    <DatePicker
                                        style={{
                                            height: '56px',
                                            padding: '20px 12px',
                                            marginTop: '14px',
                                            borderRadius: '4px',
                                            border: ` 1px solid ${Colors.blackGray}`,
                                            fontSize: '20px'
                                        }}
                                        disabled={disable}
                                        {...field}
                                        value={field.value}
                                        placeholder={placeholder}
                                        disabledDate={(current) => !defaultValue && current < dayjs().endOf('day')}
                                        format='MMM DD, YYYY'
                                        inputReadOnly
                                    />
                                    </ConfigProvider>
                                )}
                            />
                        </>
                    }

                    {
                        type === 'textarea' &&
                        <>
                            <JobTextArea
                                style={{ resize: textAreaResize }}
                                rows={rows}
                                name={name}
                                disabled={disable}
                                placeholder={placeholder}
                                {...control.register(name, {
                                    required: requiredMsg,
                                    minLength: {
                                        value: minLengthValue,
                                        message: minLengthMsg
                                    },
                                    maxLength: {
                                        value: maxLengthValue,
                                        message: maxLengthMsg
                                    },
                                    pattern: {
                                        value: patternValue,
                                        message: patternMsg
                                    },
                                })}
                            />
                        </>
                    }
                </>

                {errors[name] && <ErrorDiv><ErrorText>{errors[name].message}</ErrorText></ErrorDiv>}

            </JobInputContainer >
        </>
    )
}
