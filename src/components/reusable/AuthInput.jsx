import React from 'react'
import { AuthTextInput, AuthTextLabel, EmailInput, ErrorDiv, ErrorText, InputDiv } from '@/StyleComponet/Auth/Index'
import { Colors } from '@/StyleComponet/Index'
import OpenEye from '@/assets/svg/OpenEye'
import CloseEye from '@/assets/svg/CloseEye'

function TextInput({label, placeholder, marginTop,password, error, field}) {
  const [show, setShow] = React.useState(false)
  
  return (
    <AuthTextInput $topside={`${marginTop}`}>
        <AuthTextLabel>{label}</AuthTextLabel>
        <InputDiv 
        style={{borderColor: error ? Colors.errorColor : Colors.blackText}}>
          <input
            style={{ width: '100%', textTransform: label === 'Email ID' ? 'lowercase' : 'none' }}
            {...field}
            type={password ? show ? 'text' : 'password' : 'text'}
            placeholder={placeholder}
            />
          {password && <div tabIndex={0} role="button" onKeyDown={e => e.key === 'Enter' && setShow(pre => !pre)} onClick={() => setShow(pre => !pre)}>{show ? <OpenEye /> : <CloseEye />}</div>}
        </InputDiv>
          
            {error && <ErrorDiv>
            <ErrorText>{error.message}</ErrorText>
            </ErrorDiv>}
    </AuthTextInput>
  )
}

export default TextInput