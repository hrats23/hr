import { AvatarDiv, HeaderContainer, HeaderLogo, HeaderRouter, HeaderSide, HomeHeader, IconText, NavBars } from '@/StyleComponet/hr'
import React, { useEffect, useState } from 'react'
import Avatar from '@/assets/svg/Avatar'

import { useRouter } from 'next/router'
import { Colors } from '@/StyleComponet/Index'
import Link from 'next/link'
import Head from 'next/head'



function LayoutHeader({routerName}) {
  const router = useRouter()
  const [pathName, setPathName] = useState(router.pathname)

  useEffect(() => {
    setPathName(router.pathname)
  }, [router.pathname])

  return (
    <>
      {/* <Head>
        <title>{router.pathname.slice(1).toLocaleUpperCase()}</title>
        <meta name="Home" content="Home" />
      </Head> */}
      <HomeHeader>
        <HeaderContainer>
          <HeaderSide>
            <HeaderLogo src='https://siamcomputing.com/wp-content/uploads/2021/09/siam-computing-trans.png' alt='logo' onClick={() => router.push('/')}/>
            <HeaderRouter>
              {routerName?.map((item, index) => (
                <Link style={{ textDecoration: 'none'}} href={item.link[0]} key={index} >
                  <NavBars $path={`${item.link.includes(pathName)}`}>
                    <item.icon color={item.link.includes(pathName) ? Colors.loginBtnColor : Colors.blackText} />
                    <IconText $path={`${item.link.includes(pathName)}`}>{item.name}</IconText>
                  </NavBars>
                </Link>
              ))}
            </HeaderRouter>
          </HeaderSide>
          <AvatarDiv>
            <Avatar />
          </AvatarDiv>
        </HeaderContainer>
      </HomeHeader>
    </>
  )
}

export default LayoutHeader