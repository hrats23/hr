import { Colors } from '@/StyleComponet/Index'
import { RatioTitle, RadioItemInput, RatioListContainer, RadionItem, RadioItemLabel } from '@/StyleComponet/hr'
import { Skeleton } from 'antd'
import React, { useEffect, useState } from 'react'
import { Controller } from 'react-hook-form'

function RationList(props) {
    const { option, label, name, control, defaultValue, setFileterValue, isLoading, resetFilter, setResetFilter } = props
    const [optionSelected, setOptionSelected] = useState(defaultValue)

    useEffect(() => {
        if(resetFilter) {
            setOptionSelected(defaultValue)
            setResetFilter(false)
        }
    }, [resetFilter])

    const handleRadiotion = (value) => {
        setOptionSelected(value)
        setFileterValue(pre => ({
            ...pre,
            [name]: value
        }))
    }

    return (
        <div style={{ paddingLeft: '20px' }}>
            <RatioTitle>{label}</RatioTitle>
            {isLoading && <RatioListContainer>
                {Array(4).fill(0).map((item, index) => (
                <Skeleton.Input key={index} active  style={{ height: '20px'}}/>
            ))}
                </RatioListContainer>}
            <Controller
                name={name}
                control={control}
                render={({ field }) => (
                    <RatioListContainer {...field}>
                        {option?.map((item, index) =>
                            <RadionItem key={index} tabIndex={0} onKeyDown={(e) => e.key === 'Enter' && handleRadiotion(item.label)}>
                                <RadioItemInput 
                                    style={{ cursor: 'pointer' }}
                                    type="radio" id={item.value} 
                                    name={name} 
                                    value={item.label}
                                    onChange={(e) => handleRadiotion(e.target.value)}
                                    // defaultChecked={item.label === optionSelected}
                                    checked={item.label === optionSelected}
                                     />
                                <RadioItemLabel style={{
                                    cursor: 'pointer',
                                    color: item.label === optionSelected ? Colors.loginBtnColor : Colors.blackGray,
                                    fontWeight: item.label === optionSelected ? 500 : 400
                                }} htmlFor={item.value}>{item.value}</RadioItemLabel>
                            </RadionItem>
                        )}
                    </RatioListContainer>
                )}
            />

        </div>
    )
}

export default RationList