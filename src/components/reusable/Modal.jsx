import { ModelBackground, ModelContainer, ModelContent } from '@/StyleComponet/hr'
import React, { useEffect } from 'react'

function Modal({ children, open, close }) {

  useEffect(() => {
    //give code for prevent scroll in modal background
    // if (open) {
      window.addEventListener('scroll', (e) => {
        e.preventDefault()
        e.stopPropagation()
      })
    // }
  }, [open])
  
  return (
    <>
      {open &&
        <ModelBackground>
          <ModelContainer>
            <ModelContent>
              {children}
            </ModelContent>
          </ModelContainer>
        </ModelBackground>
      }
    </>
  )
}

export default Modal