import React from 'react'
import { ConfigProvider, DatePicker } from 'antd'
import { Colors } from '@/StyleComponet/Index'
import { Controller } from 'react-hook-form'
import { DropDownAside, JobInputLabel } from '@/StyleComponet/hr'
import dayjs from 'dayjs'

const { RangePicker } = DatePicker

function AsideDate(props) {
    const { label, name, control, placeholder, requiredMsg } = props
    return (
        <DropDownAside style={{
            marginTop: '0px',
            borderTop: 'none',
        }}>
            {/* <JobInputLabel style={{ fontWeight: '600', fontSize: '16px' }}>{label}</JobInputLabel> */}
            <Controller
                name={name}
                control={control}
                rules={{ required: requiredMsg }}
                render={({ field }) => (
                    <ConfigProvider theme={{ token: { colorPrimary: Colors.loginBtnColor } }}>
                    <RangePicker
                        
                        style={{
                            height: '44px',
                            padding: '20px 12px',
                            marginRight: '20px',
                            marginTop: '0px',
                            borderRadius: '4px',
                            border: ` 1px solid ${Colors.blackGray}`,
                            fontSize: '14px'
                        }}
                        // onOk={(date, dateString) => field.onChange(dateString)}
                        // showTime
                        {...field}
                        disabledDate={(current) =>current && current > dayjs().endOf('day')}
                        placeholder={placeholder}
                        format='DD-MM-YYYY'
                        inputReadOnly
                    />
                    </ConfigProvider>
                )}
            />
        </DropDownAside>
    )
}

export default AsideDate