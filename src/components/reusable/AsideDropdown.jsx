import { Colors } from '@/StyleComponet/Index';
import { DropDownAside, JobInputLabel, RatioTitle } from '@/StyleComponet/hr';
import React, { useEffect, useState } from 'react'
import { Controller } from 'react-hook-form';
import Select from "react-select";

function AsideDropdown(props) {
    const { label, list, placeHolder, name, control, isLoading, resetFilter } = props

    const [value, setValue] = useState(null)

    useEffect(() => {
        if (resetFilter) {
            console.log(value)
            setValue(null)
        }
    }, [resetFilter])
    
    return (
        <DropDownAside>
            {/* <JobInputLabel style={{ fontWeight: '500', fontSize: '16px', lineHeight: '24px', color: Colors.blackText }}>{label}</JobInputLabel> */}
            <RatioTitle>{label}</RatioTitle>
            <Controller
                name={name}
                control={control}
                render={({ field }) => (
                    <Select
                        {...field}
                        isSearchable={false}
                        components={{
                            IndicatorSeparator: () => null
                        }}
                        styles={{
                            control: (baseStyles, state) => ({
                                ...baseStyles,
                                height: '44px',
                                borderRadius: '6px',
                                marginRight: '20px',
                                border: `1px solid ${Colors.blackGray}`,
                                fontSize: '14px',
                                boxShadow: 'none',
                                '&:hover': {
                                    border: `1px solid ${Colors.blackGray}`
                                },
                            }),
                            dropdownIndicator: (base, state) => ({
                                ...base,
                                transform: state.selectProps.menuIsOpen ? 'rotate(180deg)' : null,
                                transition: 'transform 0.2s',
                            }),
                            valueContainer: baseStyles => ({
                                ...baseStyles,
                                marginRight: '20px',
                                // textTransform: 'uppercase',                                
                            }),
                            placeholder: baseStyles => ({
                                ...baseStyles,
                                lineHeight: '16.94px',
                                color: Colors.radioLabel,
                                // textTransform: 'capitalize'
                            }),
                            
                            option: (baseStyles, state) => ({
                                ...baseStyles,
                                height: '40px',
                                textTransform: 'uppercase',
                                background: state.isSelected ? Colors.loginBtnColor : state.isFocused ? Colors.lightPurple : Colors.white,
                                '&:hover': {
                                    background: state.selectOption ? Colors.lightPurple : Colors.white,
                                    color: Colors.loginBtnColor
                                },
                            }),
                            menu: (baseStyles, state) => ({
                                ...baseStyles,
                                width: '100%',
                                maxWidth: '240px',
                            })
                        }}
                        placeholder={placeHolder}
                        // isClearable={true}
                        value={value}
                        onChange={val => {
                            setValue(val)
                            field.onChange(val)
                        }}
                        
                        options={list?.map((val, i) => ({ value: val?.id, label: val?.department_name }))}
                    />
                    
                )}
            />

        </DropDownAside>
    )
}

export default AsideDropdown