import React from 'react'
import { AdminFromInputs, AdminInput, AdminLabel } from '@/StyleComponet/admin'
import Select from "react-select";
import { Colors } from '@/StyleComponet/Index';
import { ErrorDiv, ErrorText } from '@/StyleComponet/Auth/Index';

function AdminInputs(props) {
  const { lable, required, list, type, field, error } = props
  return (
    <AdminFromInputs>
      <AdminLabel>{lable}{required && '*'}</AdminLabel>
      {type !== 'dropDown' && <AdminInput placeholder={lable} {...field}/>}
      {type === 'dropDown' &&
        <Select
          components={{
            IndicatorSeparator: () => null
          }}
          {...field}
          styles={{
            control: (baseStyles, state) => ({
              ...baseStyles,
              height: '44px',
              marginTop: '12px',
              borderRadius: '0px',
              fontSize: '18px',
              fontWeight: '500',
              border: `1px solid ${Colors.inputBorder}`,
              boxShadow: 'none',
              '&:hover': {
                border: `1px solid ${Colors.inputBorder}`
              },
            }),
            valueContainer: baseStyles => ({
              ...baseStyles,
              // padding: '10px 16px',
            }),
            option: (baseStyles, state) => ({
              ...baseStyles,
              height: '40px',
              background: state.isSelected ? Colors.loginBtnColor : state.isFocused ? Colors.lightPurple : Colors.white,
              '&:hover': {
                background: state.selectOption ? Colors.lightPurple : Colors.white
              },

            })
          }}
          placeholder={lable}
          options={list.map((val, i) => ({ value: val, label: val }))}
        />
      }

      {error && <ErrorDiv>
            <ErrorText>{error.message}</ErrorText>
            </ErrorDiv>}
    </AdminFromInputs>
  )
}

export default AdminInputs