import React, { useEffect, useRef, useState } from 'react'
import { AllJobTitleContainer, CandidatesDiv, CloseLabel, DaysDiv, DetailsDiv, InterviewedDiv, JobTextColor, JobTextDiv, Jobdiv, LineDiv, MenuDiv, NextDiv, NumSpan, OpensDiv, PublishDateText, PublishDiv, ShortlistedDiv, SourcedDiv, ZeroDiv, ZeroOneDiv } from '@/StyleComponet/hr'
import { Dropdown, Menu } from 'antd'
import MenuBar from '@/assets/svg/MenuBar'
import { Button } from '@/StyleComponet/Auth/Index';
import WarningBox from '../modals/WarningBox';
import { Colors } from '@/StyleComponet/Index';
import { useRouter } from 'next/router';
import { JobCarMenuIcons } from '@/StyleComponet/candidate';
import { ContextMenuItem, useContextMenu } from 'use-context-menu';

function JobCards(props) {
    const { value } = props
    const [warningMessage, setWarningMessage] = useState({});
    const [boxOpen, setBoxOpen] = useState(false)
    const [menuOpen, setMenuOpen] = useState(false)
    const router = useRouter()


    function handleHoldJob(value, status) {

        // console.log(value)
        setBoxOpen(true)
        setWarningMessage({
            title: 'Are you sure?',
            subTitle: `Do you really want to hold ${value?.job_title} job `,
            id: 1,
            main: 'Confirm',
            concept: 'closeHiring',
            changeJobStatus: status,
            job_id: value?.id
        })
    }

    function handleCloseHiring(value, status) {
        // console.log(value)
        setBoxOpen(true)
        setWarningMessage({
            title: 'Are you sure?',
            subTitle: `Do you really want to close ${value?.job_title} job`,
            id: 1,
            main: 'Confirm',
            concept: 'closeHiring',
            changeJobStatus: status,
            job_id: value?.id
        })
    }

    function handleResuemeHiring(value, status) {

        setBoxOpen(true)
        setWarningMessage({
            title: 'Are you sure?',
            subTitle: `Do you really want to resume ${value?.job_title} job`,
            id: 1,
            main: 'Confirm',
            concept: 'closeHiring',
            changeJobStatus: status,
            job_id: value?.id
        })
    }

    function handleRepost(value, status) {

        setBoxOpen(true)
        setWarningMessage({
            title: 'Are you sure?',
            subTitle: `Do you really want to repost ${value?.job_title} job`,
            id: 1,
            main: 'Confirm',
            concept: 'closeHiring',
            changeJobStatus: status,
            job_id: value?.id
        })
    }
    function handleExport(value, status) {

        setBoxOpen(true)
        setWarningMessage({
            title: 'Are you sure?',
            subTitle: `Do you really want to export ${value?.job_title} job`,
            id: 1,
            main: 'Export',
            concept: 'closeHiring',
            // changeJobStatus: status,
            job_id: value?.id
        })
    }

    const handleAddJob = (id) => {
        // TODO: Implement the logic for adding a new job
        router.push(`/candidates/${id}?job_name=${value?.job_title}&jobtype=${value?.employment_name}`)
    };

    const handleEdit = (value) => {
        router.push(`/job/${value?.id}`)
    }

    // useEffect(() => {
    //     const handleKey = (e) => {
    //         const element = document.querySelectorAll('.ant-dropdown-menu-item-active')
    //         if(e.key === 'Enter' && element.length > 0){

    //             // element[0].onclick()
    //             // console.log(typeof element, element)


    //         }
    //     }

    //     addEventListener('keydown', handleKey)

    //     return () => {
    //         removeEventListener('keydown', handleKey)
    //     }

    // }, [])

    function handlekey(e) {
        e.preventDefault()
        console.log(e)
    }

    const handleMenuBarKeyDown = (event) => {
        if (event.key === 'Enter') {
            console.log("working")
        }
    };



    return (
        <>
            <Jobdiv >

                <PublishDiv>
                    <JobTextDiv>

                        <AllJobTitleContainer>
                            {value?.job_title}
                            <JobTextColor>{value?.department_name}</JobTextColor>
                        </AllJobTitleContainer>

                        <MenuDiv>

                            {value?.status == 'closed' &&
                                <CloseLabel>
                                    Closed
                                </CloseLabel>}
                            {value?.status == 'onhold' &&
                                <CloseLabel style={{
                                    color: Colors.holdLabel
                                }}>
                                    On hold
                                </CloseLabel>}


                            <Dropdown
                                trigger={['click']}
                                // open={menuOpen}
                                // onOpenChange={setMenuOpen}
                                menu={{
                                    items: value?.status === 'onhold' ? [
                                        {
                                            key: '0',
                                            label: (
                                                <a onClick={() => handleEdit(value)}
                                                    // role='button'
                                                    tabIndex={0}
                                                    onKeyDown={(e) => e.key === 'Enter' && handleEdit(value)}
                                                >
                                                    Edit
                                                </a>
                                            ),
                                        },
                                        {
                                            key: '1',
                                            label: (
                                                <a onClick={() => handleResuemeHiring(value, 'resume_hiring')}
                                                    onKeyDown={(e) => e.key === 'Enter' && handleResuemeHiring(value, 'resume_hiring')}
                                                // tabIndex={0} role='button'
                                                >
                                                    Resume hiring
                                                </a>
                                            ),
                                        },
                                        {
                                            key: '2',
                                            label: (
                                                <a onClick={() => handleCloseHiring(value, 'closed')}
                                                    onKeyDown={(e) => e.key === 'Enter' && handleCloseHiring(value, 'closed')}
                                                // tabIndex={0} role='button'
                                                >
                                                    Close hiring
                                                </a>
                                            ),
                                        },
                                        {
                                            key: '3',
                                            label: (
                                                <a onClick={() => handleExport(value, 'export')}
                                                    onKeyDown={(e) => e.key === 'Enter' && handleExport(value, 'export')}
                                                // tabIndex={0} role='button'
                                                >
                                                    Export
                                                </a>
                                            ),
                                        }
                                    ]
                                        :
                                        value?.status === 'closed' ?
                                            [
                                                {
                                                    key: '1',
                                                    label: (
                                                        <a onClick={() => handleRepost(value, 'reopen_job')}
                                                            onKeyDown={(e) => e.key === 'Enter' && handleRepost(value, 'reopen_job')}
                                                        // tabIndex={0} role='button'
                                                        >
                                                            Reopen job
                                                        </a>
                                                    ),
                                                },
                                                {
                                                    key: '4',
                                                    label: (
                                                        <a onClick={() => handleExport(value, 'export')}
                                                            onKeyDown={(e) => e.key === 'Enter' && handleExport(value, 'export')}
                                                        // tabIndex={0} role='button'
                                                        >
                                                            Export
                                                        </a>
                                                    ),
                                                }
                                            ]
                                            :
                                            [
                                                {
                                                    key: '1',
                                                    label: (
                                                        <a onClick={() => handleEdit(value)}
                                                            onKeyDown={(e) => e.key === 'Enter' && handleEdit(value)}
                                                        // tabIndex={0} role='button'
                                                        >
                                                            Edit
                                                        </a>
                                                    ),
                                                },
                                                {
                                                    key: '3',
                                                    label: (
                                                        <a onClick={() => handleHoldJob(value, 'onhold')}
                                                            onKeyDown={(e) => e.key === 'Enter' && console.log('onhold')}
                                                        // tabIndex={0} role='button'
                                                        >
                                                            On hold
                                                        </a>
                                                    ),
                                                },
                                                {
                                                    key: '2',
                                                    label: (
                                                        <a onClick={() => handleCloseHiring(value, 'closed')}
                                                            onKeyDown={(e) => e.key === 'Enter' && handleCloseHiring(value, 'closed')}
                                                        // tabIndex={0} role='button'
                                                        >
                                                            Close hiring
                                                        </a>
                                                    ),
                                                },
                                                {
                                                    key: '4',
                                                    label: (
                                                        <a onClick={() => handleExport(value, 'export')}
                                                            onKeyDown={(e) => e.key === 'Enter' && handleExport(value, 'export')}
                                                        // tabIndex={0} role='button'
                                                        >
                                                            Export
                                                        </a>
                                                    ),
                                                },
                                            ],
                                }}
                                placement="bottomLeft"
                            >
                                <MenuBar
                                    role='button'
                                    // tabIndex={0}
                                    // onKeyDown={(e) => e.key === 'Enter' && setMenuOpen(pre => !pre)}
                                    style={{
                                        cursor: 'pointer',
                                    }} />
                            </Dropdown>

                            {/* <div onClick={menu.onContextMenu} onKeyDown={menu.onKeyDown}>
                                
                            <MenuBar
                                // role='button'
                                // tabIndex={0}
                                // onContextMenu={onContextMenu}
                                // onKeyDown={onKeyDown}
                                // onKeyDown={(e) => e.key === 'Enter' && setMenuOpen(pre => !pre)}
                                style={{
                                    cursor: 'pointer',
                                }} />
                            </div> */}
                        </MenuDiv>

                    </JobTextDiv>
                </PublishDiv>

                <PublishDateText>
                    Published on: {value.publish_on_date} <ul><li>{value.employment_name}</li></ul>

                </PublishDateText>

                <CandidatesDiv>

                    <NextDiv>
                        <SourcedDiv> Sourced candidate</SourcedDiv>
                        <ZeroOneDiv>0</ZeroOneDiv>
                    </NextDiv>
                    <LineDiv></LineDiv>

                    <NextDiv>
                        <SourcedDiv> Shortlisted candidate</SourcedDiv>
                        <ZeroOneDiv>0</ZeroOneDiv>
                    </NextDiv>
                    <LineDiv></LineDiv>

                    <NextDiv>
                        <SourcedDiv>Interviewed candidate</SourcedDiv>
                        <ZeroOneDiv>0</ZeroOneDiv>
                    </NextDiv>


                </CandidatesDiv>

                <DaysDiv>
                    <OpensDiv>Day&apos;s Open: <NumSpan> {(value.days_open || 0).toString().padStart(2, '0')} </NumSpan></OpensDiv>

                    <DetailsDiv>

                        <Button
                            style={{
                                marginTop: '28px',
                                // marginBottom: '33px',
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'center',
                                gap: '10px',
                                backgroundColor: '#FFFFFF',
                                border: '1px solid #7852A9',
                                color: '#7852A9',
                                lineHeight: '24px',
                            }}
                            $topside={'0px'}
                            width={'213px'}
                            height={'40px'}
                            onClick={() => handleAddJob(value?.id)}
                        >
                            View details
                        </Button>

                    </DetailsDiv>
                </DaysDiv>
            </Jobdiv>
            <WarningBox open={boxOpen} close={() =>{ 
                setBoxOpen(false)
                setMenuOpen(false)
             }} warningMessage={warningMessage} />
        </>
    )
}

export default JobCards