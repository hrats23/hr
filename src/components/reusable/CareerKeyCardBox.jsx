import React from 'react'
import { KeyCardContainer, KeyCardIcon, KeyTitle, KeyTextContainer, KeySubTitle } from '@/StyleComponet/career'
import { Colors } from '@/StyleComponet/Index'


function CareerKeyCardBox({item, color:Colorvalue}) {
  return (
    <KeyCardContainer>
      <KeyCardIcon style={{backgroundColor: Colorvalue === 'blue' ? Colors.lightPurple : Colors.keyCardPink}}>
        <item.icon color={ Colorvalue === 'blue' ? Colors.loginBtnColor : Colors.careericon}/>
      </KeyCardIcon>
      <KeyTextContainer>
        <KeyTitle>{item.title}</KeyTitle>
        <KeySubTitle>{item.subTitle}</KeySubTitle>
      </KeyTextContainer>
    </KeyCardContainer>
  )
}

export default CareerKeyCardBox