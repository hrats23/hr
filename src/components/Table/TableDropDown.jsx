import React, { useEffect, useState, useRef } from 'react';
import { Dropdown, Space, message } from 'antd';
import AdminDropDownIcon from '@/assets/svg/AdminDropIcon';
import { put } from '@/service/baseApi';
import { useQueryClient } from 'react-query';

const TableDropDown = (props) => {
  const { user } = props;
  const [dropValue, setDropValue] = useState(props.DefaultValue);
  const queryClient = useQueryClient();
  const dropdownRef = useRef(null);
  const dropdownOpenRef = useRef(false);
  const dropdownItemsRef = useRef([]);

  useEffect(() => {
    setDropValue(props.DefaultValue);
  }, [props.DefaultValue]);

  const handleDropDown = async (item) => {
    setDropValue(item);
    const names = user.name.split(' ');
    const payload = {
      role_id: item,
      first_name: names[0],
      last_name: names[1],
      email: user.email,
      mobile: user.mobile,
      status: Boolean(user.status),
      employee_id: user.employeeId,
    };

    const res = await put(`/update-user/${user.id}`, payload);

    if (res.status) {
      message.success('User updated successfully');
      queryClient.invalidateQueries('getAllUsers');
    } else {
      message.error('Something went wrong');
    }
  };

  const handleItemClick = (item) => {
    handleDropDown(item);
    dropdownRef.current.click();// Close the dropdown after selection
  };

  const dropdownapi = () => {
    const selectedValue = document.activeElement.getAttribute('data-value');
    console.log(document.activeElement)  
    // dropdownRef.current.click();
  };

  const handleKeyDown = (e) => {
    const selectedValue = document.activeElement.getAttribute('data-value');
    console.log('selectedValue', selectedValue, 'dropdownOpenRef.current', dropdownOpenRef.current);
    if (e.key === 'Enter' && !dropdownOpenRef.current) {
      // Open the dropdown when the "Enter" key is pressed and the dropdown is not open
      dropdownRef.current.click();
      e.preventDefault(); // Prevent the default behavior of the "Enter" key
    } 
    else if (e.key === 'ArrowDown' && dropdownOpenRef.current) {
      // Navigate down through the dropdown items using the "Arrow Down" key
      e.preventDefault(); // Prevent page scrolling
      const currentIndex = dropdownItemsRef.current.indexOf(document.activeElement);
      const nextIndex = currentIndex === -1 ? 0 : currentIndex + 1;
      if (nextIndex < dropdownItemsRef.current.length) {
        dropdownItemsRef.current[nextIndex].focus();
      }
    } else if (e.key === 'ArrowUp' && dropdownOpenRef.current) {
      // Navigate up through the dropdown items using the "Arrow Up" key
      e.preventDefault(); // Prevent page scrolling
      const currentIndex = dropdownItemsRef.current.indexOf(document.activeElement);
      console.log('currentIndex', currentIndex)
      const prevIndex = currentIndex === -1 ? 0 : currentIndex - 1;
      console.log('prevIndex', currentIndex)
      if (prevIndex >= 0) {
        dropdownItemsRef.current[prevIndex].focus();
      }
    } 
    else if (e.key === 'Enter' && dropdownOpenRef.current) {
      // Select an item when the "Enter" key is pressed and the dropdown is open
      console.log('selectedValue', selectedValue);
      if (selectedValue !== null) {
        handleItemClick(selectedValue);
        dropdownRef.current.click(); // Close the dropdown after selection
      }
    }
  };

  const DropDownItem = props.rolesList?.map((item, i) => ({
    key: item.id,
    label: (
      <a
        key={i}
        data-value={item?.id}
        onClick={() => handleItemClick(item?.id)}
        onKeyDown={handleKeyDown}
        tabIndex={0}
        role="button"
        ref={(el) => (dropdownItemsRef.current[i] = el)}
      >
        {item?.role_name || 'Select Role'}
      </a>
    )
  }));

  return (
    <Dropdown
      menu={{
        items: DropDownItem,
      }}
      trigger={['click']}
      onOpenChange={(visible) => {
        dropdownOpenRef.current = visible; // Update the dropdown open state
      }}
    >
      <Space
        ref={dropdownRef}
        tabIndex={0}
        onKeyDown={handleKeyDown}
        style={{
          cursor: 'pointer',
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
      >
        {props.rolesList?.find((item) => item.id === dropValue)?.role_name || 'Select Role'}
        <AdminDropDownIcon
          style={{
            marginRight: '10px',
          }}
        />
      </Space>
    </Dropdown>
  );
};

export default TableDropDown;
