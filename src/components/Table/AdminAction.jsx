'use client'

import React, { useEffect, useState } from 'react'
import AdminEdit from '@/assets/svg/AdminEdit'
import AdminDeleteIcon from '@/assets/svg/AdminDeleteIcon'
import AdminResetIcon from '@/assets/svg/AdminResetIcon'
import { Colors } from '@/StyleComponet/Index'
import { Switch, Tooltip, message } from 'antd'
import { AdminActionIcons } from '@/StyleComponet/admin'
// import WarningBox from '../modals/WarningBox'
import { get } from '@/service/baseApi'
// import AddAdminUser from '../modals/AddAdminUser'
import { useQueryClient } from 'react-query'
import dynamic from 'next/dynamic'

const WarningBox = dynamic(() => import('../modals/WarningBox'), { ssr: false })
const AddAdminUser = dynamic(() => import('../modals/AddAdminUser'), { ssr: false })

const icons = [AdminEdit, AdminDeleteIcon, AdminResetIcon, 'switch']

function AdminAction(props) {
    const { user } = props
    const [active, setActive] = useState(props?.status)
    const [boxOpen, setBoxOpen] = useState(false)
    const [warningMessage, setWarningMessage] = useState({})
    const [addUserModal, setAddUserModal] = useState(false)
    const [edit, setEdit] = useState(false)

    const queryClient = useQueryClient()

    useEffect(() => {
        setActive(props?.status)
    }, [props?.status])

    const handleDelete = () => {
        // console.log('Delete', props.user.id)
        setWarningMessage({
            title: 'Are you sure?',
            subTitle: 'Do you really want to delete the user from Super admin?',
            id: props?.user?.id,
            main: 'delete',
            email: props?.user?.email
        })
        setBoxOpen(true)
    }

    const handleReset = () => {
        // console.log('Reset', props.user?.id)  
        setWarningMessage({
            title: 'Are you sure?',
            subTitle: 'Do you really want to reset the user password?',
            id: props?.user?.id,
            main: 'reset',
            email: props?.user?.email
        })
        setBoxOpen(true)
    }

    const handleSwitch = async () => {
        // console.log('Switch', props.user?.id)
        setActive(pre => !pre)
        const res = await get(`/read-users?&status=${!active}&user_id=${props?.user?.id}`)
        if (res.status) {
            message.success(res.message || 'Status changed successfully')
        } else {
            message.error(res.message || res.error || 'Something went wrong')
            setActive(pre => !pre)
        }
        queryClient.invalidateQueries('getAllUsers')
    }

    const handleEdit = () => {
        // console.log('Edit', props.user)
        setAddUserModal(true)
        setEdit(true)
    }

    const tabIndex = (e, index) => {
        if(e.key === 'Enter') {
            e.preventDefault()
            // console.log(index)
            switch(index) {
                case 0:
                    handleEdit()
                    break;
                case 1:
                    handleDelete()
                    break;
                case 2:
                    handleReset()
                    break;
                default:
                    break
            }
        }
    }

    return (
        <>
            <AdminActionIcons>
                {icons.map((Icon, index) => Icon !== 'switch' ? <Tooltip key={index} title={index === 0 ? 'Edit' : index === 1 ? 'Delete' : 'Reset'}><Icon

                    tabIndex={0}
                    onKeyDown={(e) => tabIndex(e, index)}
                    style={{
                        cursor: 'pointer',
                    }}

                    onClick={index === 0 ? handleEdit : index === 1 ? handleDelete : index === 2 ? handleReset : null} /> </Tooltip>
                    :
                    <Tooltip key={index} title={active ? 'Disable' : 'Enable'}><Switch
                        key={index}
                        size='small'
                        onChange={() => handleSwitch()}
                        checked={active}
                        style={{
                            background: active ? Colors.loginBtnColor : '#4D525D',
                        }}
                    /></Tooltip>)}
            </AdminActionIcons>
            <WarningBox open={boxOpen} close={() => setBoxOpen(false)} warningMessage={warningMessage} />
            <AddAdminUser open={addUserModal} close={() => setAddUserModal(false)} edit={edit} setEdit={setEdit} defaultUser={props.user} />
        </>
    )
}

export default AdminAction