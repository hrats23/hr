import { Spin } from 'antd'
import React from 'react'
import { styled } from 'styled-components'

const LoadinDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100vw;`

function LoadingScreen() {
  return (
    <LoadinDiv>
      <Spin size='large'/>
    </LoadinDiv>
  )
}

export default LoadingScreen