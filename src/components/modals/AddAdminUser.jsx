import { Modal } from 'antd'
import React, { useEffect } from 'react'
import AdminAddCloseIcon from '@/assets/svg/AdminAddCloseIcon'
import { AddUserForm, AddUserFromContainer, AdminBtn, AdminFormBtnContainer } from '@/StyleComponet/admin'
import AdminInputs from '../reusable/AdminInputs'
import { status } from '@/assets/dummyData'
import { Controller, useForm } from 'react-hook-form'
import { yupResolver } from "@hookform/resolvers/yup"
import * as yup from "yup"
import { InputContainer, JobFrom, JobFromContainer } from '@/StyleComponet/hr'
import { Button, CancelBtn } from '@/StyleComponet/Auth/Index'
import JobTextInput from '../reusable/JobTextInput'
import { get, post, put } from '@/service/baseApi'
import { message } from 'antd'
import { useQueryClient } from 'react-query'

const schema = yup.object({
    first_name: yup.string().min(3, 'Minimum 3 characters').max(10, 'Maximum 10 characters'),
    last_name: yup.string().min(3, 'Minimum 3 characters').max(10, 'Maximum 10 characters'),
    role_id: yup.object().shape({
        value: yup.string().required('Please select your role'),    
        label: yup.string().required('Please select your role')
    }),
    status: yup.object().shape({
        value: yup.string().required('Please select your status'),
        label: yup.string().required('Please select your status')
    }),
    email: yup.string().required('Please enter your email').matches(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/g, 'Please enter a valid email id').transform(val => val.toLowerCase()),
    employee_id: yup.string().required('Please enter your employee id').max(10, 'Employee id must be 10 digits'),
    mobile: yup.string().matches(/^\d{10}$/, 'Mobile number must be 10 digits').required('Mobile number is required'),
})



function AddAdminUser(props) {
    const queryClient = useQueryClient()

    const { open, close, edit, setEdit, defaultUser } = props
    const { control, handleSubmit, formState: { errors, isValid }, setValue, reset, setError } = useForm({ resolver: yupResolver(schema), mode: 'onChange' })
    const [userRoles, setUserRoles] = React.useState([])
    const [isLoading, setIsloading] = React.useState(false)

    useEffect(() => {
        const fetch = async () => {
            const res = await get('/read-roles')
            // console.log('/read-roles', res)
            if (res.status) {
                setUserRoles(res.message)
            }
        }
        if (open || edit) fetch()
    }, [edit, open])



    useEffect(() => {
        if (edit) {
            const role = userRoles.find(item => item?.id === defaultUser.role)
            const defaultStatus = status.find(item => item === (defaultUser.status === 1 ? 'Enabled' : 'Disabled'))

            setValue('status', {
                value: defaultStatus,
                label: defaultStatus
            })
            setValue('role_id', {
                value: role?.id,
                label: role?.role_name
            })
            setValue('first_name', defaultUser?.name.split(" ")?.[0])
            setValue('last_name', defaultUser?.name.split(" ")?.[1])
            setValue('email', defaultUser?.email)
            setValue('mobile', defaultUser?.mobile)
            setValue('employee_id', defaultUser?.employeeId)
        }
    }, [edit, userRoles])


    // console.log('error', errors)
    const onSubmit = async (data) => {
        // console.log('data', data)
        const payload = {
            role_id: parseInt(data.role_id.value),
            first_name: data.first_name,
            last_name: data.last_name,
            email: data.email,
            mobile: data.mobile,
            status: data.status.value === "Enabled",
            employee_id: data.employee_id
        }

        // console.log('okay', payload)

        setIsloading(true)
        const res = edit ? await put(`/update-user/${defaultUser.id}`, payload) : await post('/add-user', payload)
        // localStorage.SetItem('token', res.token)
        if (res?.status) {
            console.log(res)
            queryClient.invalidateQueries('getAllUsers')
            message.success(res?.message)
            close()
        } else {
            message.error(res?.message || res?.errors?.role_id?.[0] || res?.errors?.first_name?.[0] ||
                res?.errors?.last_name?.[0] || res?.errors?.email?.[0] ||
                res?.errors?.mobile?.[0] || res?.errors?.status?.[0] ||
                 res?.errors?.employee_id?.[0]  || 'Something went wrong')
        }
        setIsloading(false)
            
    }
    
    const handleClose = () => {
        reset({
            first_name: '',
            last_name: '',
            role_id: {
                value: '',
                label: ''
            },
            status: {
                value: '',
                label: ''
            },
            email: '',
            employee_id:'',
            mobile: '',
        })
        close()
    }


    return (
        <Modal title={`${edit ? 'Edit' : 'Add'} user`}
            className='add-user-modal'
            width={'1042px'}
            open={open}
            closeIcon={<AdminAddCloseIcon />}
            onCancel={ () => handleClose()} footer={null}>

            <JobFrom onSubmit={handleSubmit(onSubmit)}>
                <JobFromContainer style={{ padding: '0px 75px 46px 75px' }}>
                    <InputContainer>
                        <Controller
                            control={control}
                            name='first_name'
                            render={({ field }) => <JobTextInput lable='First name' required={true} placeHolder='First name' field={field} error={errors.first_name} />}
                        />
                        <Controller
                            control={control}
                            name='last_name'
                            render={({ field }) => <JobTextInput lable='Last name' required={true} placeHolder='Last name' field={field} error={errors.last_name} />}
                        />
                    </InputContainer>
                    <InputContainer>
                        <Controller
                            control={control}
                            name='role_id'
                            render={({ field }) => <JobTextInput lable='User role' required={true} placeHolder='User role' type='dropDown' list={userRoles} field={field} error={errors?.role_id?.label} />}
                        />
                        <Controller
                            control={control}
                            name='status'
                            render={({ field }) => <JobTextInput lable='Status' required={true} placeHolder='Status' type='dropDown' list={status} field={field} error={errors?.status?.label} />}
                        />

                    </InputContainer>
                    <InputContainer>
                        <Controller
                            control={control}
                            name='email'
                            render={({ field }) => <JobTextInput lable='Email Id' required={true} placeHolder='name@example.com' field={field} error={errors.email} />}
                        />
                        <Controller
                            control={control}
                            name='employee_id'
                            render={({ field }) => <JobTextInput lable='Employee Id' required={true} placeHolder='Employee Id' field={field} error={errors.employee_id} />}
                        />

                    </InputContainer>
                    <InputContainer>
                        <Controller
                            control={control}
                            name='mobile'
                            rules={{
                                maxLength: {
                                    value: 10,
                                    message: 'Please enter a valid mobile number'
                                },
                            }}
                            render={({ field }) => <JobTextInput lable='Mobile number' formateType='number' required={true} placeHolder='1234567890' field={field} error={errors.mobile} />}
                        />
                    </InputContainer>
                    <InputContainer style={{ justifyContent: 'center', gap: '12px' }}>
                        <CancelBtn type='button' onClick={() => handleClose()} width={'217px'}>Cancel</CancelBtn>
                        <Button type='submit' disabled={isLoading || !isValid} width={'217px'}>{edit ? 'Update' : 'Add'} User</Button>
                    </InputContainer>
                </JobFromContainer>
            </JobFrom>
            
        </Modal>
    )
}

export default AddAdminUser