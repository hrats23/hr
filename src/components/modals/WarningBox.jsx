import { Modal, message } from 'antd'
import React, { useEffect, useState } from 'react'
import DeleteModalClose from '@/assets/svg/DeleteModalClose'
import { WarningBoxDiv, WarningBtns, WarningSubTitle, WarningTitle } from '@/StyleComponet/admin'
import { Button, CancelBtn } from '@/StyleComponet/Auth/Index'
import { deleteMethod, get, post } from '@/service/baseApi'
import { useUser } from '@/context/User'
import { useMutation, useQueryClient } from 'react-query'
import { useRouter } from 'next/router'

function WarningBox(props) {

    const { open, close, warningMessage, menuClose } = props
    const [isLoading, setIsLoading] = useState(false)

    const { user, setUser } = useUser()
    const router = useRouter()
    const queryClient = useQueryClient()

    const {mutate, data, isLoading: loading} = useMutation(({id, status}) => get(`/view_all_jobs?job_id=${id}&status=${status}`), {
        onSettled: () => {
            queryClient.invalidateQueries('allJobs')
            close()
        }, 
        onSuccess: (data) => {
            message.success(data.message || 'Job status changed successfully')
            close()
        } ,
        onError: (error) => {
            message.error('Something went wrong')
        }
    })

    // console.log(isLoading, loading)
    const handleConfirm = async () => {
        // console.log(warningMessage.main, warningMessage.id)

        // console.log('okay')
        if (warningMessage.main === 'delete') {
            setIsLoading(true)
            // deleteMutate()
            const res = await deleteMethod(`/delete-user/${warningMessage.id}`)
            // console.log(res)
            if (res.status) {
                queryClient.invalidateQueries(['getAllUsers'])
                message.success(res.message || 'User deleted successfully')
                close()
            } else {
                message.error(res.message || 'Something went wrong')
            }
            setIsLoading(false)
        } else if (warningMessage.main === 'reset') {
            setIsLoading(true)
            const res = await post('/change-password', { email: warningMessage?.email })
            // console.log(res)
            if (res.status) {
                queryClient.invalidateQueries(['getAllUsers'])
                message.success(res.message || 'Password reset successfully')
                close()
            } else {
                message.error(res.message || 'Something went wrong')
            }
            setIsLoading(false)
        } else if (warningMessage.main === 'logout') {
            // console.time()
            setIsLoading(true)
            // console.log('logout')
            localStorage.removeItem('token')
            localStorage.removeItem('user_id')
            setUser({
                isLogin: false,
                role_name: ''
            })
            setIsLoading(false)
            // console.timeEnd()
            
            router.push('/login')
        } else if (warningMessage.changeJobStatus) {
            // setIsLoading(true)
            mutate({id: warningMessage.job_id, status: warningMessage.changeJobStatus})
            // setIsLoading(false)
        }   else if (warningMessage.main === 'Export') {

            setIsLoading(true)
            window.open(`${process.env.NEXT_PUBLIC_BASEAPI}single_job_export/${warningMessage.job_id}?token=${localStorage.getItem('token')}`)
            setIsLoading(false)
            close()
        }
        // setIsLoading(false)
        // close()

    }


    return (
        <Modal
            className='delete-modal'
            width={'465px'}
            open={open}
            closeIcon={<DeleteModalClose />}
            onCancel={close} footer={null}>
            <WarningBoxDiv>
                <WarningTitle>{warningMessage.title}</WarningTitle>
                {warningMessage.subTitle && <WarningSubTitle>{warningMessage.subTitle}</WarningSubTitle>}
                <WarningBtns>
                    <CancelBtn style={{ fontSize: '14px', fontWeight: '500' }} type='button' width={'180px'} $topside={'0px'} onClick={close}>Cancel</CancelBtn>
                    <Button disabled={isLoading || loading} style={{ textTransform: 'capitalize', fontSize: '14px', fontWeight: '500' }} width={'180px'} $topside={'0px'} onClick={() => handleConfirm()}>{warningMessage.main}</Button>
                </WarningBtns>
            </WarningBoxDiv>

        </Modal>
    )
}

export default WarningBox