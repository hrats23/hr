import React, { useState } from 'react'
import { Modal, Skeleton } from 'antd'
import AdminAddCloseIcon from '@/assets/svg/AdminAddCloseIcon'
import { JobPreview, JobPreviewDescription, JobPreviewLocation, JobPreviewTitle } from '@/StyleComponet/candidate'
import { CareerKeyCardList, CareerKeyDetailsContainer, CareerKeyTitle } from '@/StyleComponet/career'
import EmployemenType from '@/assets/svg/EmployementTypeIcon'
import WorkModal from '@/assets/svg/WorkModal'
import WorkExprience from '@/assets/svg/WorkExperience'
import NoticePeriod from '@/assets/svg/NoticePeriod'
import AnualRange from '@/assets/svg/AnualRange'
import NumberOpening from '@/assets/svg/NumberOpening'
import Education from '@/assets/svg/Education'
import CareerKeyCardBox from '../reusable/CareerKeyCardBox'
import { get } from '@/service/baseApi'
import { moneyDigit } from '@/assets/dummyData'

const keyDetails = [
    {
        title: 'Employement Type',
        icon: EmployemenType,
        subTitle: 'Full Time'
    }, {
        title: 'Work Modal', 
        icon: WorkModal,
        subTitle: 'Remote'
    },{
        title: 'Work Experience',
        icon: WorkExprience,
        subTitle: '1 year'
    }, {
        title: 'Notice Period',
        icon: NoticePeriod,
        subTitle: '1 month'
    }, {
        title: 'Annual Range',
        icon: AnualRange,
        subTitle: '1 year'
    }, {
        title: 'Number of Openings',
        icon: NumberOpening,
        subTitle: '1'
    }, {
        title: 'Education',
        icon: Education,
        subTitle: 'B.E'
    }
]

function ViewJobModal(props) {
    const { open, close ,id } = props
    const [jobData, setJobData] = useState({})
    const [details, setDetails] = useState(keyDetails)
    const [isLoading, setIsLoading] = useState(false)

    React.useEffect(() => {
        const fetch = async() => {
            setIsLoading(true)
            const res1 = await get('add_new_job')
            const res = await get(`/view_job/${id}`)

            const list = res1?.data

            // console.log('list', list)
            // console.log('res', res)

            setJobData(res.job)
            setDetails(pre => {
                const temp = [...keyDetails]
                
                temp[0].subTitle = list?.employmentTypes?.filter(item => item?.id === res?.job?.employment_type_id)[0]?.employment_name
                temp[1].subTitle = list?.workingModes?.filter(item => item?.id === res?.job?.working_mode_id)[0]?.working_mode_name
                temp[2].subTitle = res.job.max_experience
                temp[3].subTitle = list?.notice_period?.filter(item => item?.id === res?.job?.notice_period)[0]?.day
                temp[4].subTitle = `${moneyDigit(res.job.min_ctc)} - ${moneyDigit(res.job.max_ctc)}`
                temp[5].subTitle = list?.no_of_openings?.filter(item => item?.id === res?.job?.no_of_openings)[0]?.opening
                temp[6].subTitle = list?.educations?.filter(item => item?.id === res?.job?.education_id)[0]?.education_type

                // console.log(list?.no_of_openings?.filter(item => item?.id === 2)[0]?.opening, list?.educations?.filter(item => item?.id === 2)[0]?.education_type)
                
                return temp
            })
            setIsLoading(false)
        }
        if(open) fetch()

    }, [open, id])
    return (
        <Modal
            title={`Job Preview Details`}
            className='job-modal'
            width={'924px'}
            open={open}
            closeIcon={<AdminAddCloseIcon />}
            onCancel={() => close()} footer={null}>
            <JobPreview>
                    {isLoading ? <Skeleton.Input active/> : <JobPreviewTitle>{jobData?.job_title}</JobPreviewTitle>}
                    {isLoading ? <><br/><Skeleton.Input active style={{marginTop: '8px'}}/></> : <JobPreviewLocation>{jobData?.location_name}</JobPreviewLocation>}
                <CareerKeyDetailsContainer style={{
                    paddingTop: '28px'
                }}>
                    <CareerKeyTitle>
                        Key details
                    </CareerKeyTitle>
                    <CareerKeyCardList>
                        {isLoading ? <Skeleton.Input active /> : keyDetails?.map((item, index) => (
                                <CareerKeyCardBox color='blue' item={item} key={index} />
                        ))}
                    </CareerKeyCardList>
                </CareerKeyDetailsContainer>
                    <div className="ql-editor" style={{ height: '100%'}}>

                    {isLoading ? <Skeleton active style={{paddingTop: '40px'}}/> : <JobPreviewDescription style={{ paddingTop: '28px'}} dangerouslySetInnerHTML={{ __html: (jobData?.job_description || '') }}></JobPreviewDescription>}
                    </div>
                                    
            </JobPreview>
        </Modal>
    )
}

export default ViewJobModal