import { JobDescriptionContainer, JobDescriptionList, JobDescriptionSubTitle, JobDescriptionTitle, JobModelFooter, JobModelHeaderContainer, JobModelTitle, JobParagraph, JobSideDiv, ModelBackground, ModelContainer, ModelContent, Radio, RadioContainer, RadioLabel, SearchInput, SearchList, TempSearchContainer } from '@/StyleComponet/hr'
import { Button, CancelBtn } from '@/StyleComponet/Auth/Index'
import React, { useEffect, useState } from 'react'
import SearchIcon from '@/assets/svg/SearchIcon'
import { jobDescription } from '@/assets/dummyData'
import { Colors } from '@/StyleComponet/Index'
import useSize from '@/hooks/useSize'
import Exit from '@/assets/svg/Exit'
// import Modal from '../reusable/Modal'
import { Modal } from 'antd'
import { get } from '@/service/baseApi'

function JobDescriptionTemp(props) {
    const { width } = useSize()
    const { open, close, setDefaultDescription } = props
    const [jobTemp, setJobTemp] = useState(0)
    const [jobTemplist, setJobTemplist] = useState([])
    const [search, setSearch] = useState('')

    useEffect(() => {
     jobTempApi()
    }, [])

    useEffect(() => {
        const select = jobTemplist.filter(val => val?.description_name.toLowerCase().includes(search.toLowerCase()))
        setJobTemp(select[0]?.id)
    }, [search])

    const jobTempApi = () => {
        get('/read-job-descriptions').then((res) => {
            // console.log(message)
            setJobTemplist(res?.message)
            setJobTemp(res?.message?.[0]?.id)
        })
    }

    const handleOnChange = (value) => {
        // console.log(jobTemplist?.filter(item => item?.id === value)[0])
        setJobTemp(value)
        // setJobDescription(jobTemplist?.filter(item => item?.id === value)[0])
    }

    const handleButton = () => {
        setDefaultDescription(jobTemplist?.filter(item => item?.id === jobTemp)[0])
        close()
        setSearch('')
    }

    const handleClose = () => {
        setSearch('')
        close()
    }


    return (
        <>
            <Modal open={open} className='job-template-select' onCancel={close} footer={null} width={1319} centered={true}>
                <JobModelHeaderContainer>
                    <JobModelTitle>
                        Job description templates
                    </JobModelTitle>
                    <Exit style={{ cursor: 'pointer' }} onClick={close} />
                </JobModelHeaderContainer>

                <JobDescriptionContainer >
                    <JobSideDiv 
                        $widthprops={'472px'}
                        $paddingProp={'36px 78px 36px 40px'} >
                        <TempSearchContainer>
                            <SearchIcon />
                            <SearchInput placeholder='Search for a template' onChange={(e) => setSearch(e.target.value)} />
                        </TempSearchContainer>
                        <SearchList>
                            {jobTemplist?.filter(val => val?.description_name.toLowerCase().includes(search.toLowerCase()))?.map((item, index) =>
                                <RadioContainer key={index} onClick={() => handleOnChange(item?.id)}>
                                    <Radio type='radio' name='job_description'
                                        id={item?.id}
                                        checked={item?.id === jobTemp}
                                        // onChange={(e) => handleOnChange(e.target.value)}
                                        value={item?.id} />
                                    <RadioLabel htmlFor={item?.id}>{item?.description_name}</RadioLabel>
                                </RadioContainer>
                            )}
                        </SearchList>
                    </JobSideDiv>
                    <JobSideDiv
                        style={{
                            borderLeft: `1px solid ${Colors.boxBorder}`
                        }}
                        $widthprops={'847px'}
                        $paddingProp={'24px 38px'}>
                        {jobTemplist?.filter(item => item?.id === jobTemp).map((item, index) => {
                            return (
                                <div key={index}>
                                    <JobDescriptionTitle><RenderHtml val={item?.description_context}/></JobDescriptionTitle>
                                    {/* {item.content.map((val, i) => <DescriptionTemp key={i} val={val}/>)} */}
                                </div>
                            )
                        })}
                    </JobSideDiv>
                </JobDescriptionContainer>
                <JobModelFooter>
                    <CancelBtn style={{
                        fontWeight: '700',
                        lineHeight: '24px',
                        letterSpacing: '-0.72px'
                    }} $topside={'0px'} width={'217px'} onClick={handleClose} >Cancel</CancelBtn>
                    <Button style={{
                        fontWeight: '700',
                        lineHeight: '24px',
                        letterSpacing: '-0.72px'
                    }} type='button' $topside={'0px'} width={'217px'} onClick={handleButton}>Use template</Button>
                </JobModelFooter>
            </Modal>

        </>
    )
}

function RenderHtml ({ val }) { 
    return (
        <div dangerouslySetInnerHTML={{ __html: val }} />
    )
}


export default JobDescriptionTemp