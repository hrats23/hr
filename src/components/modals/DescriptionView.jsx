import React, { useEffect, useState } from 'react';
import QuillToolbar, { formats, modules1 } from '@/hooks/CustomToolBar';
import { ToolbarParent, ZoomIcons } from '@/StyleComponet/Auth/Index';
import { JobModelFooter } from '@/StyleComponet/hr';
import { Button, CancelBtn } from '@/StyleComponet/Auth/Index';
import { Controller } from 'react-hook-form';
import ZoomIn from '@/assets/svg/ZoomIn';
import ReactQuill from 'react-quill';
import Popup from 'reactjs-popup';

import 'reactjs-popup/dist/index.css';

function DescriptionView(props) {
    const { open, placeholder, name, closePopup, control, content, setContent } = props;
    const [previousValue, setPreviousValue] = useState(content); // Previous saved value
    const [currentValue, setCurrentValue] = useState(''); // Current value in the editor

    useEffect(() => {
        setPreviousValue(content);
        setCurrentValue(content);
    }, [content, open]);

    const contentSave = () => {
        setContent(currentValue);
        setPreviousValue(currentValue);
        closePopup();
    };

    const handleClose = () => {
        setContent(previousValue); // Restore the previous value
        closePopup();
    };

    return (
        <Popup open={open} contentStyle={{ padding: '0px', }}>
            <ToolbarParent >
                <ZoomIcons type="button" $tooltips="Minimize"  onClick={() => handleClose(close)}>
                    <ZoomIn />
                </ZoomIcons>
                <QuillToolbar id={'t2'} />
            </ToolbarParent>

            <Controller
                name={name}
                control={control}
                render={({ field }) => (
                    <ReactQuill
                        theme="snow"
                        placeholder={placeholder}
                        value={currentValue}
                        onChange={(value) => {
                            setCurrentValue(value);
                            field.onChange(value)
                        }}
                        onBlur={field.onBlur}
                        modules={modules1}
                        formats={formats}
                    />
                )}
            />

            <JobModelFooter>
                <CancelBtn $topside={'0px'} width={'217px'} onClick={() => handleClose(close)}>
                    Cancel
                </CancelBtn>
                <Button $topside={'0px'} width={'217px'} onClick={() => contentSave(close)}>
                    Save
                </Button>
            </JobModelFooter>
        </Popup>
    );
}

export default DescriptionView;