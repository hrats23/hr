import React, { useEffect, useState } from 'react';
import QuillToolbar, { formats, modules, redoChange, undoChange } from "@/hooks/CustomToolBar";
import DescriptionView from "./modals/DescriptionView";
import { ToolbarParent, ZoomIcons } from "@/StyleComponet/Auth/Index";
import ZoomOut from '@/assets/svg/ZoomOut'
import ReactQuill from 'react-quill';
import { Controller } from 'react-hook-form';
import { ErrorDiv, ErrorText } from '@/StyleComponet/Auth/Index';

function MyEditor(props) {
  const { name, placeholder, errors, requiredMsg, control, defaultDescription, description, setDescription } = props;
  const [descView, setDescView] = useState(false);

  useEffect(() => {
    setDescription(defaultDescription?.description_context);
  }, [defaultDescription])

  return (
    <div className="text-editor">

      <ToolbarParent>
        <ZoomIcons type="button" $tooltips="Maximize" onClick={() => setDescView(pre => !pre)}>
          <ZoomOut />
        </ZoomIcons>
        <QuillToolbar id={'t1'} />
      </ToolbarParent>

      <Controller
        name={name}
        control={control}
        rules={{ required: requiredMsg }}
        render={({ field }) => (
          <div>
            <ReactQuill
              theme="snow"
              value={description}
              placeholder={placeholder}
              // value={content}
              onChange={(value) => {
                setDescription(value)
                field.onChange(value)
              }}
              onBlur={field.onBlur}
              modules={modules}
              formats={formats}
            />
            {errors[name] && <ErrorDiv><ErrorText>{errors[name].message}</ErrorText></ErrorDiv>}
          </div>
        )}
      />

      <DescriptionView
        open={descView}
        closePopup={() => setDescView(pre => !pre)}
        // control={control}
        content={description}
        setContent={setDescription}
        name={name}
        requiredMsg={requiredMsg}
        control={control}
        placeholder={placeholder}
      />


    </div>
  )
}

export default MyEditor