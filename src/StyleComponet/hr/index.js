import { styled } from "styled-components";
import { Colors, size } from "../Index";

export const HomeHeader = styled.header`
display: flex;
justify-content: center;
align-items: center;
height: 82px;
    background-color: ${Colors.whiteText};
    border: 0px, 0px, 1px, 0px;
    border-bottom: 1px solid #EBEBEB;
    `

export const HeaderContainer = styled.div`
    height: 82px;
    padding: 16px 40px;
    width: 100%;
    display: flex;
    max-width: 1700px;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    background-color: ${Colors.whiteText};
    /* background-color: red; */
    border-bottom: 1px solid #EBEBEB;
    position: fixed;
    /* top: 0; */
    z-index: 10;
`

export const Main = styled.main`
        height: 100%;
        background-color: ${Colors.authbg};
        /* padding: 24px 40px 38px 40px; */
        display: flex;
        /* background-color: red; */
        flex-direction: row;
        align-items: center;
        justify-content: center;`

export const HomeScreen = styled.div`
    display: flex;
    height: calc(100vh - (24px + 24px + 38px));
    max-height: 941px;
    /* height: 100vh; */
    width: 100%;
    max-width: 1700px;
  
    `

export const Homediv =styled.section`

width: 100%;
margin-left: 20px;
    overflow-y: scroll;
    /* &::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none; 
    scrollbar-width: none;  */
`


export const Aside = styled.aside`
    max-height: 941px;
    max-width: 280px;
    width: 100%;
    height: 100%;
    overflow-y: scroll;
    /* margin-right: 20px; */
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    border-radius: 4px;
    background: #FFF;
    box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.10);
    &::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none; 
    scrollbar-width: none; 
    `

export const AsideContainer = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    /* align-items: start; */`

export const LogoutText = styled.span`
    font-weight: 500;
    font-size: 18px;
    line-height: 21.78px;
    letter-spacing: 0.18px;
    color: ${Colors.logoutGray};
    @media (max-width: ${size.laptop}){
        font-size: 16px;
    }
`

export const AsideBtnContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border-bottom: 1px solid #EBEFF2`


export const Outbtn =styled.button`
    /* margin-top: 112px;  */
    font-weight: 500;
    background-color: transparent;
    font-size: 18px;
    display: flex;
    flex-direction: row;
    gap: 9px;
    width: fit-content;
    align-items: center;
    color: #747474;
    cursor: pointer;
    @media (max-height: 870px){
        margin-top: 100px;
    }
`   

export const AsideListContainer = styled.div`
    /* background-color: blue; */
    /* margin-left: 20px; */
    `

        export const AsideFilterHeader = styled.div`
            display: flex;
            flex-direction: row;
            /* background-color: red; */
            justify-content: space-between;
            align-items: center;
            padding: 20px;`

export const RatioTitle = styled.p`
    font-weight: 600;
    font-size: 16px;
    color: ${Colors.blackText};
    line-height: 24px; 
    @media (max-width: ${size.laptop}){
        font-size: 14px;
        line-height: 20px;
    }`

export const RatioListContainer = styled.div`
    display: flex;
    margin-top: 16px;
    flex-direction: column;
    gap: 24px;`

export const RadionItem = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    gap: 12px;`

export const RadioItemLabel = styled.label`
    font-weight: 400;
    line-height: 24px;
    font-size: 16px;
    text-transform: lowercase;
    &::first-letter {
        text-transform: uppercase
    };
    @media (max-width: ${size.laptop}){
        font-size: 14px;
        line-height: 20px;
    }
    `

export const RadioItemInput = styled.input`
    width: 20px;
    height: 20px;
    accent-color: #7852A9;
    @media (max-width: ${size.laptop}){
        width: 16px;
        height: 16px;
    }
    `

export const DropDownAside = styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
    margin-top: 28px;
    padding-top: 19px;
    padding-left: 20px;
    margin-bottom: 35px;
    border-top: 1px solid #EBEFF2;`


export const Middlediv =styled.div`
    
    /* display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;  */

    width: 100%;
    display: flex;
    flex-wrap: wrap;
    row-gap: 10px;
    border-radius: 4px;
    color: #000;
    border-top: ${props => props.$topborder === 'true' ? '1px solid #000' : ''};
    flex-direction: row;
    background-color: ${props => props.$colorbackground === 'true' ? Colors.whiteText : 'transparent'};
    padding: 12px 20px;
    align-items: center;
    justify-content: space-between;
    box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.10);
`

export const MiddleHeaderText =styled.p`
    font-size: 24px;
    font-weight: 400;
    color: ${Colors.blackText};
    font-style: normal;
    line-height: 24px;
    letter-spacing: -0.96px;
    @media (max-width: ${size.laptop}){
        font-size: 20px;
    }
`


export const Carddiv =styled.div`
    display: flex;
    /* flex-direction: row; */
    flex-wrap: wrap;
    justify-content: space-evenly;
    gap: 16px;
    padding-bottom: 20px;
    /* margin-left: 20px;
    margin-right: 40px; */
`



export const Jobdiv =styled.div`
    /* width: 1140px;
    height: 62px; */
    max-width: 498px;
    width: 100%;
    margin-top: 16px;
    /* margin-left: 300px; */
    border-radius: 4px;
    background-color: #FFFFFF;
    box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.10);
    padding: 18px;
    /* display: flex; */
    /* flex-direction: row; */
    /* justify-content: space-between; */
    `

export const PublishDiv =styled.div`
    /* display: flex;
    flex-wrap: wrap;
    align-items: center;
    flex-direction: row;
    justify-content: space-between; */
`





export const JobTextDiv =styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    text-align: start;
    justify-content: space-between;`

export const AllJobTitleContainer = styled.div`
    /* max-width: 370px; */
    font-size: 20px;
    font-weight: 600;
    line-height: 24px;
    letter-spacing: 0em;
    color: ${Colors.blackText};
    @media (max-width: ${size.laptop}){
        font-size: 18px;
        line-height: 20px;
    }
    
    `

export const AllJobTitle = styled.div``

export const JobTextColor =styled.span`
    width: 47px;
    height: 24px;
    font-weight: 500;
    font-size: 14px;
    color: #7852A9;
    text-transform: uppercase;
    background-color: #EFE6FD;
    text-align: center;
    margin-left: 6px;
    border-radius: 9px;
    line-height: 24px;
    letter-spacing: -0.56px;
     display:inline-block;
    /* margin-top: 18px; */
`

export const MenuDiv =styled.div`
    display: flex;
    flex-direction: row;
    gap: 8px;
`

export const CloseLabel = styled.p`
    width: 67px;
    /* background-color: red; */
    justify-content: end;
    display: flex;
    /* justify-content: center; */
    align-items: start;
    font-weight: 400;
    color: ${Colors.errorColor};
    line-height: 24px;
    font-size: 18px; 
    @media (max-width: ${size.laptop}){
        font-size: 16px;
    }`


export const PublishDateText =styled.div`
    margin-top: 8px;
    font-size: 14px;
    font-weight: 400;
    color: #747474;
    line-height: 24px;
    letter-spacing: 0em;
    /* list-style: disc; */
    li{list-style: disc !important ;}
    display: flex;
    /* flex-wrap: wrap; */
    gap: 30px;
`

export const CandidatesDiv =styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    /* flex-wrap: wrap; */
    width: 100%;
    margin-top: 20px;
`


export const SourcedDiv =styled.div`
    font-size: 14px;
    font-weight: 400;
    line-height: 24px;
    color: #747474;
    /* line-height: 24px; */
    /* margin-right: 12px; */
`


export const ShortlistedDiv =styled.div`
    font-size: 14px;
    font-weight: 400;
    color: #747474;
    margin-right: 12px;
    margin-left: 12px;
    letter-spacing: 0em;
`


export const InterviewedDiv =styled.div`
    font-size: 14px;
    font-weight: 400;
    color: #747474;
    margin-left: 12px;
    letter-spacing: 0em;
`

export const LineDiv =styled.div`
    border: 0.5px solid #D9D9D9;
    height: 36px;
    margin-top: 8px;
`

export const DaysDiv =styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`

export const OpensDiv =styled.div`
    font-size: 16px;
    font-weight: 700;
    line-height: 24px;
    display: flex;
    flex-direction: row;
    color: ${Colors.blackText};
    align-items: center;
    margin-top: 36px;
    @media (max-width: ${size.laptop}){
        font-size: 14px;
    }
`

export const NumSpan =styled.span`
    font-size: 16px;
    font-weight: 500;
    color: #2EBA6A;
    margin-left: 8px;

    /* color: '#2EBA6A'
    marginLeft: '8px' */
`




export const DetailsDiv =styled.div``




export const NextDiv =styled.div`
    display: flex;
    flex-direction: column;
    gap: 4px;
`






/* export const ViewButton =styled.button`
    background-color: #FFFFFF;
    border: 1px solid #7852A9;
` */


export const ZeroOneDiv =styled.div`
    font-weight: 600;
    font-size: 30px;
    color: ${Colors.blackText};
    line-height: 24px;
    letter-spacing: -1.2px;
    display: flex;
    flex-direction: column;
`

export const ZeroDiv =styled.div`
    font-weight: 600;
    font-size: 30px;
    display: flex;
    flex-direction: column;
    margin-left: 12px;
    /* margin-top: 4px; */
`



















export const Available =styled.div`
    font-size: 28px;
    font-weight: 400;
    margin-top: 39px;
    text-align: center;
`

export const HeaderLogo = styled.img`
    cursor: pointer;
    width: 87px;
    height: 50px;`

export const HeaderSide = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;`

export const HeaderRouter = styled.div`
    margin-left: 88px;
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;`

export const NavBars = styled.div`
    display: flex;
    flex-direction: row;
    gap: 10px;
    background-color: ${props => props.$path === 'true' ? Colors.lightPurple : 'transparent'};
    border-radius: 6px;
    padding: 10px;
    margin-right: 25px;
    align-items: center;`

export const IconText = styled.p`
margin-top: 4px;
font-size: 18px;
font-style: normal;
color: ${props => props.$path === 'true' ? Colors.loginBtnColor : Colors.blackText};
font-weight: ${props => props.$path === 'true' ? 700 : 400};
line-height: 24px; /* 133.333% */
@media (max-width: ${size.laptop}){
    font-size: 16px;
}
`

export const AvatarDiv = styled.div`
 width: 53px;
 height: 50px;
 display: grid;
 place-items: center;
 border-radius: 11px;
background: #FFF;
box-shadow: 0px 4px 15px 0px rgba(0, 0, 0, 0.08);
cursor: not-allowed;
`

export const JobContainer = styled.div`
    width: 100%;
    height: 100%;
    max-width: 1232px;
    /* background-color: red; */
    padding: 0px 53px 0px 53px`

export const JobHeader = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;`

export const StrickyJobHeader = styled(JobHeader)`
    position: sticky;
    top: 82px;
    background-color: ${Colors.authbg};
    /* background-color: red; */
    z-index: 1;
    /* border-radius: 10px; */
    padding: 28px 0px 28px 0px;`

export const BackBtnContainer = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: start;
    align-items: center;
    text-align: center;
    gap: 23px;`

export const BackText = styled.p`
color: ${Colors.blackText};
font-size: 24px;
font-style: normal;
font-weight: 700;
line-height: 24px; 
@media (max-width: ${size.laptop}){
    font-size: 20px;
}`

export const JobFrom = styled.form`
    width: 100%;`

export const JobFromContainer = styled.div`
    width: 100%;
    border-radius: 12px;
    padding: 24px 76px 46px 76px;
    background-color: ${Colors.whiteText};`

export const FromHeading = styled.p`
    color: ${Colors.loginBtnColor};
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: 24px; /* 100% */
    @media (max-width: ${size.laptop}){
        font-size: 20px;
    }
    `

export const InputContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
    /* background-color: red; */
    margin-top: 40px`

export const JobInputContainer = styled.div`
    width: 47%;
    display: flex;
    flex-direction: column;`

export const JobInputLabel = styled.label`
    color: ${Colors.blackText};
    font-weight: 700;
    line-height: 24px;
    font-size: 20px; 
    @media (max-width: ${size.laptop}){
        font-size: 16px;
        line-height: 20px;
    }`

export const JobLabelRequired = styled.span`
    color: ${Colors.errorColor}`

export const JobTextInputs = styled.input`
    width: 100%;
    height: 56px;
    padding: 20px 12px;
    margin-top: 14px;
    border-radius: 4px;
    border: 1px solid ${Colors.blackGray};
    font-size: 16px;
    font-style: normal;
    font-weight: 400;`

export const DropDownList = styled.div`
    position: absolute;
    width: 100%;
    height: 160px;
    border-radius: 6px;
    background: #FFF;
    overflow-y: scroll;
    box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.10);`

export const DropDownItem = styled.div`
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    height: 40px;
    /* background-color: red; */
    cursor: pointer;
    padding: 12px 10px;
    &:hover{
        background-color: ${Colors.lightPurple};
    }`

export const OptionLabel = styled.span`
    font-size: 20px;
    font-weight: 400;
    color: ${Colors.optionGray}`

export const JobInputRange = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between`

export const JobRangeNumberInput = styled.input`
    width: 47%;
    /* background-color: red; */
    height: 56px;
    padding: 20px 12px;
    margin-top: 14px;
    border-radius: 4px;
    border: 1px solid ${Colors.blackGray};
    font-size: 16px;
    font-style: normal;
    font-weight: 400;`

export const ModelBackground = styled.div`
    position: fixed;
    background-color: transparent;
    width: 100%;
    height: 100vh;    
    top: 50%;
    left: 50%;
    z-index: 10;
    overflow-y: hidden;
    transform: translate(-50%, -50%);
    `

export const ModelContainer = styled.div`
    width: 100%;
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    overflow-y: hidden;
    background-color: #00000080;`

export const ModelContent = styled.div`
    /* width: calc(100% - 65px); */
    position: relative;
    width: calc(100% - 65px);
    max-width: 1400px;
    /* height: 100%; */
    /* min-height: fit-content; */
    max-height: calc(100% - 80px);
    border-radius: 12px;
    /* border-top-right-radius: 12px;
        border-top-left-radius: 12px; */
    background-color: #FFF;
    overflow-y: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none; /* IE 11 */
    scrollbar-width: none; /* Firefox 64 */
    /* @media (max-height: 710px){
        
    } */
    `

export const JobModelHeaderContainer = styled.div`
        /* position: fixed; */
        /* background-color: red; */
        border-bottom: 1px solid ${Colors.boxBorder};
        width: 100%;
        max-width: 1316px;
        height: 68px;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        border-top-right-radius: 12px;
        border-top-left-radius: 12px;
        padding: 22px 40px;
        background-color: #FFF;

        `
export const JobModelTitle = styled.p`
    color: ${Colors.blackText};
    font-size: 18px;
    line-height: 24px; /* 133.333% */
    letter-spacing: -0.72px;
    font-weight: 500;`

export const JobDescriptionContainer = styled.div`
    width: 100%;
    max-width: 1319px;
    max-height: 647px;
    min-height: 500px;
    /* padding-top: 68px; */
    display: flex;
    flex-direction: row;
    border-bottom: 1px solid ${Colors.boxBorder};
    /* @media (max-height: 890px){
        max-height: calc(100% - 150px);
    } */
    @media (max-height: 705px){
        min-height: calc(470px - 25%);
    }
    `

export const JobSideDiv = styled.div`
    width: 100%;
    max-width: ${props => props.$widthprops || '100%'};
    padding: ${props => props.$paddingProp || '23px 38px'};
    display: flex;
    flex-direction: column;
    max-height: 647px;
    overflow-y: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none; /* IE 11 */
    scrollbar-width: none; /* Firefox 64 */
    @media (max-height: 835px){
        height: calc(100vh - 230px);
    }  
    /* background-color: red; */
    `

export const TempSearchContainer = styled.div`
    width: 100%;
    height: 56px;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 16px 10px;
    border-radius: 4px;
    border: 1px solid ${Colors.borderGray}`

export const SearchInput = styled.input`
    width: 100%;
    margin-left: 8px;
    /* background-color: red; */
    line-height: 24px; /* 150% */
    letter-spacing: -0.64px;
    font-weight: 500;
    font-size: 16px;
    &::placeholder{
        color: ${Colors.radioLabel};
    }`

export const SearchList = styled.div`
    width: 100%;
    margin-top: 20px;
    gap: 20px;
    display: flex;
    flex-direction: column;`

export const Radio = styled.input`
    accent-color: #000;
    border-width: 2px;

    &:disabled{
        border: 2px solid #000;
    }
    &:checked {
        accent-color: ${Colors.loginBtnColor};
    }
    
    `

export const RadioLabel = styled.label`
    font-weight: 400;
    font-size: 16px;
    margin-left: 12px;
    line-height: 24px; /* 150% */
    letter-spacing: -0.64px;
    color: ${Colors.radioLabel};
    `

export const RadioContainer = styled.button`
    width: 100%;
    display: flex;
    align-items: center;
    background-color: transparent;
    flex-direction: row;`

export const JobDescriptionTitle = styled.p`
    /* font-weight: 500;
    font-size: 24px; */
    color: #000`

export const JobDescriptionSubTitle = styled.p`
    font-weight: 600;
    font-size: 16px;
    margin-top: 14px;
    margin-bottom: 4px;`

export const JobParagraph = styled.p`
    font-size: 14px;
    font-weight: 400;
    margin-top: 12px;`

export const JobDescriptionList = styled.li`
    list-style: disc;
    font-size: 14px;
    font-weight: 400;
    margin-top: 12px;`

export const JobModelFooter = styled.div`
    position: sticky;
    /* bottom: 0; */
    background-color: #fff;
    border-top: 1px solid ${Colors.boxBorder};
    border-bottom-left-radius: 12px;
    border-bottom-right-radius: 12px;
    width: 100%;
    max-width: 1400px;
    display: flex;
    flex-direction: row;
    justify-content: end;
    padding: 29px 22px;
    gap: 12px;
    @media (max-width: 1280px) or (max-height: 720px){
        padding: 10px 10px;
        /* height: max-content; */
    }
    `

export const DummyImages = styled.div`
    width: 100%;
    height: calc(100vh - (82px + 56px));
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;`

    export const JobInputAlign = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    `