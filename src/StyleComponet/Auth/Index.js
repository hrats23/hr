import { createGlobalStyle, styled } from 'styled-components'
import { Colors, size } from '../Index'
import Image from 'next/image'


export const LoginMain = styled.div`
    width: 100%;
    display: flex;
    /* max-height: 1024px; */
    /* background-color: red; */
    flex-direction: row;
    height: 100vh;
    @media (max-height: 800px)  {
      height: 100%;
    }
    `

  export const BgSide = styled`
    width: 54%;
    background-color: blue;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
  `

export const AuthSideBg = styled.div`
  background-color: ${Colors.lightPurple};
  /* max-height: 1024px; */
  /* height: 100%; */
  display: flex;
  flex-direction: column;
  align-items: end;
  /* background-repeat: no-repeat;
  background-position: bottom; */
  /* background-size: cover; */
  /* background-image: url('https://firebasestorage.googleapis.com/v0/b/siam-storage.appspot.com/o/pana.svg?alt=media&token=c0ed56ca-9977-4541-96fc-3e461cb2d3fb'); */
  width: 54%;

  @media(max-width: 700px){
    display: none;
  }  
`

export const SideImageContainer = styled.div`
  width: 100%;
  max-width: '713px';
  `

export const BackgroundBottomBg = styled.div`
  width: 100%;
  height: 75vh;
  max-height: 635px;
  background-position: top;
  background-repeat: no-repeat;
  background-size: contain;
  background-image: url('https://firebasestorage.googleapis.com/v0/b/siam-storage.appspot.com/o/pana.svg?alt=media&token=c0ed56ca-9977-4541-96fc-3e461cb2d3fb');
  @media(min-height: 900px){
    margin-bottom: 106.02px;
  }`

export const AuthImage = styled(Image)`
  max-width: 100%;
  margin: 0 auto;
  `

export const AuthBgTexts = styled.div`
  /* display: flex; */
  margin-top: 70px;
  margin-bottom: 20px;
  width: 100%;
  padding: 0px 40px;
  /* background-color: red; */
  /* flex-direction: column; */
  /* align-items: start; */
  @media (max-height: 750px){
    margin-top: 40px;
  }
  `

export const AuthGraph = styled.div`
  height: 408px;
  /* background-color: red; */
  background: url('https://firebasestorage.googleapis.com/v0/b/siam-storage.appspot.com/o/svgviewer-output.svg?alt=media&token=98e8bc09-4790-4100-bc37-576805d23f2c') no-repeat center;
  width: 100%;
  `

export const LoginContainer = styled.div`
  /* height: 100vh; */
  width: 46%;
  display: flex;
  justify-content: center;
  @media (max-width: 700px){
    width: 100%;
  }
  @media (min-width: 1800px){
    justify-content: start;
    margin-left: 30px;
  }
  
  `

export const AuthBgTitle = styled.div`
  color: #7852A9;
  /* text-align: center; */
  
  font-size: 32px;
  font-style: normal;
  font-weight: 700;
  line-height: 34px; 
  @media (max-width: ${size.laptop}){
    font-size: 28px;
  }
  `
export const AuthBgSubTitle = styled.p`
  margin-top: 16px;
    color: #B794E4;
  /* text-align: center; */
  /* font-family: Inter; */
  font-size: 16px;
  font-style: normal;
  max-width: 576px;
  font-weight: 500;
  line-height: 27px; 
  @media (max-width: ${size.laptop}){
    font-size: 14px;
  }`

export const LogoBox = styled.div`
  padding-top: 88px;
  padding-bottom: 44px;
  
  @media (max-width: ${size.laptop}) {
    padding-top: 28px;
    padding-bottom: 14px;
  }
  @media (min-width: 1800px){
    padding-top: 30px;
  }
  `

export const LogoContainer = styled.div`
  /* padding: 88px; */
  width: 100%; 
    max-width: 213.3px;
    height: 122px;
    margin: 0 auto;
    background: url('https://siamcomputing.com/wp-content/uploads/2021/09/siam-computing-trans.png') no-repeat center;
    background-size: contain ;
    @media (max-width: 1280px) or (max-height: 720px) {
      /* margin-top: 50px; */
      /* height: 80px; */
      max-width: 150px;
  }`

export const AuthContainer = styled.div`
    width: 100%;
    /* height: 100%; */
    /* margin-top: 44px; */
    display: flex;
    flex-direction: column;
    /* justify-content: center; */
    align-items: center; 
    @media (max-width: 1280px) or (max-height: 720px) {
      margin-top: 0px;
    }`


export const AuthFromDiv = styled.div`
    width: 100%;
    max-width: 542px;
    height: 100%;
    max-height: 622px;`

export const EmailInput = styled.input`
    width: 100%;
    /* max-width: 384px; */
    height: 54px;
    padding: 12px 19px;
    border-radius: 8px;
    border: 1px solid ${Colors.blackText};
    @media (max-width: 1280px) or (max-height: 720px){
      height: 44px;
    }`

export const InputDiv = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    width: 100%;
     max-width: 384px;
    height: 54px;
    padding: 12px 19px;
    border-radius: 8px;
    border: 1px solid ${Colors.blackText};
    @media (max-width: 1280px) or (max-height: 720px){
      height: 44px;
    }`

export const AuthFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 34px;
  max-width: 542px;
  width: 100%;
  background: #FFF;
  /* height: 100%; */
  max-height: 622px;
  margin: 0 auto 20px;
  padding: 90px 0px 89px;
  box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.10);
  /* overflow: hidden; */

  @media (max-width: 1280px)  {
      max-width: 430px;
      max-height: 435px;
      /* background-color: red; */
  }
  @media (max-width: 322px){
    width: 100%;
  }
`
export const LoginFromContainer = styled.div`
  max-width: 384px;
  @media (max-width: 320px){
    width: 100%;
  }
  `

export const ForgotDiv = styled.div`
  max-width: 384px;
  
  @media (max-width: 1280px) or (max-height: 720px){
    max-width: 300px;
  }`

export const AuthWelcomeDiv = styled.div`
@media (max-width: 1280px) or (max-height: 720px){
    /* margin-top: 40px; */
  }`


export const AuthTitle = styled.p`
  color: ${Colors.blackText};
  text-align: center; 
  font-size: 24px;
  font-weight: 700;
  line-height: 34px;
  width: 100%;
  flex-wrap: wrap;
  
  @media (max-width: ${size.laptop}) or (max-height: 720px){
    font-size: 18px;
    line-height: 24px;
  }`


export const AuthForm = styled.form`

  @media (max-width: 1280px) or (max-height: 720px){
    /* background-color: red; */
    max-width: 300px;
  }`

export const AuthVerifyForm = styled.form`
  margin-top: 30px;
  max-width: 384px;
  @media (max-width: 1280px) or (max-height: 720px){
    max-width: 300px;
    margin-top: 0px;
  }`

export const AuthTextInput = styled.div`
    margin-top: ${props => props.$topside === 'true' ? '31px' : '0px'};
    display: flex;
    flex-direction: column;
    justify-content: center;
    
    `

export const Otpdiv = styled.div`
    input{
      width: 56px !important;
      height: 54px !important;
      font-size: 24px !important;
      border: 1px solid !important;
      border-radius: 8px !important;
      margin-right: 9px !important;
      color: ${Colors.blackText};
      font-weight: 600;
    }
    @media (max-width: 1280px) or (max-height: 720px) {
     input{
       width: 40px !important;
       font-size: 16px;
       font-weight: 500;
        height: 40px !important;
     }
    }
    `

export const AuthTextLabel = styled.label`
  color: ${Colors.blackText};
  font-size: 14px;
  font-weight: 600;
  margin-bottom: 8px; 
  @media (max-width: 1280px) or (max-height: 720px){
    font-size: 14px;
    font-weight: 600;
  }`

export const ForgotBtnText = styled.span`
  color: ${Colors.loginBtnColor};
  font-size: 18px;
  font-weight: 600;
  width: 384px;
  display: flex;
  justify-content: ${props => props.$side};
  margin-top: 18px;
  @media (max-width: 1280px) or (max-height: 720px){
    width: auto;
    font-size: 14px;
  }`

export const Button = styled.button`
  width: ${props => props.width || '384px'};
  height: 48px;
  margin-top: ${props => props.$topside || '46px'};
  border-radius: 8px;
  font-weight: 600;
  font-size: 18px;
  cursor: pointer;
  color: ${Colors.whiteText};
  background: ${props => props.disabled ? Colors.disableBtn : Colors.loginBtnColor};
  @media (max-width: ${size.laptop}) or (max-height: 720px) {
    margin-top: ${props => props.$topside || '26px'};
    width: ${props => props.width || '300px'};
    height: 44px;
    font-size: 16px;
  }
  @media (max-width: 320px){
    width: 100%;
  }
  &:hover {
    box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  }
  `

export const CancelBtn = styled(Button)`
  background: transparent;
  border: 1px solid ${Colors.loginBtnColor};
  color: ${Colors.loginBtnColor}`

export const ResetBtn = styled(CancelBtn)`
      width: 70px;
      height: 24px;
      font-size: 12px;
      margin-top: 0px;
      border-radius: 2px;
      border-width: 0px;
      font-weight: 600;
      color: ${Colors.blackGray};
      text-decoration: underline;
      &:hover{
        box-shadow: none;
        color: ${Colors.blackText};
      }
`

export const AuthSubTitle = styled.p`
  font-size: ${props => props.fontSize || '16px'};
  font-style: normal;
  font-weight: 500;
  line-height: 24px;
  text-align: center;
  margin-top: 14px;
  margin-bottom: 31px;
  max-width: ${props => props.$maxwidth || '384px'};
  color: ${Colors.blackGray};
  @media (max-width: ${size.laptop}){
    font-size: 14px;
  }`

export const ErrorDiv = styled.div`
  position: relative;`

export const ErrorText = styled.p`
  position: absolute; 
  color: ${Colors.errorColor};
  font-size: 12px;
  top: 5px;`

export const ToolbarParent = styled.div`
  position: relative;
  background-color: ${Colors.editorHead};
  `

export const ZoomIcons = styled.button.attrs(({ $tooltips }) => {
  return {
    'tooltip': $tooltips
  }
})`
  position: absolute;
  background-color: transparent;
  /* top: 10; */
  margin-top: 14px;
  cursor: pointer;
  left: 96%; 
  &:hover{
    &::after{
      content: attr(tooltip);
    display: flex;
    justify-content: center;
    padding: 5px;
    border-radius: 4px;
    margin-top: -55px;
    margin-right: -60px;
    color: white;
    transform: translateX(-50%);
    background-color:#000;
    }
  }
  `