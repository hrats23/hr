import { styled } from "styled-components";
import { Colors, size } from "./Index";

export const AdminBoards = styled.main`
    height: calc(100vh - 119px);
    width: 100%;
    /* background-color: red; */
    max-width: 1700px;
    display: flex;
    flex-direction: row;
    `
export const AdminCenterLayout = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;`

export const AdminLayout = styled.div`
/* background-color: blue; */
width: 100%;
height: 100vh;
position: fixed;
display: flex;
flex-direction: column;
/* align-items: center; */
justify-content: center;`

export const AdminAsid = styled.aside`
    height: 100%;
    max-width: 280px;
    background-color: ${Colors.whiteText};
    width: 100%;
    padding: 37px 33px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-right: 20px;
    border-radius: 4px;
    box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.10);
    `

export const AdminSideDiv = styled.div`
    width: 100%;
    color: #1D1D1D;
    `
export const AdminSideHead = styled.p`
    font-weight: 600;
    font-size: 16px;`

export const AdminLogoutBtn = styled.button`
    /* width: 100%; */
    background-color: transparent;
    color: ${Colors.blackGray};
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    font-weight: 400;
    font-size: 18px;
    gap: 5px;
    text-align: center; 
    @media (max-width: ${size.laptop}){
        font-size: 16px;
    }`

export const AdminSideNavContainer = styled.div`
    width: 100%;
    /* background-color: red; */
    gap: 8px;
    /* margin-top: 15px; */
    /* padding-left: 24px; */
    display: flex;
    flex-direction: column;
    `

export const AdminSideNav = styled.nav`
    font-weight: 400;
    font-size: 18px;
    display: flex;
    flex-direction: row;
    cursor: pointer;
    /* text-align: center; */
    align-items: center;`

export const AdminMainContainer = styled.div`
    width: 100%;
    overflow-y: scroll;
    &::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none; /* IE 11 */
    scrollbar-width: none; /* Firefox 64 */
    /* max-width: 1120px; */
    background-color: #fff;`

export const AdminMainHeader = styled.div`
    width: 100%;
    display: flex;
    /* flex-wrap: wrap; */
    row-gap: 10px;
    border-radius: 4px;
    color: #000;
    border-top: ${props => props.$topborder === 'true' ? '1px solid #000' : ''};
    flex-direction: row;
    background-color: ${props => props.$colorbackground === 'true' ? Colors.whiteText : 'transparent'};
    padding: 12px 20px;
    align-items: center;
    justify-content: space-between;
    box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.10);
    `

export const AdminHeaderText = styled.p`
font-size: 24px;
font-weight: 400;
font-style: normal;
line-height: 24px;
letter-spacing: -0.96px; 
@media (max-width: ${size.laptop}){
    font-size: 20px;
}`

export const AddUserBtn = styled.button`
    background-color: ${Colors.adminGray};
    padding: 13px 12px;
    gap: 10px;
    display: flex;
    flex-direction: row;
    font-size: 18px;
    font-style: normal;
    font-weight: 700;
    line-height: 24px; /* 133.333% */
    letter-spacing: -0.72px;`

export const AddUserFromContainer = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    `
export const AddUserForm = styled.form`
    width: 100%;
    max-width: 900px;
    margin-top: 60px;
    display: flex;
    row-gap: 43px;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;`

export const AdminFromInputs = styled.div`
    max-width: 434px;
    width: 100%;`

export const AdminLabel = styled.label`
    color: #000;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: 24px; /* 133.333% */
    letter-spacing: -0.72px;`

export const AdminInput = styled.input`
    width: 100%;
    height: 44px;
    padding: 10px 16px;
    font-style: normal;
    font-size: 18px;
    font-weight: 500;
    margin-top: 12px;
    line-height: 24px; /* 133.333% */
    letter-spacing: -0.72px;
    border: 1px solid ${Colors.inputBorder};`

export const AdminFormBtnContainer = styled.div`
    width: 100%;
    display: flex;
    column-gap: 25px;
    padding: 40px 0px;
    flex-direction: row;
    justify-content: end;`

export const AdminBtn = styled.button`
    background-color: ${props => props.inActive ? '#F0F0F0':Colors.adminBtn};
    display: flex;
    font-size: 16px;
    color: ${props => props.inActive === 'true' ? '#000' : '#fff'};
    font-style: normal;
    font-weight: 700;
    line-height: 24px; /* 150% */
    width: 162px;
    height: 42px;
    padding: 14px 31px;
    justify-content: center;
    align-items: center;`



// export const AdminTableDiv =styled.div`
//     overflow-x:auto;
// `
    
export const AdminTableContainer = styled.table`
    margin: 0px 24px;
    border-collapse: collapse;
    /* height: 112px;
    width: 1605px; */
    /* top: 304px; */
    /* overflow-x: auto; */
    /* left: 301px
    border: 1px */
    `

export const AdminTR = styled.tr``

export const AdminTH = styled.th`
    border: 1px solid #000;
    align-items: start;
    padding: 17px 18px;
    min-width: 88px;`

export const AdminTD = styled.td`
    border: 1px solid #000;
    align-items: start;
    padding: 17px 18px;
    `

export const AdminTable = styled.div`
    border-radius: 16px;
    margin-right: 14px;
    margin-bottom: 14px;
    margin-top: 20px;
    margin: 20px 14px 20px 0px;
    overflow: hidden;
    border: 0.6px solid #DEDEDE;
    `

export const AdminActionIcons = styled.div`
    display: flex;
    flex-direction: row;
    gap: 10px;
    align-items: center;
    justify-content: start;`

    
export const WarningBoxDiv = styled.div`
height: 100%;
padding: 36px 40px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
max-height: 194px`

export const WarningTitle = styled.div`
    font-weight: 600;
    text-align: center;
    font-size: 20px;
    line-height: 24px; 
    @media (max-width: ${size.laptop}){
        font-size: 18px;
    }`

export const WarningSubTitle = styled.div`
    font-weight: 400;
    font-size: 14px;
    margin-top: 8px;
    text-align: center;
    line-height: 22px;`

export const WarningBtns = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 24px;
    gap: 24px;`