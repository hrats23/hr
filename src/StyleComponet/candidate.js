import { styled } from "styled-components";
import { Colors } from "./Index";

export const JobPreview = styled.div`
    padding: 24px 40px 47px 40px;
    .ql-editor{
        resize: none
    }
    `

export const JobPreviewTitle = styled.p`
    /* font-family: 'Inter'; */
    font-size: 36px;
    color: ${Colors.loginBtnColor};
    font-style: normal;
    font-weight: 700;
    text-transform: capitalize;
    line-height: 43.57px;
    `

export const JobPreviewLocation = styled.p`
color: ${Colors.blackText};
font-size: 18px;
font-style: normal;
padding-top: 6px;
text-transform: capitalize;
font-weight: 400;
line-height: normal;`

export const JobPreviewDescription = styled.div`
    width: 100%;
    padding-top: 40px;
    @media (max-width: 500px) {
        padding-top: 25px;
    }
    `

export const JobCarMenuIcons = styled.div`
/* display: flex;
align-items: center; */
cursor: pointer;
margin: 0 auto;
background-color: red;
width: 10px;
padding-left: 2px;
padding-right: 2px;
justify-content: center;`