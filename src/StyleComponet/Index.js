import { createGlobalStyle } from "styled-components"

export const Colors = {
  loginBtnColor: '#7852A9',
  blackText: '#1D1D1D',
  blackGray: '#747474',
  whiteText: '#FFFFFF',
  textBtn: '#7852A9',
  disableBtn: '#D2D2D2',
  errorColor: '#FF4664',
  authbg: '#FCFAFF',
  headColor: '#2D3748',
  lightPurple: '#F7F1FF',
  optionGray: '#969698',
  borderGray: '#BFBFC2',
  radioLabel: '#7E7E7E',
  boxBorder: '#AAA9AE',
  adminGray: '#F0F0F0',
  authAside: '#5C5C5C',
  adminModal: '#D9D9D9',
  inputBorder: '#D8CECE',
  adminBtn: '#838383',
  holdLabel: '#FE9D00',
  editorHead: '#F7F7F7',
  careerError: '#DA4545',
  keyCardPink: '#FFEDE8',
  keySubText: '#939598',
  careericon: '#DC5050',
  careerBtn: '#333333',
  logoutGray: '#90A0B7'
}

export const size = {
  laptop: "1300px",
}

export const GlobalStyle = createGlobalStyle`
  
  *{
    padding: 0px;
    margin: 0;
    border: 0;
    box-sizing: border-box;
    font-family: 'Inter', sans-serif;
  }
  
  body{
    position: relative;
    height: 100%;
  }
    @font-face {
      font-family: 'Inter';
    }

    /* li{
      list-style: none;
    } */
    /* Chrome, Safari, Edge, Opera */
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input:focus{
    outline: none;
  }

  a:-webkit-any-link{
    text-decoration: none;
  }
  
  /* Firefox */
  input[type=number] {
    -moz-appearance: textfield;
  }

  .ql-toolbar.ql-snow{
    height: 54px;
    align-items: center;
    display: flex;
    justify-content: start;
    align-items: center;
    background-color:#F7F7F7;
    padding: 19px 20px 20px 20px;
  }

  #toolbar{
    background-color: #E6E6E6;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
  }

  .left-side{
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
  }

  .right-side{
    /* background-color: red; */
  }
 
  .ql-snow .ql-picker.ql-size .ql-picker-item[data-value="14px"]::before {
    font-size: 14px !important;
}

.ql-snow .ql-picker.ql-size .ql-picker-item[data-value="16px"]::before {
    font-size: 16px !important;
}

.ql-snow .ql-picker.ql-size .ql-picker-item[data-value="18px"]::before {
    font-size: 18px !important;
}
.ql-snow .ql-picker.ql-size .ql-picker-item[data-value="20px"]::before {
    font-size: 20px !important;
}


  .ql-toolbar.ql-snow .ql-picker-label {
    background-color: #FFF;
    border-radius: 4px;
  }

  .ql-editor{
    height: 239px;
    resize: vertical;
  }

  .ant-picker .ant-picker-input >input{
    font-size: 16px;
  }

  .ant-picker .ant-picker-suffix{
    color: #7852A9;
  }


.ql-editor{
  font-family: 'Inter' sans-serif;
}
.ql-font span[data-value="Inter"]::before, .ql-font span[data-label="Impact"]::before {
  font-family: 'Inter' sans-serif;
  content: 'Inter' !important;
}
.ql-font span[data-value="impact"]::before, .ql-font span[data-label="Impact"]::before {
  font-family: "Impact";
  content: 'Impact' !important;
}
.ql-font span[data-value="courier"]::before, .ql-font span[data-label="Courier"]::before {
  font-family: "Courier";
  content: 'Courier' !important;
}
.ql-font span[data-value="comic"]::before, .ql-font span[data-label="Comic Sans MS"]::before {
  font-family: "Comic Sans MS";
  content: 'Comic Sans' !important;
}

.ql-font-impact { font-family: 'Impact'; }
.ql-font-courier { font-family: 'Courier'; }
.ql-font-comic { font-family: 'Comic Sans MS'; }
.ql-font-Inter { font-family: 'Inter' sans-serif; } 

.text-editor {
  width: 100%;
  margin: 0 auto;
}

.ql-font{
  margin-right: 20px;
}

/* .ql-snow .ql-picker.ql-font{
  width: 100%;
} */


.ant-modal .ant-modal-content{
  padding: 0px;
  border-radius: 0px;
}
.ant-modal .ant-modal-title{
  padding: 22px 40px 22px;
  font-weight: 600;
  line-height: 24px;
  color: #000000;
  font-size: 18px;
}

.add-user-modal .ant-modal-header{
  background-color: #FFF;
  border-bottom: 1px solid #AAA9AE;
}

.logout-modal .ant-modal-content{
  /* background-color: red; */
  padding: 20px;
}

.logout-modal .ant-btn-primary {
  background-color: #F0F0F0;
  color: black;
  border: 1px solid #d9d9d9;
}

.ant-pagination .ant-pagination-item-active{
  background-color: #7852A9;
  color: #FFF;
  border: none;
}

.ant-pagination .ant-pagination-item-active a {
  color: #FFF;
}

.ant-pagination .ant-pagination-item-active:hover a {
  color: #FFF;
}

.delete-modal .ant-modal-content{
  border-radius: 8px;
}

.popup-content .ql-container.ql-snow {
  border: none;
}

.popup-content {
  max-width: 1080px !important;
  width: 100% !important;
}

.popup-content .ql-editor{
  min-height: 450px;
  max-height: 600px;
}

.text-editor{
  border-radius: 4px;
}

.job-modal .ant-modal-title{
  font-weight: 500;
  font-size: 18px;
  line-height: 24px;
  border-bottom: 1px solid #C7C7C7;
  border-top: 12px;
  color: ${Colors.blackText};
}

.job-modal .ant-modal-content {
  border-radius: 12px;
}

/* :where(span, svg) {
  outline: none;
} */

.job-template-select .ant-modal-content .ant-modal-close{
  display: none;
}

.job-template-select .ant-modal-content{
  border-radius: 12px;
}

svg{
  pointer-events: all;
}

input:-webkit-autofill,
input:-webkit-autofill:hover, 
input:-webkit-autofill:focus,
textarea:-webkit-autofill,
textarea:-webkit-autofill:hover,
textarea:-webkit-autofill:focus,
select:-webkit-autofill,
select:-webkit-autofill:hover,
select:-webkit-autofill:focus {
/* border: none; */
  -webkit-text-fill-color: ${Colors.blackText};
  -webkit-box-shadow: 0 0 0px 1000px transparent inset;
  transition: background-color 5000s ease-in-out 0s;
}

.ant-picker-time-panel{
  display: none !important;
  /* width: 0px;
  padding: 0px; */
}

  [data-tooltip]{
    content: attr(data-tooltip);
    display: none;
  }

  [data-tooltip]:hover{
    &::after{
    content: attr(data-tooltip) !important;
    display: flex;
    position: absolute !important;
    /* z-index: 1; */
    padding: 5px;
    border-radius: 4px;
    margin-top: -30px;
    margin-left: -7px;
    color: white;
    background-color: #000 !important;
    }
  }

  [data-tooltip='Font Style']:hover{
    &::after{
    content: attr(data-tooltip) !important;
    display: flex;
    position: absolute !important;
    /* z-index: 1; */
    padding: 5px;
    border-radius: 4px;
    margin-top: -50px;
    margin-left: 10px;
    color: white;
    background-color: #000 !important;
    }
  }
  [data-tooltip='Font Size']:hover{
    &::after{
    content: attr(data-tooltip) !important;
    display: flex;
    position: absolute !important;
    /* z-index: 1; */
    padding: 5px;
    border-radius: 4px;
    margin-top: -50px;
    margin-left: 10px;
    color: white;
    background-color: #000 !important;
    }
  }


  `

