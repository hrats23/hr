import { styled } from "styled-components";
import { Colors, size } from "./Index";

export const CareerMain = styled.main`
    width: 100%;
    height: 100%;
    background-color: #fff;`

export const CareerHeader = styled.header`
    display: flex;
    max-width: 748px;
    width: 100%;
    justify-content: space-between;
    margin: 100px auto;
    @media screen and (max-width: 850px) {
        max-width: 550px;
      }
      @media screen and (max-width: 650px) {
        margin: 35.71px auto 48.55px;
        max-width: 300px;
      }
      @media screen and (max-width: 390px) {
        max-width: 260px;
      }
      
    `
export const SelectedHeader = styled.div`
    position: relative;
    cursor: pointer;
    text-align: center;
    `

export const UnSelectedItem = styled.button`
    color: #666;
    font-family: Prompt;
    font-size: 24px;
    font-style: normal;
    font-weight: 600;
    line-height: 31.2px;
    cursor: pointer;
    background: transparent;
    @media screen and (max-width: 650px) {
        font-size: 16px;
        line-height: 20.8px;
      }
    `
export const SelectedItem = styled(UnSelectedItem)`
    color: #333;
   `
export const CardContainer = styled.div`
   display: grid;
   grid-template-columns: 1fr 1fr 1fr;
   gap: 50px;
   max-width: 1280px;
   margin: 0 auto 100px;
   padding: 0 20px;
   @media screen and (max-width: 1024px) {
    grid-template-columns: 1fr 1fr;
  }
  @media screen and (max-width: 650px) {
    grid-template-columns: 1fr ;
    gap: 20px;
    padding: 0 16px;
  }
   `
export const CardBoxes = styled.div`
   border-radius: 2px;
   border: 1px solid #DDD;
   background: #FFF;
   width: auto;
   cursor: pointer;
   min-height: 169px;
   padding: 30px 22px 28px 30px;
   display: flex;
    flex-direction: column;
    justify-content: space-between;
   &:hover {
    border: 1px solid #EEE;
    box-shadow: 10px 14px 64px 0px rgba(194, 194, 194, 0.25);
  }
  @media screen and (max-width: 1024px) {
    min-height: auto;
  }
  @media screen and (max-width: 650px) {
    padding: 15px;
}
`
export const CardHeading = styled.div`
    color: #DA4545;
    font-family: Prompt;
    font-size: 24px;
    font-style: normal;
    font-weight: 600;
    text-transform: capitalize;
    line-height: 31.2px;
    margin-bottom: 18px;
    text-align: start;
    width: fit-content;
    background-color: transparent;
    @media screen and (max-width: 650px) {
        font-size: 20px;
        line-height: 22px;
    }
`
export const CardFooter = styled.div`
    display: flex;
    align-items: center;
    justify-content: space-between;
`
export const CardFooterFirstChild = styled.p`
    color: #444;
    font-family: Prompt;
    font-size: 20px;
    font-style: normal;
    font-weight: 500;
    line-height: 34px;
    margin: 0;
    max-width:200px;
    word-wrap: break-word;
    text-transform: capitalize;
    @media screen and (max-width: 650px) {
        font-size: 16px;
        line-height: 27.2px;
    }
`
export const CardFooterSecondChild = styled.p`
    color: #444;
    font-family: Prompt;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: 23.8px;
    max-width: 166px;
    margin: 0;
`

export const CircularPath = styled.svg`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  transition: scale 0.9s;
  width: 174.61px;
  height:73.59px;

  @media screen and (max-width: 650px) {
    width: 101px;
  }
`;

export const CareerJobDetailsMain = styled.main`
    width: 100%;
    padding: 60px 89px 160px 89px;
    display: flex;
    flex-direction: column;
    max-width: 1440px;
    margin: 0 auto;
    align-items: start;
    @media (max-width: 500px) {
        padding: 24px 16px 48px 16px;
    }

    .ql-editor{
        overflow-y: hidden;
        resize: none;
        width: 100%;
        height: 100% !important;
    }
`
export const CareerBackButton = styled.button`
    background-color: transparent;
    font-weight: 500;
    font-family: Prompt;
    font-size: 16px;
    line-height: 27.2px;
    display: flex;
    flex-direction: row;
    align-items: center;
    cursor: pointer;
    gap: 10px;
    text-decoration: underline;
    color: #444444;`

export const CareerJobTitle = styled.header`
    font-weight: 800;
    font-size: 40px;
    line-height: 52px;
    padding-top: 60px;
    color: #333;
    font-family: 'Prompt';
    @media (max-width: 500px) {
        padding-top: 24px;
        font-size: 20px;
        line-height: 26px;
    }
    `

export const CareerJobLoation = styled.p`
    font-weight: 400;
    font-size: 20px;
    font-family: 'Prompt';
    text-transform: capitalize;
    line-height: 34px;
    color: ${Colors.careerError};
    padding-top: 14px;
    @media (max-width: 500px) {
        padding-top: 8px;
        line-height: 23.8px;
        font-size: 14px;
    }
    `

export const CareerKeyDetailsContainer = styled.div`
    width: 100%;
    padding-top: 24px`

export const CareerKeyTitle = styled.p`
 /* font-family: 'Inter'; */
 font-weight: 600;
 font-size: 18px;
 color: ${Colors.blackText};
 line-height: 21.78px;
 @media (max-width: 500px) {
     font-size: 16px;
 }`

export const CareerKeyCardList = styled.div`
    width: 100%;
    padding-top: 14px;
    display: grid;
    row-gap: 24px;
    grid-template-columns: repeat(auto-fit, minmax(148px, 1fr));;
    max-width: 721px;`

export const KeyCardContainer = styled.div`
    max-width: 166px;
    width: 100%;
    display: flex;
    gap: 6px;
    flex-direction: row;
    `

export const KeyCardIcon = styled.span`
    width: 34px;
    height: 24px;
    display: grid;
    place-items: center;
    background-color: ${Colors.keyCardPink};
    border-radius: 5px;
    @media (max-width: 500px) {
        width: 30px;
        height: 20px;
        svg {
            width: 13px !important;
            height: 12px !important;
        }
    }`

export const KeyTextContainer = styled.div``

export const KeyTitle = styled.p`
color: ${Colors.blackText};
font-size: 14px;
font-style: normal;
font-weight: 400;
line-height: 16.94px;
letter-spacing: -0.56px;
`

export const KeySubTitle = styled(KeyTitle)`
color: ${Colors.keySubText};
@media (max-width: 500px) {
    font-size: 12px;
}`


export const SubHeader = styled.div`
    display: flex;
    width:100%;
    align-items: center;
    position: relative;
    z-index: 2;
    justify-content: space-between;
    height: 76px;
    background: #FFF;
    box-shadow: 0px 0px 7px 0px rgba(0, 0, 0, 0.10);
`

export const HeaderBtn = styled.button`
    color: #7852A9;
    font-size: 18px;
    font-style: normal;
    font-weight: 600;
    cursor: pointer;
    line-height: 24px; 
    border-radius: 6px;
    background: #F7F1FF;
    padding:12px;
    text-align: center;
    @media (max-width: ${size.laptop}) {
        font-size: 16px
    };
    &:hover{
        box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
    }
`
export const JobListWraper = styled.div`
    padding: 64px 0 110px 0;
    max-width:1339px;
    margin:0 auto;
`
export const BackBtnText = styled.p`
    color: #000;
    font-family: Prompt;
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    text-decoration-line: underline;
    margin:0px;
`
export const SpaceCenter = styled.div`
    display: flex;
    width:100%;
    gap:45px;
`
export const JobleftContainer = styled.div`
    width:35%;
    max-width:476px;
`
export const Positions = styled.div`
    position:relative;
`
export const JobleftHeading = styled.h3`
    color: #444;
    font-family: Prompt;
    font-size: 40px;
    font-style: normal;
    font-weight: 700;
    line-height: 50px; 
`
export const JobleftParagrap = styled.p`
    color: #444;
    font-family: Prompt;
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    margin-top:36px;
`
export const JobleftCard = styled.div`
    border-radius: 10px;
    background: #F5F6FA;
    margin-top:80px;
    display: flex;
    align-items: center;
    flex-direction: column;
    max-width:451px;
`
export const JobleftCardText = styled.div`
    color: #444;
    text-align: center;
    font-family: Prompt;
    font-size: 20px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    margin-top:10px;
`
export const JobleftCardMail = styled(JobleftCardText)`
    color: #EE343E;
    margin-top:0;
    margin-bottom:24px;
`

export const JobRightContainer = styled.div`
    width:65%;
    max-width:815px;
`
export const JobRightHeading = styled.div`
    color: #444;
    font-family: Prompt;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    text-align: center;
    margin-bottom:36px;
`
export const FormTopic = styled.div`
    color: #444;
    font-family: Roboto;
    font-size: 20px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    border-bottom: 1px solid #E1E1E1;
    padding-bottom: 10px;
`
export const SubmitBtn = styled.button`
    background: #000;
    color: #FFF;
    font-family: Prompt;
    font-size: 24px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    margin: 88px auto 42px;
    display:block;
    padding: 18px 81.5px;
    cursor: pointer;
`
export const JobTextArea = styled.textarea`
    width: 100%;
    padding: 16px 12px;
    margin-top: 14px;
    border-radius: 4px;
    border: 1px solid ${Colors.blackGray};
    font-size: 16px;
    font-style: normal;
    font-weight: 400;`

export const CareerJobContainer = styled.div`
    width: 100%;
    padding-top: 40px;
    @media (max-width: 500px){
        display: grid;
        place-items: center;
        padding-top: 39.89px;
    }`
export const FileUploadSection = styled.div`
    border-radius: 10px;
    border: 2px dashed #E4E4E4;
    background: #F5F6FA;
    max-width: 463px;
    min-height: 99px;
    margin: 16px auto 0;
    cursor: pointer;
    text-align: center;
    `
export const CareerApplyBtn = styled.button`
    width: 100%;
    max-width: 250px;
    font-family: Prompt;
    font-weight: 500;
    font-size: 20px;
    color: #FFF;
    cursor: pointer;
    background-color: ${Colors.careerBtn};
    height: 60px;
    line-height: 30px;
    border-radius: 2px;
    @media (max-width: 500px) {
        font-size: 16px;
        max-width: 198px;
        height: 50px;
        font-weight: 500;
        line-height: 24px;
    }
    `
export const CareerFooter = styled.div`
    width: 100%;
    max-height: 433px;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    padding: 85px 0px 80px 0px;
    background-color: ${Colors.careerError};
    @media (max-width: 500px) {
        max-height: 167px;
    }`

export const CareerFooterText = styled.p`
    font-weight: 800;
    font-size: 60px;
    max-width: 701px;
    color: #FFF;
    text-align: center;
    line-height: 78px;
    padding-bottom: 43px;
    font-family: Prompt;
    @media (max-width: 500px) {
        font-size: 16px;
        line-height: 20.8px;
        max-width: 174px;
        padding-bottom: 17.79px;
    }`

export const CareerFooterBtn = styled(CareerApplyBtn)`
    max-width: 214px;
    background-color: #fff;
    line-height: 30px;
    color: ${Colors.careerBtn};
    @media (max-width: 500px) {
        max-width: 140px;
        height: 38px;
        font-size: 14px;
        line-height: 21px;
    }
    `

export const CareerFooterConteriner = styled.div`
    width: fit-content;
    position: relative;
    text-align: center;`


export const CareerArrowContainer = styled.div`
    position: absolute;
    object-fit: contain;
    right: 0;
    top: 40%;
    transform: rotate(5deg);
    width: 151.3px;
    height: 133.2px;
    @media (max-width: 500px) {
        width: 50px !important;
        height: 50px !important;
        transform: rotate(-30deg);
        right: -40px;
        top: 25%;
    }
    `