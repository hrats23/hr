import * as React from "react";
const SVGComponent = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={236}
    height={20}
    viewBox="0 0 236 20"
    fill="none"
    {...props}
  >
    <path
      d="M2.5 13.0012C50.8333 8.00117 144.1 -0.498832 130.5 5.50117C113.5 13.0012 105 15.0012 130.5 16.5012C150.9 17.7012 207.333 11.0012 233 5.50117"
      stroke="#EE343E"
      strokeWidth={5}
      strokeLinecap="round"
    />
  </svg>
);
export default SVGComponent;
