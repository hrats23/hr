import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={24}
    height={24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <mask
      id="mask0_1044_1371"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={24}
      height={24}
    >
      <rect width={24} height={24} fill="#D9D9D9" />
    </mask>
    <g mask="url(#mask0_1044_1371)">
      <path
        d="M7.33804 18.8898L5.93652 17.4936L11.5426 11.9088L5.93652 6.32394L7.33804 4.92773L12.9441 10.5125L18.5502 4.92773L19.9517 6.32394L14.3456 11.9088L19.9517 17.4936L18.5502 18.8898L12.9441 13.305L7.33804 18.8898Z"
        fill="#1C1B1F"
      />
    </g>
  </svg>
);
export default SVGComponent;
