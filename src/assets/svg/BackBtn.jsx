import * as React from "react";
const SVGComponent = ({width,height, ...props}) => (
  <svg
    width={width}
    height={height}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <mask
      id="mask0_471_5910"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={24}
      height={24}
    >
      <rect width={24} height={24} fill="#D9D9D9" />
    </mask>
    <g mask="url(#mask0_471_5910)">
      <path
        d="M7.825 12.8313L13.425 18.3815L12 19.7691L4 11.8401L12 3.91113L13.425 5.29871L7.825 10.849H20V12.8313H7.825Z"
        fill="#1D1D1D"
      />
    </g>
  </svg>
);
export default SVGComponent;
