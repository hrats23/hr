import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={25}
    height={25}
    viewBox="0 0 25 25"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <mask
      id="mask0_471_526"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={25}
      height={25}
    >
      <rect x={0.224365} y={0.625977} width={24} height={24} fill="#D9D9D9" />
    </mask>
    <g mask="url(#mask0_471_526)">
      <path
        d="M5.22437 21.626C4.67437 21.626 4.20353 21.4301 3.81187 21.0385C3.4202 20.6468 3.22437 20.176 3.22437 19.626V5.62598C3.22437 5.07598 3.4202 4.60514 3.81187 4.21348C4.20353 3.82181 4.67437 3.62598 5.22437 3.62598H12.2244V5.62598H5.22437V19.626H12.2244V21.626H5.22437ZM16.2244 17.626L14.8494 16.176L17.3994 13.626H9.22437V11.626H17.3994L14.8494 9.07598L16.2244 7.62598L21.2244 12.626L16.2244 17.626Z"
        fill="#90A0B7"
      />
    </g>
  </svg>
);
export default SVGComponent;
