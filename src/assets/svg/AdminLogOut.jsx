import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={24}
    height={24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <mask
      id="mask0_34_9032"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={24}
      height={24}
    >
      <rect
        y={24}
        width={24}
        height={24}
        transform="rotate(-90 0 24)"
        fill="#D9D9D9"
      />
    </mask>
    <g mask="url(#mask0_34_9032)">
      <path
        d="M21 19C21 19.55 20.8042 20.0208 20.4125 20.4125C20.0208 20.8042 19.55 21 19 21H5C4.45 21 3.97917 20.8042 3.5875 20.4125C3.19583 20.0208 3 19.55 3 19L3 12H5V19H19V12H21V19ZM17 8L15.55 9.375L13 6.825V15H11V6.825L8.45 9.375L7 8L12 3L17 8Z"
        fill="white"
      />
    </g>
  </svg>
);
export default SVGComponent;
