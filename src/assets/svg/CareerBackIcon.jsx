import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={15}
    height={15}
    viewBox="0 0 15 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g clipPath="url(#clip0_2404_2821)">
      <path
        d="M3.22472 7.49996C3.22472 7.23113 3.32737 6.96233 3.53223 6.75737L9.98196 0.307712C10.3922 -0.102571 11.0574 -0.102571 11.4676 0.307712C11.8777 0.717829 11.8777 1.3829 11.4676 1.79321L5.76048 7.49996L11.4674 13.2067C11.8775 13.617 11.8775 14.282 11.4674 14.6921C11.0572 15.1026 10.392 15.1026 9.98176 14.6921L3.53203 8.24254C3.32714 8.03749 3.22472 7.76869 3.22472 7.49996Z"
        fill="#444444"
      />
    </g>
    <defs>
      <clipPath id="clip0_2404_2821">
        <rect
          width={15}
          height={15}
          fill="white"
          transform="matrix(0 1 -1 0 15 0)"
        />
      </clipPath>
    </defs>
  </svg>
);
export default SVGComponent;
