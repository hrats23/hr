import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={12}
    height={12}
    viewBox="0 0 12 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M6.58584 6.00036L4.11084 3.52536L4.81784 2.81836L7.99984 6.00036L4.81784 9.18236L4.11084 8.47536L6.58584 6.00036Z"
      fill="#2D3748"
    />
  </svg>
);
export default SVGComponent;
