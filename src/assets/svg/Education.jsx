import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={18}
    height={18}
    viewBox="0 0 18 18"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <mask
      id="mask0_2404_2896"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={18}
      height={18}
    >
      <rect width={18} height={18} fill={props.color} />
    </mask>
    <g mask="url(#mask0_2404_2896)">
      <path
        d="M16.4674 13.0334V7.85347L9.71011 11.5319L1.45117 7.02769L9.71011 2.52344L17.969 7.02769V13.0334H16.4674ZM9.71011 16.0362L4.45442 13.1835V9.42995L9.71011 12.2826L14.9658 9.42995V13.1835L9.71011 16.0362Z"
        fill={props.color}
      />
    </g>
  </svg>
);
export default SVGComponent;
