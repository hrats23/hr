import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={12}
    height={12}
    viewBox="0 0 12 12"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M5.414 6.00036L7.889 8.47536L7.182 9.18236L4 6.00036L7.182 2.81836L7.889 3.52536L5.414 6.00036Z"
      fill="#2D3748"
    />
  </svg>
);
export default SVGComponent;
