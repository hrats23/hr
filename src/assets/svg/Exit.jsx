import * as React from "react";
const SVGComponent = (props) => (
  <svg
    width={24}
    height={24}
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <mask
      id="mask0_835_13457"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={24}
      height={24}
    >
      <rect width={24} height={24} fill="#D9D9D9" />
    </mask>
    <g mask="url(#mask0_835_13457)">
      <path
        d="M7.33804 19.0386L5.93652 17.6373L11.5426 12.032L5.93652 6.42671L7.33804 5.02539L12.9441 10.6307L18.5502 5.02539L19.9517 6.42671L14.3456 12.032L19.9517 17.6373L18.5502 19.0386L12.9441 13.4333L7.33804 19.0386Z"
        fill="#1C1B1F"
      />
    </g>
  </svg>
);
export default SVGComponent;
