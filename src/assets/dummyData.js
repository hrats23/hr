export const dummyUser = {
    isAuth: true,
    name: 'dev'
}

export const departmentsList = [
    'PDS',
    'Labs',
    'CET',
    'CST'
]

export const workingModel = [
    'Work from office',
    'Hybird',
    'Remote'
]

export const senorityLeve = [
    'Associate',
    'Senior Associate',
    'Senior',
    'Engineer',
    'Lead',
    'None of above'
]

export const employeType = [
    'Full Time',
    'Intern',
    'Trainee',
]

export const techStack = [
    'React',
    'Laravel',
    'Node',
]

export const education = [
    'UG',
    'PG'
]

export const noticePeriod = [
    '10 Days',
    '20 Days',
    '30 Days',
    '60 Days',
    '90 Days',
]

export const openingJobs = [1, 2, 3, 4]

export const jobTemplist = ['Marketing inter', 'Backend Engineer', 'Front Engineer', 'Product manager', 'Product designer Associate ', 'UI/UX Designer']

// export const adminSideNav = ['Manage users', 'Manage Roles', 'Manage Permissions']
export const adminSideNav = [{
    label: 'Manage Users',
    value: 'Manage Users'
}, {
    label: 'Manage Roles',
    value: 'Manage Roles'
}, {
    label: 'Manage Permissions',
    value: 'Manage Permissions'
}]

export const userRole = ['HR Recuriter', 'HR Manager', 'CEO', 'Division head', 'Interviewer']

export const status = ['Enabled', 'Disabled']



export const jobDescription = [
    {
        title: 'Marketing inter',
        content: [
            {
                type: 'paragraph',
                subTitle: 'Job Description',
                data: [
                    'Conduct market research to gather valuable insights about the target audience, competitors, industry trends, and customer preferences.',
                    'Help create engaging and relevant content for various marketing channels such as social media, blogs, email campaigns, and website content. Collaborate with the content team to maintain a consistent brand voice.',
                    'Assist in managing social media accounts, including content scheduling, posting, and responding to comments and messages. Monitor social media analytics to track performance and suggest improvements.',
                    'Support the marketing team in planning, executing, and measuring the effectiveness of marketing campaigns. Help coordinate promotional activities and events.',
                    'Assist in email marketing efforts, including creating email templates, managing subscriber lists, and tracking campaign performance.'
                ]
            },
            {
                type: 'ul',
                subTitle: 'Skills',
                data: [
                    'Marketing interns need to be able to solve problems quickly and efficiently.',
                    'Assist in email marketing efforts, including creating email templates, managing subscriber lists, and tracking campaign performance.',
                    'Marketing interns need to be able to solve problems quickly and efficiently.',
                    'Assist in email marketing efforts, including creating email templates, managing subscriber lists, and tracking campaign performance.'
                ]
            }

        ],
    },
    {
        title: 'Backend Engineer',
        content: [
            {
                type: 'paragraph',
                subTitle: 'Job Description',
                data: [
                    'Marketing interns need to be able to solve problems quickly and efficiently.',
                    'Assist in email marketing efforts, including creating email templates, managing subscriber lists, and tracking campaign performance.',
                    'Marketing interns need to be able to solve problems quickly and efficiently.',
                    'Assist in email marketing efforts, including creating email templates, managing subscriber lists, and tracking campaign performance.'
                ]
            },
            {
                type: 'ol',
                subTitle: 'Skills',
                data:[
                    'Conduct market research to gather valuable insights about the target audience, competitors, industry trends, and customer preferences.',
                    'Help create engaging and relevant content for various marketing channels such as social media, blogs, email campaigns, and website content. Collaborate with the content team to maintain a consistent brand voice.',
                    'Assist in managing social media accounts, including content scheduling, posting, and responding to comments and messages. Monitor social media analytics to track performance and suggest improvements.',
                    'Support the marketing team in planning, executing, and measuring the effectiveness of marketing campaigns. Help coordinate promotional activities and events.',
                    'Assist in email marketing efforts, including creating email templates, managing subscriber lists, and tracking campaign performance.'
                ] 
            }

        ],
    },
]

export const adminTableHead = ['Sn', 'First name', 'Last name', 'User role', 'Email ID', 'Employee ID', 'Mobile', 'Status', 'Action']

export const adminTableData = ['1', 'John', 'Doe', 'HR Recuriter', 'a@a.com', '1234567890', '9876543210', 'Enabled </AdminDropIcon>', '</AdminEdit> </AdminDeleteIcon> </AdminResetIcon>']

export const moneyDigit = (num) => {
    const digit = new Intl.NumberFormat('en-IN', {
        style: 'currency',
        currency: 'INR',
        minimumFractionDigits: 0
    }).format(num).split('₹')[1]

    return digit
}

export const removeComma = (str) => {
    return parseInt(str.replaceAll(',', ''))
}
