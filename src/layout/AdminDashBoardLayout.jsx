import { AdminAsid, AdminBoards, AdminCenterLayout, AdminLayout, AdminLogoutBtn, AdminSideDiv, AdminSideHead, AdminSideNav, AdminSideNavContainer } from '@/StyleComponet/admin'
import { adminSideNav } from '@/assets/dummyData'
import AdminHeader from '@/components/reusable/AdminHeader'
import React, { useState } from 'react'
import AdminLogout from '@/assets/svg/Logout'
import AdminNavIcons from '@/assets/svg/AdminNavIcons'
import { useUser } from '@/context/User'
import { useRouter } from 'next/router'
import { Modal } from 'antd'
import LayoutHeader from '@/components/reusable/Header'
import DashBoardIcons from '@/assets/svg/DashBoardIcons'
import RationList from '@/components/reusable/RationList'
import WarningBox from '@/components/modals/WarningBox'
import { useForm } from 'react-hook-form'

const routerName = [{ name: 'Dashboard', icon: DashBoardIcons, link: ['/admin'] }]

function AdminDashBoardLayout({ children }) {
  const { user, setUser } = useUser()
  const router = useRouter()

  const { control, handleSubmit, formState: { errors, isValid} } = useForm()

  const [sideNav, setSideNav] = useState('Manage users')
  const [boxOpen, setBoxOpen] = useState(false)
  const [warningMessage, setWarningMessage] = useState({})
  const [fileterValue, setFileterValue] = useState({})
  
  const handleLogout = () => {
    setWarningMessage({
      title: 'Are you sure you want to logout?',
      // subTitle: 'Do you really want to logout?',
      main: 'logout'
    })
    setBoxOpen(true)
  }

  // if (user.role_name === 'Super Admin') {
    return (
      <>
        <AdminLayout>
          <LayoutHeader routerName={routerName}/>

          <AdminCenterLayout>
          <AdminBoards style={{ height: 'calc(100vh - 91px)' }}>
            <AdminAsid>
              <AdminSideDiv>
                <AdminSideNavContainer>
                     <RationList option={adminSideNav} control={control} isLoading={false} name='Jobs' label='Super Admin' defaultValue='Manage Users' setFileterValue={setFileterValue}/>
                </AdminSideNavContainer>
              </AdminSideDiv>
              <AdminSideDiv>
                <AdminLogoutBtn style={{ cursor: 'pointer' }} onClick={() => handleLogout()}>
                  <AdminLogout />
                  Log out
                </AdminLogoutBtn>
              </AdminSideDiv>
            </AdminAsid>
            {children}
          </AdminBoards>
          </AdminCenterLayout>
        </AdminLayout>
        <WarningBox open={boxOpen} close={() => setBoxOpen(false)} warningMessage={warningMessage}/>
      </>
    )
  // } else return <>Loading...</>
}

export default AdminDashBoardLayout