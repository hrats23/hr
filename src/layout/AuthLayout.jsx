'use client'

import React, { useEffect } from 'react'
import { LoginBg, AuthSideBg, AuthGraph, AuthBgTexts, AuthBgTitle, AuthBgSubTitle, LoginContainer, LogoContainer, AuthContainer, AuthImage, LogoBox, LoginMain, BgSide, BackgroundBottomBg, SideImageContainer } from '@/StyleComponet/Auth/Index'
import { useRouter } from 'next/router'
import { useUser } from '@/context/User'
import Image from 'next/image'
import LoadingScreen from '@/components/LoadingScreen'
import hrats from '@/assets/image/hrats1.svg'
import hratstext from '@/assets/image/hratstext.svg'

function AuthLayout({ children }) {

  const { user } = useUser()
  const router = useRouter()

  useEffect(() => {
    if (user.isLogin && user.role_name === 'hr_recruiter') {
      router.push('/')
    } else if (user.isLogin && user.role_name === 'Super Admin') {
      router.push('/admin')
    }
  }, [user])

  if (user.isLogin) return <LoadingScreen />

  // if (!user.isLogin) {
  return (
    // <LoginBg>
    <LoginMain>


        <AuthSideBg>
          <SideImageContainer style={{
            maxWidth: '776px',
          }}>
            <AuthBgTexts >
              <AuthBgTitle>HR-ATS</AuthBgTitle>
              <AuthBgSubTitle>Connecting Talents, Make Hiring easier</AuthBgSubTitle>
            </AuthBgTexts>


            {/* <AuthImage src={BackImg} alt='image' /> */}

            <BackgroundBottomBg/>
          </SideImageContainer>

        </AuthSideBg>

      <LoginContainer>
        <div style={{ width: '100%', maxWidth: '550px'}}>
          <LogoBox>
            <LogoContainer />
          </LogoBox>
          <AuthContainer>
            {children}
          </AuthContainer>
        </div>
      </LoginContainer>
    </LoginMain>
    // </LoginBg>
  )
  // } else {
  //   return <LoadingScreen/>
  // }

}

export default AuthLayout