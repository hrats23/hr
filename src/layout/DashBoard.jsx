import React from 'react'
import Header from '../components/reusable/Header'
import { Aside, Main } from '@/StyleComponet/hr'
import { useUser } from '@/context/User'
import privateRouter from '@/HOC/privateRouter'

import DashBoardIcons from '@/assets/svg/DashBoardIcons'
import InterViewIcon from '@/assets/svg/InterViewIcon'
import JobIcon from '@/assets/svg/JobIcon'

const routerName = [{ name: 'Dashboard', icon: DashBoardIcons, link: ['/dashboard'] }, { name: 'Job', icon: JobIcon, link: ['/', '/job', '/job/[id]', '/candidates/[id]'] }, { name: 'Interview', icon: InterViewIcon, link: ['/interview'] }]
function DashBoard({ children }) {

    const {user} = useUser()

    // if(user.role_name === 'hr_recruiter'){
        return (
            <>
                <Header routerName={routerName}/>
                <Main>
                    {children}
                </Main>
            </>
        )
    // } else{
    //     return <>Loading...</>
    // }
}

export default privateRouter(DashBoard)
