import LoadingScreen from '@/components/LoadingScreen'
import { instance, post } from '@/service/baseApi'
import { useRouter } from 'next/router'
import React, { createContext, useContext, useEffect, useState } from 'react'

export const UserContext = createContext(null)

const common = ['/career', '/career/[id]',  '/career/[id]/[job]']

const hrScreen = ['/job', '/', '/job/[id]', '/candidates/[id]', '/dashboard', '/interview', ...common]
const adminScreen = ['/admin', ...common]
const authScreen = ['/login', '/forgot', '/verify', '/password', ...common]

function UserProvider({ children }) {

  const [user, setUser] = React.useState({
    isLogin: null,
    // initLoading: true,
    role_name: '',
  })

  const router = useRouter()

  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const refreshToken = async () => {

      setIsLoading(true)
      if (localStorage.getItem('token')) {
        const payload = {
          token: localStorage.getItem('token'),
          user_id: parseInt(localStorage.getItem('user_id'))
        }
        // console.log(payload)
        const res = await post('/refresh', payload)
        if (res?.status) {
          // console.log('refreshToken', res)
          instance.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`
          setUser({
            isLogin: res.status,
            ...res
          })
          if( res.role_name === 'hr_recruiter') {
            !hrScreen.includes(router.pathname) && router.push('/')
          } else if (res.role_name === 'Super Admin') {
            !adminScreen.includes(router.pathname) && router.push('/admin')
          }
 
          // res.role_name === 'Super Admin' ? router.push('/admin') : res.role_name === 'hr_recruiter' && router.push('/')
        } else {
          setUser(pre => ({
            ...pre,
            isLogin: false
          }))
          localStorage.removeItem('token')
          localStorage.removeItem('user_id')
          !common.includes(router.pathname) && router.push('/login')
          // router.push('/login')
        }
      } else {
        setUser(pre => ({
          ...pre,
          isLogin: false
        }))
        !common.includes(router.pathname) && router.push('/login')
        // router.push('/login')
      }
      setIsLoading(false)
    }

    refreshToken()
  }, [])


  if (user.isLogin === null || isLoading) return <LoadingScreen />

  const value = {
    user,
    setUser
  }

  return (
    <UserContext.Provider value={value}>
      {children}
    </UserContext.Provider>
  )
}

export default UserProvider

export const useUser = () => {
  return useContext(UserContext)
}
