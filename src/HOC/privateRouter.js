import { useUser } from "@/context/User"
import { post } from "@/service/baseApi"
import { useRouter } from "next/router"
import { useEffect, useState } from "react"

/**
 * Auth component is responsible for handling authentication and authorization logic.
 * It redirects the user to the appropriate page based on their role.
 * @param {Object} props - The component props.
 * @returns {JSX.Element} - The rendered component.
 */

export default PrivateRouter => {
    const Auth = (props) => {
        const router = useRouter()
        const {user} = useUser()
        // const [AuthLoad, setAuthLoad] = useState(true)

        const common = ['/career', '/career/[id]', '/career/[id]/[job]']

        const hrScreen = ['/job', '/', '/job/[id]', '/candidates/[id]', '/dashboard', '/interview', ...common]
        const adminScreen = ['/admin', ...common]
        const authScreen = ['/login', '/forgot', '/verify', '/password', ...common]

        useEffect(() => {
            switch(user.role_name){
                case 'hr_recruiter':
                    // console.log(router.pathname)
                    !hrScreen.includes(router.pathname) && router.push('/')
                
                    break;
                case 'Super Admin':
                    // console.log(!adminScreen.includes(router.pathname))
                    !adminScreen.includes(router.pathname) && router.push('/admin')

                    break
                default:
                    // console.log(!authScreen.includes(router.pathname))
                    !authScreen.includes(router.pathname) && router.push('/login')
                    break
            }

            // setAuthLoad(false)
            
        }, [router.pathname, user])

        return <PrivateRouter {...props} />
    }

    return Auth

}


// import { post } from "@/service/baseApi";
// import { Router } from "next/router";

// const checkUserAuthentication = async() => {
//     // return { auth: null }; // change null to { isAdmin: true } for test it.
//     if(localStorage.getItem('token')){
//         const payload = {
//             token: localStorage.getItem('token'),
//             user_id: parseInt(localStorage.getItem('user_id'))
//         }
//         // console.log(payload)
//         const res = await post('/refresh', payload)
//         if (res.status) {
//           console.log('refreshToken', res)
//           instance.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`
//           setUser({
//               isLogin: res.status,
//               ...res
//             })

//             return {
//                 isLogin: res.status,
//                 ...res
//             }
//             // res.role_name ? router.push('/admin') : router.push('/')
//         } else {
//             localStorage.removeItem('token')
//             localStorage.removeItem('user_id')
//             return {
//                 isLogin: false
//             }
//         }

//       } else {
//           return {
//               isLogin: false
//           }
//       }
//   };

// export default PrivateRouter =>{
//     const hocComponent = ({ ...props }) => <WrappedComponent {...props} />;

//     hocComponent.getInitialProps = async (context) => {
//         const userAuth = await checkUserAuthentication();

//         if(!userAuth.isLogin){
//             if(context.res){
//                 context.res.writeHead(302, { Location: userAuth.role_name === 'Super Admin' ? '/admin' : '/' });
//                 context.res.end();
//             }else {
//                 Router().replace('/login');
//             }
            
//         }  else if (WrappedComponent.getInitialProps) {
//             const wrappedProps = await WrappedComponent.getInitialProps({...context, auth: userAuth});
//             return { ...wrappedProps, userAuth };
//         }
//         return { userAuth };
//     }
    
//     return hocComponent;
// }

